package com.atlassian.webhooks.plugin;

public final class Version
{
    public static final String MAJOR_VERSION = "${parsedVersion.majorVersion}";
}
