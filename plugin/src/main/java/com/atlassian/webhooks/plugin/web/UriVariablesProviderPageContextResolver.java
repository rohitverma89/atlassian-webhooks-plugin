package com.atlassian.webhooks.plugin.web;

import com.atlassian.webhooks.api.document.ProvidesUrlVariables;
import com.atlassian.webhooks.api.register.RegisteredWebHookEvent;
import com.atlassian.webhooks.api.register.WebHookEventGroup;
import com.atlassian.webhooks.api.register.WebHookEventSection;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.spi.DocumentedUriVariablesProvider;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public final class UriVariablesProviderPageContextResolver
{
    private final WebHookPluginRegistrationContainer webHookPluginRegistrationContainer;

    @Autowired
    public UriVariablesProviderPageContextResolver(final WebHookPluginRegistrationContainer webHookPluginRegistrationContainer)
    {
        this.webHookPluginRegistrationContainer = webHookPluginRegistrationContainer;
    }

    public Map<String, Collection<String>> uriVariablesForWebhooks()
    {
        Map<String, Collection<String>> result = Maps.newTreeMap();
        for (RegisteredWebHookEvent webhook : webHookPluginRegistrationContainer.getAllWebhooks())
        {
            result.put(webhook.getId(), Sets.<String>newTreeSet());
            Iterable<UriVariablesProvider> uriVariablesProviders = webHookPluginRegistrationContainer.getUriVariablesProviders().allForType(webhook.getEventClass());
            for (UriVariablesProvider provider : uriVariablesProviders)
            {
                result.get(webhook.getId()).addAll(toDocumentedVariablesProvider(provider).providedVariables());
            }
        }
        return result;
    }

    public List<String> allUriVariables()
    {
        Set<String> result = Sets.newTreeSet();
        Set<String> visibleWebhooks = getVisibleWebhooks();
        for (Map.Entry<String, Collection<String>> entry : uriVariablesForWebhooks().entrySet())
        {
            String webhookId = entry.getKey();
            if (visibleWebhooks.contains(webhookId))
            {
                result.addAll(entry.getValue());
            }
        }
        return Lists.newArrayList(result);
    }

    private static <T> DocumentedUriVariablesProvider<T> toDocumentedVariablesProvider(final UriVariablesProvider<T> variablesProvider)
    {
        if (variablesProvider instanceof DocumentedUriVariablesProvider)
        {
            return (DocumentedUriVariablesProvider<T>) variablesProvider;
        }
        else
        {
            return new DocumentedUriVariablesProvider<T>()
            {
                @Override
                public Collection<String> providedVariables()
                {
                    return getProvidedVariablesFromAnnotation(variablesProvider);
                }

                @Override
                public Map<String, Object> uriVariables(final T event)
                {
                    return variablesProvider.uriVariables(event);
                }
            };
        }
    }

    private static <T> Collection<String> getProvidedVariablesFromAnnotation(final UriVariablesProvider<T> variablesProvider)
    {
        ProvidesUrlVariables annotation = variablesProvider.getClass().getAnnotation(ProvidesUrlVariables.class);
        if (annotation == null)
        {
            for (Method method : variablesProvider.getClass().getMethods())
            {
                if (method.isAnnotationPresent(ProvidesUrlVariables.class))
                {
                    return Lists.newArrayList(method.getAnnotation(ProvidesUrlVariables.class).value());
                }
            }
            return Collections.emptySet();
        }
        else
        {
            return Lists.newArrayList(annotation.value());
        }
    }

    private Set<String> getVisibleWebhooks()
    {
        Set<String> result = Sets.newHashSet();
        for (WebHookEventSection section : webHookPluginRegistrationContainer.getWebHookSections())
        {
            if (!Strings.isNullOrEmpty(section.getNameI18nKey()))
            {
                for (WebHookEventGroup webHookEventGroup : Iterables.filter(section.getGroups(), new Predicate<WebHookEventGroup>()
                {
                    @Override
                    public boolean apply(final WebHookEventGroup input)
                    {
                        return !Strings.isNullOrEmpty(input.getNameI18nKey());
                    }
                }))
                {
                    result.addAll(Lists.transform(webHookEventGroup.getEvents(), new Function<RegisteredWebHookEvent, String>()
                    {
                        @Override
                        public String apply(final RegisteredWebHookEvent input)
                        {
                            return input.getId();
                        }
                    }));
                }
            }
        }
        return result;
    }
}
