package com.atlassian.webhooks.plugin.module;

import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.RegisteredWebHookEvent;
import com.atlassian.webhooks.api.register.WebHookEventGroup;
import com.atlassian.webhooks.api.register.WebHookEventSection;
import com.atlassian.webhooks.api.register.WebHookPluginRegistration;
import com.atlassian.webhooks.api.util.SectionKey;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.SetMultimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.google.common.collect.Iterables.transform;

/**
 * Class that holds all registered web hook events.
 */
public class WebHookRegistry
{
    private final Map<WebHookKey, RegisteredWebHookEvent<?>> registrationsByWebHookKey = Maps.newConcurrentMap();
    private final SetMultimap<Class, RegisteredWebHookEvent> registrationsByEvent = HashMultimap.create();
    private final Map<ModuleKey, Set<RegisteredWebHookEvent>> registrationsByModuleKey = Maps.newConcurrentMap();
    private final Map<WebHookKey, SectionKey> sectionsByWebHookKey = Maps.newConcurrentMap();

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Returns all identifiers of registered web hook events.
     *
     * @return an immutable set of keys for all registered web hook events.
     */
    public Set<WebHookKey> getWebHookIds()
    {
        return ImmutableSet.copyOf(registrationsByWebHookKey.keySet());
    }

    /**
     * Returns all web hook events that listen for a specified system event.
     *
     * @param event any event
     * @return an immutable list of web hook events that are interested in the specified system event.
     */
    public List<WebHookEvent> getWebHooks(final Object event)
    {
        try
        {
            if (!lock.readLock().tryLock(5, TimeUnit.SECONDS))
            {
                throw new IllegalStateException("Could not acquire read lock to WebHooks registrations");
            }

            final Map<Class,Collection<RegisteredWebHookEvent>> webHookRegistrationByEvent = Maps.filterKeys(registrationsByEvent.asMap(), new Predicate<Class>()
            {
                @Override
                public boolean apply(final Class eventClass)
                {
                    return eventClass.isInstance(event);
                }
            });
            final Iterable<RegisteredWebHookEvent> webHookRegistrations = Iterables.concat(webHookRegistrationByEvent.values());
            return ImmutableList.copyOf(transform(webHookRegistrations, new Function<RegisteredWebHookEvent, WebHookEvent>()
            {
                @Override
                public WebHookEvent apply(final RegisteredWebHookEvent registration)
                {
                    return new WebHookEvent(registration.getId(), event, registration.getEventMatcher());
                }
            }));
        }
        catch (InterruptedException e)
        {
            logger.error("There was an error while reading existing WebHooks events", e);
            return Collections.emptyList();
        }
        finally
        {
            lock.readLock().unlock();
        }
    }

    void add(ModuleKey moduleKey, WebHookPluginRegistration pluginRegistration)
    {
        try
        {
            if (!lock.writeLock().tryLock(30, TimeUnit.SECONDS))
            {
                throw new IllegalStateException("Could not acquire write lock to WebHooks registrations");
            }

            for (WebHookEventSection section : pluginRegistration.getSections())
            {
                for (WebHookEventGroup group : section.getGroups())
                {
                    for (RegisteredWebHookEvent event : group.getEvents())
                    {
                        if (event.getEventClass() != null)
                        {
                            registrationsByEvent.put(event.getEventClass(), event);
                        }
                        registrationsByWebHookKey.put(new WebHookKey(event.getId()), event);
                        sectionsByWebHookKey.put(new WebHookKey(event.getId()), new SectionKey(section.getKey()));
                    }
                }
            }

            registrationsByModuleKey.put(moduleKey, ImmutableSet.copyOf(pluginRegistration.getRegistrations()));
        }
        catch (InterruptedException e)
        {
            logger.error("Error when adding new WebHooks events", e);
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    void remove(ModuleKey moduleKey)
    {
        try
        {
            if (!lock.writeLock().tryLock(30, TimeUnit.SECONDS))
            {
                throw new IllegalStateException("Could not acquire write lock to WebHooks registrations");
            }

            Set<RegisteredWebHookEvent> registrations = registrationsByModuleKey.remove(moduleKey);

            for (Iterator<RegisteredWebHookEvent> i = registrationsByEvent.values().iterator(); i.hasNext(); )
            {
                if (registrations.contains(i.next()))
                {
                    i.remove();
                }
            }

            for (Iterator<RegisteredWebHookEvent<?>> i = registrationsByWebHookKey.values().iterator(); i.hasNext(); )
            {
                if (registrations.contains(i.next()))
                {
                    i.remove();
                }
            }

            for (RegisteredWebHookEvent event : registrations)
            {
                sectionsByWebHookKey.remove(new WebHookKey(event.getId()));
            }
        }
        catch (InterruptedException e)
        {
            logger.error("Error when removing existing WebHooks events", e);
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    public SectionKey sectionOf(final String id)
    {
        return sectionsByWebHookKey.get(new WebHookKey(id));
    }
}
