package com.atlassian.webhooks.plugin.module;

import com.atlassian.webhooks.api.register.WebHookPluginRegistration;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;

@Named ("cloudConditionResolver")
public class CloudConditionResolverImpl implements CloudConditionResolver
{
    private final WebHookPluginRegistrationContainer container;

    @Autowired
    public CloudConditionResolverImpl(final WebHookPluginRegistrationContainer container)
    {
        this.container = container;
    }

    public boolean isCloud()
    {
        final Iterable<WebHookPluginRegistration> webHookRegistrationsWithCloudCondition = Iterables.filter(container.getWebHookRegistrations(), new Predicate<WebHookPluginRegistration>()
        {
            @Override
            public boolean apply(final WebHookPluginRegistration registration)
            {
                return registration.getCloudCondition().isDefined();
            }
        });

        if (Iterables.isEmpty(webHookRegistrationsWithCloudCondition))
        {
            return false;
        }
        else
        {
            return Iterables.all(webHookRegistrationsWithCloudCondition, new Predicate<WebHookPluginRegistration>()
            {
                @Override
                public boolean apply(final WebHookPluginRegistration registration)
                {
                    return registration.getCloudCondition().get().isCloud();
                }
            });
        }
    }

}
