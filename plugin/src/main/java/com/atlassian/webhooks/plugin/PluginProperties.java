package com.atlassian.webhooks.plugin;

public class PluginProperties
{
    private PluginProperties()
    {
    }

    public static final String PLUGIN_KEY = "com.atlassian.webhooks.atlassian-webhooks-plugin";
}
