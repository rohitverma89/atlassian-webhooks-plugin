package com.atlassian.webhooks.plugin.rest;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerServiceResponse;
import com.atlassian.webhooks.api.util.Channel;
import com.atlassian.webhooks.api.util.MessageCollection;
import com.atlassian.webhooks.plugin.api.SecuredWebHookListenerService;
import com.google.common.base.Function;
import com.google.common.base.Supplier;

import java.net.URI;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static com.atlassian.webhooks.api.register.listener.WebHookListenerService.WebHookListenerUpdateInput;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;
import static javax.ws.rs.core.Response.created;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

@Path ("webhook")
@Produces (MediaType.APPLICATION_JSON)
public class WebHookListenerResource
{
    private final UserManager userManager;
    private final SecuredWebHookListenerService webHookListenerService;

    public WebHookListenerResource(UserManager userManager, SecuredWebHookListenerService webHookListenerService)
    {
        this.webHookListenerService = checkNotNull(webHookListenerService);
        this.userManager = checkNotNull(userManager);
    }

    @GET
    public Response getAllListeners(@Context final UriInfo uriInfo)
    {
        final Iterable<PersistentWebHookListener> allWebHookListeners = webHookListenerService.getAllWebHookListeners(Channel.REST);
        return ok(transform(allWebHookListeners, new Function<PersistentWebHookListener, WebHookListenerResponseBean>()
                {
                    @Override
                    public WebHookListenerResponseBean apply(final PersistentWebHookListener webHook)
                    {
                        final URI self = uriInfo.getAbsolutePathBuilder().path(String.valueOf(webHook.getId().get())).build();
                        return new WebHookListenerResponseBean.Factory(userManager).create(webHook, self);
                    }
                })
        ).build();
    }

    @POST
    @Consumes (MediaType.APPLICATION_JSON)
    public Response register(final WebHookListenerJsonBean registration,
            @Context final UriInfo uriInfo,
            @DefaultValue ("false") @QueryParam ("ui") boolean registeredViaUI)
    {
        final WebHookListenerServiceResponse webHookListenerServiceResponse =
                webHookListenerService.registerWebHookListener(registeredViaUI ? Channel.UI : Channel.REST, registration.toRegisteredWebHookListener());

        return webHookListenerServiceResponse.fold(new Function<MessageCollection, Response>()
        {
            @Override
            public Response apply(final MessageCollection messageCollection)
            {
                return fromMessageCollection(messageCollection);
            }
        }, new Function<PersistentWebHookListener, Response>()
        {
            @Override
            public Response apply(final PersistentWebHookListener listener)
            {
                final URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(listener.getId().get())).build();
                return created(uri)
                        .entity(new WebHookListenerResponseBean.Factory(userManager).create(listener, uri))
                        .build();
            }
        });
    }

    @GET
    @Path ("{id}")
    public Response getListener(@PathParam ("id") final int id, @Context final UriInfo uriInfo)
    {
        return webHookListenerService.getWebHookListener(Channel.REST, id).fold(new Supplier<Response>()
        {
            @Override
            public Response get()
            {
                return status(Response.Status.NOT_FOUND).build();
            }
        }, new Function<PersistentWebHookListener, Response>()
        {
            @Override
            public Response apply(final PersistentWebHookListener listener)
            {
                final URI self = uriInfo.getAbsolutePath();
                return ok(new WebHookListenerResponseBean.Factory(userManager).create(listener, self)).build();
            }
        });
    }

    @PUT
    @Path ("{id}")
    @Consumes (MediaType.APPLICATION_JSON)
    public Response update(@PathParam ("id") final int id, final WebHookListenerJsonBean registration, @Context final UriInfo uriInfo)
    {
        return webHookListenerService.updateWebHookListener(Channel.REST, id, updateInput(registration)).fold(new Function<MessageCollection, Response>()
        {
            @Override
            public Response apply(final MessageCollection messageCollection)
            {
                return fromMessageCollection(messageCollection);
            }
        }, new Function<PersistentWebHookListener, Response>()
        {
            @Override
            public Response apply(final PersistentWebHookListener listener)
            {
                final URI self = uriInfo.getAbsolutePath();
                return ok(new WebHookListenerResponseBean.Factory(userManager).create(listener, self)).build();
            }
        });
    }

    @DELETE
    @Path ("{id}")
    public Response deleteListener(@PathParam ("id") final int id)
    {
        final MessageCollection messageCollection = webHookListenerService.deleteWebHookListener(Channel.REST, id);
        if (messageCollection.isEmpty())
        {
            return noContent().build();
        }
        else
        {
            return fromMessageCollection(messageCollection);
        }
    }

    private Response fromMessageCollection(final MessageCollection messageCollection)
    {
        return Response.status(MessageCollection.Reason.getWorstReason(messageCollection.getReasons()).getHttpStatusCode())
                .entity(new ErrorCollectionBean(messageCollection))
                .build();
    }

    private WebHookListenerUpdateInput updateInput(final WebHookListenerJsonBean bean)
    {
        return WebHookListenerUpdateInput.builder()
                .setDescription(bean.getDescription())
                .setName(bean.getName())
                .setEnabled(bean.isEnabled())
                .setEvents(bean.getEvents())
                .setFilters(bean.getFilters())
                .setExcludeBody(bean.isExcludeBody())
                .setUrl(bean.getUrl())
                .build();
    }

}
