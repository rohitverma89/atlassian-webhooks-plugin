package com.atlassian.webhooks.plugin;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.ModuleDescriptorWebHookListenerRegistry;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.google.common.base.Supplier;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;

import java.util.Collection;
import javax.inject.Named;

/**
 * Listener registry for WebHookListeners declared in atlassian-plugin.xml via <webhook> descriptor.
 */
@ExportAsService (ModuleDescriptorWebHookListenerRegistry.class)
@Named ("moduleDescriptorWebHookListenerRegistry")
public class ModuleDescriptorWebHookListenerRegistryImpl implements ModuleDescriptorWebHookListenerRegistry, WebHookListenerProvider
{
    private final Multimap<String, WebHookListener> listeners = newMultimap();

    @Override
    public void register(final String webhookId, final WebHookListener webHookListener)
    {
        listeners.put(webhookId, webHookListener);
    }

    @Override
    public void unregister(final String webhookId, final WebHookListener webHookListener)
    {
        listeners.get(webhookId).remove(webHookListener);
    }

    @Override
    public Iterable<WebHookListener> getListeners(WebHookEvent webHookEvent)
    {
        return listeners.get(webHookEvent.getId());
    }

    private static Multimap<String, WebHookListener> newMultimap()
    {
        return Multimaps.synchronizedMultimap(
                Multimaps.newMultimap(Maps.<String, Collection<WebHookListener>>newHashMap(),
                        new Supplier<Collection<WebHookListener>>()
                        {
                            public Collection<WebHookListener> get()
                            {
                                return Sets.newHashSet();
                            }
                        }));
    }
}
