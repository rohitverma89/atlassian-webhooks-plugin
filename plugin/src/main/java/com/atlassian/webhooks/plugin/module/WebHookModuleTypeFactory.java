package com.atlassian.webhooks.plugin.module;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Registers the {@link WebHookModuleDescriptor} with the host container
 */
@ModuleType(ListableModuleDescriptorFactory.class)
@Named("webhookModuleTypeFactory")
public class WebHookModuleTypeFactory extends SingleModuleDescriptorFactory<WebHookModuleDescriptor>
{
    @Inject
    public WebHookModuleTypeFactory(HostContainer hostContainer)
    {
        super(hostContainer, "webhook", WebHookModuleDescriptor.class);
    }
}
