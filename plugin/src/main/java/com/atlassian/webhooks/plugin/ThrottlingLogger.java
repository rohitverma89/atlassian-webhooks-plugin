package com.atlassian.webhooks.plugin;

import com.atlassian.fugue.Effect;
import org.slf4j.Logger;

final class ThrottlingLogger
{
    private final Logger logger;
    private final TokenBucket tokenBucket;

    public ThrottlingLogger(final Logger logger, final TokenBucket bucket)
    {
        this.logger = logger;
        this.tokenBucket = bucket;
    }

    public Void use(Effect<Logger> loggingEffect)
    {
        if (tokenBucket.getToken())
        {
            loggingEffect.apply(logger);
        }
        return null;
    }
}
