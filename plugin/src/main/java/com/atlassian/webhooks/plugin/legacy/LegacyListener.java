package com.atlassian.webhooks.plugin.legacy;

import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.spi.provider.PluginModuleListenerParameters;
import com.google.common.base.Objects;

import java.net.URI;

public final class LegacyListener
{
    private final WebHookListener listener;
    private final PluginModuleListenerParameters legacyParameters;

    private LegacyListener(final WebHookListener listener, final PluginModuleListenerParameters legacyParameters)
    {
        this.listener = listener;
        this.legacyParameters = legacyParameters;
    }

    public static LegacyListener of(final URI uri, String pluginKey, PluginModuleListenerParameters legacyParameters)
    {
        return new LegacyListener(LegacyToNewListenerTransformer.of(legacyParameters).transform(pluginKey, uri), legacyParameters);
    }

    public WebHookListener getListener()
    {
        return listener;
    }

    public PluginModuleListenerParameters getLegacyParameters()
    {
        return legacyParameters;
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(listener, legacyParameters);
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        final LegacyListener other = (LegacyListener) obj;
        return Objects.equal(this.listener, other.listener) && Objects.equal(this.legacyParameters, other.legacyParameters);
    }
}
