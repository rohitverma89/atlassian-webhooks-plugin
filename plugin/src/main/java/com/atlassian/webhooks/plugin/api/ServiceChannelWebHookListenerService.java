package com.atlassian.webhooks.plugin.api;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;
import com.atlassian.webhooks.api.register.listener.WebHookListenerService;
import com.atlassian.webhooks.api.register.listener.WebHookListenerServiceResponse;
import com.atlassian.webhooks.api.util.Channel;
import com.atlassian.webhooks.api.util.MessageCollection;

import javax.inject.Inject;
import javax.inject.Named;

import static com.google.common.base.Preconditions.checkNotNull;

@ExportAsService(WebHookListenerService.class)
@Named("webHookListenerService")
public class ServiceChannelWebHookListenerService implements WebHookListenerService
{
    private final SecuredWebHookListenerService delegate;

    @Inject
    public ServiceChannelWebHookListenerService(SecuredWebHookListenerService delegate)
    {
        this.delegate = checkNotNull(delegate, "securedWebHookListenerService");
    }

    @Override
    public Iterable<PersistentWebHookListener> getAllWebHookListeners()
    {
        return delegate.getAllWebHookListeners(Channel.SERVICE);
    }

    @Override
    public Option<PersistentWebHookListener> getWebHookListener(int id)
    {
        return delegate.getWebHookListener(Channel.SERVICE, id);
    }

    @Override
    public WebHookListenerServiceResponse registerWebHookListener(PersistentWebHookListener registrationParameters, RegistrationMethod registrationMethod)
    {
        return delegate.registerWebHookListener(toChannel(registrationMethod), registrationParameters);
    }

    @Override
    public WebHookListenerServiceResponse registerWebHookListener(PersistentWebHookListener registrationParameters)
    {
        return delegate.registerWebHookListener(Channel.SERVICE, registrationParameters);
    }

    @Override
    public WebHookListenerServiceResponse updateWebHookListener(int id, WebHookListenerUpdateInput registrationParameters)
    {
        return delegate.updateWebHookListener(Channel.SERVICE, id, registrationParameters);
    }

    @Override
    public MessageCollection deleteWebHookListener(int id)
    {
        return delegate.deleteWebHookListener(Channel.SERVICE, id);
    }

    private Channel toChannel(RegistrationMethod method)
    {
        switch (method)
        {
            case SERVICE:
                return Channel.SERVICE;
            case REST:
                return Channel.REST;
            case UI:
                return Channel.UI;
        }
        throw new IllegalArgumentException("Unsupported registration method: " + method);
    }
}
