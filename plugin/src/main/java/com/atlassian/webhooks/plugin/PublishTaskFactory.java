package com.atlassian.webhooks.plugin;

import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;

public interface PublishTaskFactory
{
    PublishTask getPublishTask(WebHookEvent webHookEvent, WebHookListener listener);
}
