package com.atlassian.webhooks.plugin.uri;

import com.google.common.base.Charsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class UrlVariableSubstitutor
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String PLACEHOLDER_PATTERN_STRING = "\\$?\\{([^}]*)}";
    private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile(PLACEHOLDER_PATTERN_STRING);

    public String replace(String source, Map<String, ?> context)
    {
        Matcher m = PLACEHOLDER_PATTERN.matcher(source);
        StringBuffer sb = new StringBuffer();
        while (m.find())
        {
            String term = m.group(1);
            String value = fromContext(term, context);
            m.appendReplacement(sb, encodeQuery(value));
        }
        m.appendTail(sb);

        return sb.toString();
    }

    private String encodeQuery(String value)
    {
        try
        {
            return value != null ? encode(value) : null;
        }
        catch (UnsupportedEncodingException ex)
        {
            logger.error("Error encoding value '" + value + "' to querystring", ex);
            return "";
        }
    }

    private String encode(final String value) throws UnsupportedEncodingException
    {
        return URLEncoder.encode(value, Charsets.UTF_8.name());
    }


    private String fromContext(String term, Map<String, ?> context)
    {
        Iterable<String> terms = Arrays.asList(term.split("\\."));
        Object value = fromContext(terms, context);
        if (null == value)
        {
            value = context.get(term);
        }
        if (null == value)
        {
            return "";
        }
        if (value instanceof Number
                || value instanceof String
                || value instanceof Boolean)
        {
            return value.toString();
        }
        else if (value instanceof String[]
                || value instanceof Number[]
                || value instanceof Boolean[])
        {
            return ((Object[]) value)[0].toString();
        }
        return "";
    }

    private Object fromContext(Iterable<String> terms, Map<String, ?> context)
    {
        Object current = context;
        for (String key : terms)
        {
            if (null == current)
            {
                return null;
            }
            if (current instanceof Map)
            {
                current = ((Map) current).get(key);
            }
        }
        return current;
    }
}
