package com.atlassian.webhooks.plugin.validator;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.util.ErrorMessage;
import com.atlassian.webhooks.api.util.MessageCollection;
import com.atlassian.webhooks.plugin.module.WebHookKey;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.plugin.module.WebHookRegistry;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;

import static com.atlassian.webhooks.api.util.MessageCollection.Reason.VALIDATION_FAILED;
import static com.google.common.base.Preconditions.checkNotNull;

@Named("eventsValidator")
public class EventsValidator
{
    private final WebHookRegistry webHookRegistry;
    private final I18nResolver i18n;

    @Autowired
    public EventsValidator(WebHookPluginRegistrationContainer container, final I18nResolver i18n)
    {
        this.i18n = checkNotNull(i18n);
        this.webHookRegistry = checkNotNull(container.getWebHookRegistry());
    }

    public MessageCollection validate(final PersistentWebHookListener registrationParameters)
    {
        if (registrationParameters.getEvents() != null)
        {
            final Iterable<WebHookKey> validEventNames = webHookRegistry.getWebHookIds();
            Iterable<String> invalidEvents = Iterables.filter(registrationParameters.getEvents(), new Predicate<String>()
            {
                @Override
                public boolean apply(final String eventName)
                {
                    return !Iterables.contains(validEventNames, new WebHookKey(eventName));
                }
            });

            return createInvalidEventsMessage(invalidEvents);
        }
        else
        {
            return MessageCollection.empty();
        }
    }

    private MessageCollection createInvalidEventsMessage(final Iterable<String> invalidEvents)
    {
        return Iterables.isEmpty(invalidEvents) ?
                MessageCollection.empty() :
                transformToMessageCollection(invalidEvents);
    }

    private MessageCollection transformToMessageCollection(final Iterable<String> invalidEvents)
    {
        final MessageCollection.Builder builder = MessageCollection.builder();
        for (String event : invalidEvents)
        {
            builder.addMessage(new ErrorMessage(i18n.getText("webhooks.invalid.event", event)), VALIDATION_FAILED);
        }
        return builder.build();
    }
}
