package com.atlassian.webhooks.plugin.rest;

import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static com.google.common.base.Objects.firstNonNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebHookListenerJsonBean
{
    @JsonProperty
    private String name;
    @JsonProperty
    private String url;
    @JsonProperty
    private String description;
    @JsonProperty
    private Boolean excludeBody;
    @JsonProperty
    private Map<String, String> filters;
    @JsonProperty
    private Collection<String> events;
    @JsonProperty
    private Boolean enabled;

    public WebHookListenerJsonBean()
    {
    }

    public WebHookListenerJsonBean(String name, String url, String description, boolean excludeBody, Map<String, String> filters, Collection<String> events, Boolean enabled)
    {
        this.name = name;
        this.url = url;
        this.description = description;
        this.excludeBody = excludeBody;
        this.filters = filters;
        this.events = events;
        this.enabled = enabled;
    }

    public String getName()
    {
        return name;
    }

    public String getUrl()
    {
        return url;
    }

    public String getDescription()
    {
        return description;
    }

    public Boolean isExcludeBody()
    {
        return excludeBody;
    }

    public Map<String, String> getFilters()
    {
        return filters;
    }

    public Collection<String> getEvents()
    {
        return events;
    }

    public Boolean isEnabled()
    {
        return enabled;
    }

    @JsonIgnore
    public PersistentWebHookListener toRegisteredWebHookListener()
    {
        return PersistentWebHookListener
                .newlyCreated()
                .addFilters(Objects.firstNonNull(getFilters(), Collections.<String, String>emptyMap()))
                .addWebHookIds(Objects.firstNonNull(getEvents(), Collections.<String>emptyList()))
                .setEnabled(Objects.firstNonNull(enabled, Boolean.TRUE))
                .setExcludeBody(Objects.firstNonNull(isExcludeBody(), Boolean.FALSE))
                .setListenerName(Strings.nullToEmpty(getName()))
                .setUrl(Strings.nullToEmpty(getUrl()))
                .setDescription(Strings.nullToEmpty(getDescription()))
                .build();
    }

}
