package com.atlassian.webhooks.plugin.module;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.webhooks.spi.WebHookPluginRegistrationFactory;
import org.dom4j.Element;

import javax.annotation.Nonnull;

public class WebHookRegistrationFactoryModuleDescriptor extends AbstractModuleDescriptor<WebHookPluginRegistrationFactory>
{
    public WebHookRegistrationFactoryModuleDescriptor(final ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    @Override
    public void init(@Nonnull final Plugin plugin, @Nonnull final Element element) throws PluginParseException
    {
        super.init(plugin, element);
    }

    @Override
    public void enabled()
    {
        super.enabled();
    }

    @Override
    public void disabled()
    {
        super.disabled();
    }

    @Override
    public WebHookPluginRegistrationFactory getModule()
    {
        return moduleFactory.createModule(moduleClassName, this);
    }
}
