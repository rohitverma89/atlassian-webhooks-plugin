package com.atlassian.webhooks.plugin;

import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerService;
import com.atlassian.webhooks.api.util.ConsiderAllListeners;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import org.apache.commons.lang.StringUtils;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.contains;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

/**
 * Retrieves all WebHook listeners from AO. Filters disabled listeners and those which couldn't be transformed by the
 * provider.
 */
public class PersistentWebHookListenerProvider implements WebHookListenerProvider
{
    private final WebHookListenerService webHookListenerService;
    private final WebHookPluginRegistrationContainer webHookContainer;

    public PersistentWebHookListenerProvider(WebHookListenerService webHookListenerService, final WebHookPluginRegistrationContainer webHookContainer)
    {
        this.webHookContainer = webHookContainer;
        this.webHookListenerService = checkNotNull(webHookListenerService);
    }

    @Override
    public Iterable<WebHookListener> getListeners(final WebHookEvent webHookEvent)
    {
        final Iterable<PersistentWebHookListener> enabledWebHookListeners = filter(webHookListenerService.getAllWebHookListeners(), new Predicate<PersistentWebHookListener>()
        {
            @Override
            public boolean apply(final PersistentWebHookListener webHookListenerParameters)
            {
                return webHookListenerParameters.isEnabled() && preFilter(webHookEvent, webHookListenerParameters);
            }
        });
        return transform(enabledWebHookListeners, new Function<PersistentWebHookListener, WebHookListener>()
        {
            @Override
            public WebHookListener apply(final PersistentWebHookListener registeredListener)
            {
                return WebHookListener
                        .fromPersistentStore(registeredListener.getId().get(), registeredListener.getName(),
                                StringUtils.isEmpty(registeredListener.getLastUpdatedByUser()) ? Optional.<String>absent() : Optional.of(registeredListener.getLastUpdatedByUser()))
                        .to(registeredListener.getUrl())
                        .excludeBody(registeredListener.isExcludeBody())
                        .withFilter(registeredListener.getFilterFor(webHookContainer.getWebHookRegistry().sectionOf(webHookEvent.getId())))
                        .build();
            }
        });
    }

    private boolean preFilter(WebHookEvent webHookEvent, PersistentWebHookListener listener)
    {
        boolean skipFiltering = webHookEvent.getEventMatcher().getClass().isAnnotationPresent(ConsiderAllListeners.class);
        return skipFiltering || contains(listener.getEvents(), webHookEvent.getId());
    }
}
