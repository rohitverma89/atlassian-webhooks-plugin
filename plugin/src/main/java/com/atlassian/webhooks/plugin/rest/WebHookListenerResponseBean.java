package com.atlassian.webhooks.plugin.rest;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.util.Filter;
import com.atlassian.webhooks.api.util.SectionKey;
import com.google.common.collect.Maps;
import org.codehaus.jackson.annotate.JsonProperty;

import java.net.URI;
import java.util.Map;

public class WebHookListenerResponseBean extends WebHookListenerJsonBean
{
    @JsonProperty
    private URI self;
    @JsonProperty
    private String lastUpdatedUser;
    @JsonProperty
    private String lastUpdatedDisplayName;
    @JsonProperty
    private Long lastUpdated;

    public WebHookListenerResponseBean()
    {
    }

    public WebHookListenerResponseBean(PersistentWebHookListener listener, URI self, String lastUpdatedDisplayName, Long lastUpdated)
    {
        super(listener.getName(), listener.getUrl(), listener.getDescription(), listener.isExcludeBody(), asStrings(listener.getFilters()), listener.getEvents(), listener.isEnabled());
        this.lastUpdatedDisplayName = lastUpdatedDisplayName;
        this.lastUpdated = lastUpdated;
        this.lastUpdatedUser = listener.getLastUpdatedByUser();
        this.self = self;
    }

    private static Map<String, String> asStrings(Map<SectionKey, Filter> filters)
    {
        Map<String, String> result = Maps.newHashMap();
        for (Map.Entry<SectionKey, Filter> entry : filters.entrySet())
        {
            result.put(entry.getKey().getValue(), entry.getValue().getValue());
        }
        return result;
    }

    public URI getSelf()
    {
        return self;
    }

    public String getLastUpdatedUser()
    {
        return lastUpdatedUser;
    }

    public String getLastUpdatedDisplayName()
    {
        return lastUpdatedDisplayName;
    }

    public Long getLastUpdated()
    {
        return lastUpdated;
    }

    public static class Factory
    {
        private final UserManager userManager;

        public Factory(UserManager userManager)
        {
            this.userManager = userManager;
        }

        public WebHookListenerResponseBean create(PersistentWebHookListener listenerParameters, URI self)
        {
            final UserProfile userProfile = userManager.getUserProfile(listenerParameters.getLastUpdatedByUser());
            final String userFullName = userProfile != null ? userProfile.getFullName() : listenerParameters.getLastUpdatedByUser();
            return new WebHookListenerResponseBean(listenerParameters, self, userFullName, listenerParameters.getLastUpdated().getTime());
        }
    }
}
