package com.atlassian.webhooks.plugin.impl;

import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.spi.QueryParamsProvider;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Named;

@Named ("compositeQueryParamsProvider")
public class CompositeQueryParamsProvider implements QueryParamsProvider
{
    private static final Logger log = LoggerFactory.getLogger(CompositeQueryParamsProvider.class);
    private final WebHookPluginRegistrationContainer container;
    private final QueryParamsProvider userQueryParamsProvider;

    @Inject
    public CompositeQueryParamsProvider(WebHookPluginRegistrationContainer container,
            @Qualifier ("userQueryParamProvider") QueryParamsProvider userQueryParamsProvider)
    {
        this.container = container;
        this.userQueryParamsProvider = userQueryParamsProvider;
    }

    @Override
    public Multimap<String, String> provideQueryParams(Optional<UserProfile> userProfile, WebHookListenerRegistrationDetails registrationDetails)
    {
        ImmutableMultimap.Builder<String, String> fullQueryParamMapBuilder = ImmutableMultimap.builder();
        ImmutableSet<QueryParamsProvider> queryParamsProviders = ImmutableSet.<QueryParamsProvider>builder()
                .addAll(container.getQueryParamsProviders())
                .add(userQueryParamsProvider)
                .build();
        for (QueryParamsProvider uriQueryParamsProvider : queryParamsProviders)
        {
            try
            {
                Multimap<String, String> queryParams = uriQueryParamsProvider.provideQueryParams(userProfile, registrationDetails);
                fullQueryParamMapBuilder.putAll(queryParams);
            }
            catch (RuntimeException exception)
            {
                log.warn("uriQueryParamsProvider " + uriQueryParamsProvider + "has thrown a runtime exception", exception);
            }
        }
        return fullQueryParamMapBuilder.build();
    }
}
