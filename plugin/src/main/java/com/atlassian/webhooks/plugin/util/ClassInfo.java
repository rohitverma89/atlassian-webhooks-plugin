package com.atlassian.webhooks.plugin.util;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.*;

import static com.google.common.base.Objects.firstNonNull;

public final class ClassInfo<T>
{
    private final Class<T> type;

    private ClassInfo(Class<T> type)
    {
        this.type = type;
    }

    public static <T> ClassInfo<T> of(Class<T> type)
    {
        return new ClassInfo<T>(type);
    }

    /**
     * Computes all levels of class hierarchy.
     *
     * @return class hierarchy from most specific to least
     */
    public List<HierarchyLevel> typeHierarchy()
    {
        List<HierarchyLevel> hierarchyWithoutObject = computeHierarchy(type);
        hierarchyWithoutObject.add(HierarchyLevel.singleton(Object.class));
        return ImmutableList.copyOf(hierarchyWithoutObject);
    }

    private static List<HierarchyLevel> computeHierarchy(Class type)
    {
        List<HierarchyLevel> result = Lists.newArrayList();
        if (type != null && !Object.class.equals(type))
        {
            result.add(HierarchyLevel.singleton(type));

            LinkedList<Class> superTypes = superTypes(type);
            List<HierarchyLevel> nextHierarchyLevels = Collections.emptyList();
            while (!superTypes.isEmpty())
            {
                nextHierarchyLevels = zip(nextHierarchyLevels, computeHierarchy(superTypes.pop()));
            }

            result.addAll(nextHierarchyLevels);
        }
        return result;
    }

    private static LinkedList<Class> superTypes(Class type)
    {
        LinkedList<Class> superTypes = Lists.newLinkedList();
        if (type.getSuperclass() != null)
        {
            superTypes.add(type.getSuperclass());
        }
        superTypes.addAll(Arrays.asList(firstNonNull(type.getInterfaces(), new Class[0])));
        return superTypes;
    }

    private static List<HierarchyLevel> zip(List<HierarchyLevel> levels1, List<HierarchyLevel> levels2)
    {
        int maxSize = Math.max(levels1.size(), levels2.size());
        List<HierarchyLevel> zipped = Lists.newArrayListWithCapacity(maxSize);
        for (int i = 0; i < maxSize; i++)
        {
            zipped.add(HierarchyLevel.combine(getOrNull(levels1, i), getOrNull(levels2, i)));
        }
        return zipped;
    }

    private static <T> T getOrNull(List<T> list, int index)
    {
        return list.size() > index ? list.get(index) : null;
    }

    public static class HierarchyLevel
    {
        private final Set<Class<?>> types;

        private HierarchyLevel(Iterable<Class<?>> types)
        {
            this.types = ImmutableSet.copyOf(types);
        }

        static HierarchyLevel singleton(Class<?> type)
        {
            return new HierarchyLevel(Lists.<Class<?>>newArrayList(type));
        }

        static HierarchyLevel level(Class<?>... types)
        {
            return new HierarchyLevel(Arrays.asList(types));
        }

        public Set<Class<?>> getTypes()
        {
            return types;
        }

        private static HierarchyLevel combine(HierarchyLevel l1, HierarchyLevel l2)
        {
            Set<Class<?>> combined = Sets.newLinkedHashSet();
            if (l1 != null)
            {
                combined.addAll(l1.types);
            }
            if (l2 != null)
            {
                combined.addAll(l2.types);
            }
            return new HierarchyLevel(combined);
        }

        @Override
        public String toString()
        {
            return types.toString();
        }


        @Override
        public int hashCode()
        {
            return Objects.hashCode(types);
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null || getClass() != obj.getClass())
            {
                return false;
            }
            final HierarchyLevel other = (HierarchyLevel) obj;
            return Objects.equal(this.types, other.types);
        }
    }
}
