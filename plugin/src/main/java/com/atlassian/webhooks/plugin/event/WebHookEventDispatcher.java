package com.atlassian.webhooks.plugin.event;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.webhooks.api.events.WebHookListenerCreatedEvent;
import com.atlassian.webhooks.api.events.WebHookListenerDeletedEvent;
import com.atlassian.webhooks.api.events.WebHookListenerEditedEvent;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;
import com.atlassian.webhooks.api.register.listener.WebHookListenerParameters;
import com.atlassian.webhooks.api.util.Filter;
import com.atlassian.webhooks.api.util.SectionKey;
import com.google.common.collect.Maps;

import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;

@Named("webHookEventDispatcher")
public class WebHookEventDispatcher
{
    private final EventPublisher eventPublisher;

    @Inject
    public WebHookEventDispatcher(EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }

    public void webHookCreated(PersistentWebHookListener webHook, RegistrationMethod registrationMethod)
    {
        eventPublisher.publish(
                new WebHookListenerCreatedEvent(webHook.getName(), webHook.getUrl(), webHook.getEvents(), parametersAsMap(webHook), registrationMethod)
        );
    }

    public void webHookDeleted(PersistentWebHookListener webHook, RegistrationMethod registrationMethod)
    {
        eventPublisher.publish(
                new WebHookListenerDeletedEvent(webHook.getName(), webHook.getUrl(), webHook.getEvents(), parametersAsMap(webHook), registrationMethod)
        );
    }

    public void webHookEdited(PersistentWebHookListener webHook, RegistrationMethod registrationMethod)
    {
        eventPublisher.publish(
                new WebHookListenerEditedEvent(webHook.getName(), webHook.getUrl(), webHook.getEvents(), parametersAsMap(webHook), registrationMethod)
        );
    }

    private static Map<String, String> parametersAsMap(PersistentWebHookListener listener)
    {
        Map<String, String> result = Maps.newHashMap();
        result.put(WebHookListenerParameters.Names.EXCLUDE_BODY, String.valueOf(listener.isExcludeBody()));
        for (Map.Entry<SectionKey, Filter> entry : listener.getFilters().entrySet())
        {
            result.put(entry.getKey() + "-" + WebHookListenerParameters.Names.FILTER, entry.getValue().getValue());
        }
        return result;
    }
}
