package com.atlassian.webhooks.plugin.legacy;

import com.atlassian.fugue.Option;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.spi.provider.PluginModuleListenerParameters;

import java.net.URI;
import java.util.Collections;
import java.util.List;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.webhooks.spi.plugin.WebHookModuleDescriptor.PROPERTY_KEYS;

final class LegacyToNewListenerTransformer
{
    private final PluginModuleListenerParameters consumerParams;

    private LegacyToNewListenerTransformer(final PluginModuleListenerParameters legacyParameters)
    {
        this.consumerParams = legacyParameters;
    }

    public static LegacyToNewListenerTransformer of(PluginModuleListenerParameters parameters)
    {
        return new LegacyToNewListenerTransformer(parameters);
    }

    public WebHookListener transform(String pluginKey, URI uri)
    {
        return WebHookListener.fromModuleDescriptor(pluginKey)
                .to(uri.toString())
                .excludeBody(getExcludeBody())
                .withFilter(getFilter().getOrElse(""))
                .withPropertyKeys(getPropertyKeys())
                .build();
    }

    private boolean getExcludeBody()
    {
        if (consumerParams.getParams().containsKey("excludeBody"))
        {
            return Boolean.valueOf((String) consumerParams.getParams().get("excludeBody"));
        }
        else if (consumerParams.getParams().containsKey("excludeIssueDetails"))
        {
            return Boolean.valueOf((String) consumerParams.getParams().get("excludeIssueDetails"));
        }
        else
        {
            return false;
        }
    }

    private Option<String> getFilter()
    {
        if (consumerParams.getParams().containsKey("filter"))
        {
            return Option.some((String) consumerParams.getParams().get("filter"));
        }
        else if (consumerParams.getParams().containsKey("jql"))
        {
            return Option.some((String) consumerParams.getParams().get("jql"));
        } else {
            return none();
        }
    }

    private List<String> getPropertyKeys() {
        if (consumerParams.getParams().containsKey(PROPERTY_KEYS)) {
            @SuppressWarnings("unchecked")
            List<String> propertyKeys = (List<String>) consumerParams.getParams().get(PROPERTY_KEYS);
            return propertyKeys;
        }
        else
        {
            return Collections.emptyList();
        }
    }
}
