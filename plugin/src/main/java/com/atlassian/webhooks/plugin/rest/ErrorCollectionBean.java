package com.atlassian.webhooks.plugin.rest;

import com.atlassian.sal.api.message.Message;
import com.atlassian.webhooks.api.util.MessageCollection;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class ErrorCollectionBean
{
    private final MessageCollection messageCollection;

    public ErrorCollectionBean(MessageCollection messageCollection)
    {
        this.messageCollection = messageCollection;
    }

    @JsonProperty
    public List<MessageBean> getMessages()
    {
        return Lists.newArrayList(Iterables.transform(messageCollection.getMessages(), new Function<Message, MessageBean>()
        {
            @Override
            public MessageBean apply(Message message)
            {
                return new MessageBean(message);
            }
        }));
    }

    @JsonIgnoreProperties (ignoreUnknown = true)
    public static class MessageBean
    {
        private final Message message;

        public MessageBean(final Message message)
        {
            this.message = message;
        }

        @JsonProperty
        public String getKey()
        {
            return message.getKey();
        }

        @JsonProperty
        public Serializable[] getArguments()
        {
            return message.getArguments();
        }
    }
}
