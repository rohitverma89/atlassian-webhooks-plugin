package com.atlassian.webhooks.plugin.module;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.util.concurrent.ResettableLazyReference;
import com.atlassian.webhooks.api.register.RegisteredWebHookEvent;
import com.atlassian.webhooks.api.register.WebHookEventGroup;
import com.atlassian.webhooks.api.register.WebHookEventSection;
import com.atlassian.webhooks.api.register.WebHookPluginRegistration;
import com.atlassian.webhooks.spi.EventSerializer;
import com.atlassian.webhooks.spi.RequestSigner;
import com.atlassian.webhooks.spi.RequestSigner2;
import com.atlassian.webhooks.spi.QueryParamsProvider;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import com.atlassian.webhooks.spi.WebHookListenerActionValidator;
import com.atlassian.webhooks.spi.WebHookPluginRegistrationFactory;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import javax.inject.Named;

import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;

@Named("webHookPluginRegistrationContainer")
public class WebHookPluginRegistrationContainerImpl implements WebHookPluginRegistrationContainer
{
    private final WebHookRegistry webHookRegistry = new WebHookRegistry();
    private final ConcurrentMap<ModuleKey, WebHookPluginRegistration> cachedRegistrations = new ConcurrentHashMap<ModuleKey, WebHookPluginRegistration>();
    private final ResettableLazyReference<Set<WebHookPluginRegistration>> webHookRegistrations;

    private final ResettableLazyReference<ClassSpecificProcessors<EventSerializer>> eventSerializers;
    private final ResettableLazyReference<ClassSpecificProcessors<UriVariablesProvider>> uriVariablesProviders;
    private final ResettableLazyReference<List<WebHookEventSection>> webHookSection;
    private final ResettableLazyReference<Set<RequestSigner>> requestSigners;
    private final ResettableLazyReference<Set<RequestSigner2>> requestSigners2;
    private final ResettableLazyReference<Set<QueryParamsProvider>> queryParamsProviders;
    private final ResettableLazyReference<Iterable<WebHookListenerActionValidator>> validators;

    @VisibleForTesting
    final DefaultPluginModuleTracker<WebHookPluginRegistrationFactory, WebHookRegistrationFactoryModuleDescriptor> tracker;

    @Autowired
    public WebHookPluginRegistrationContainerImpl(final PluginAccessor pluginAccessor, final PluginEventManager pluginEventManager)
    {
        tracker = new DefaultPluginModuleTracker<WebHookPluginRegistrationFactory, WebHookRegistrationFactoryModuleDescriptor>(
                pluginAccessor, pluginEventManager, WebHookRegistrationFactoryModuleDescriptor.class, new PluginModuleTracker.Customizer<WebHookPluginRegistrationFactory, WebHookRegistrationFactoryModuleDescriptor>()
        {
            @Override
            public WebHookRegistrationFactoryModuleDescriptor adding(final WebHookRegistrationFactoryModuleDescriptor descriptor)
            {
                ModuleKey moduleKey = ModuleKey.of(descriptor);
                final WebHookPluginRegistration pluginRegistration = descriptor.getModule().createPluginRegistration();
                addRegistration(moduleKey, pluginRegistration);
                return descriptor;
            }

            @Override
            public void removed(final WebHookRegistrationFactoryModuleDescriptor descriptor)
            {
                ModuleKey moduleKey = ModuleKey.of(descriptor);
                removeRegistration(moduleKey);
            }
        });

        webHookRegistrations = new ResettableLazyReference<Set<WebHookPluginRegistration>>()
        {
            @Override
            protected Set<WebHookPluginRegistration> create() throws Exception
            {
                return ImmutableSet.copyOf(cachedRegistrations.values());
            }
        };

        eventSerializers = getReferenceToProcessors("Event Serializer", new Function<WebHookPluginRegistration, Map<Class, EventSerializer>>()
        {
            @Override
            public Map<Class, EventSerializer> apply(WebHookPluginRegistration registration)
            {
                return registration.getEventSerializers();
            }
        });

        uriVariablesProviders = getReferenceToProcessors("Uri Variable Provider", new Function<WebHookPluginRegistration, Map<Class, UriVariablesProvider>>()
        {
            @Override
            public Map<Class, UriVariablesProvider> apply(WebHookPluginRegistration registration)
            {
                return registration.getUriVariablesProviders();
            }
        });

        webHookSection = new ResettableLazyReference<List<WebHookEventSection>>()
        {
            @Override
            protected List<WebHookEventSection> create() throws Exception
            {
                return ImmutableList.copyOf(concat(transform(cachedRegistrations.values(), new Function<WebHookPluginRegistration, List<WebHookEventSection>>()
                {
                    @Override
                    public List<WebHookEventSection> apply(final WebHookPluginRegistration webHookPluginRegistration)
                    {
                        return webHookPluginRegistration.getSections();
                    }
                })));
            }
        };

        requestSigners = new ResettableLazyReference<Set<RequestSigner>>()
        {
            @Override
            protected Set<RequestSigner> create() throws Exception
            {
                return ImmutableSet.copyOf(concat(transform(cachedRegistrations.values(), new Function<WebHookPluginRegistration, Set<RequestSigner>>()
                {
                    @Override
                    public Set<RequestSigner> apply(final WebHookPluginRegistration registration)
                    {
                        return registration.getRequestSigners();
                    }
                })));
            }
        };

        requestSigners2 = new ResettableLazyReference<Set<RequestSigner2>>()
        {
            @Override
            protected Set<RequestSigner2> create() throws Exception
            {
                return ImmutableSet.copyOf(concat(transform(cachedRegistrations.values(), new Function<WebHookPluginRegistration, Set<RequestSigner2>>()
                {
                    @Override
                    public Set<RequestSigner2> apply(final WebHookPluginRegistration registration)
                    {
                        return registration.getRequestSigners2();
                    }
                })));
            }
        };

        validators = new ResettableLazyReference<Iterable<WebHookListenerActionValidator>>()
        {

            @Override
            protected Iterable<WebHookListenerActionValidator> create() throws Exception
            {
                return ImmutableSet.copyOf(concat(transform(cachedRegistrations.values(), new Function<WebHookPluginRegistration, Set<WebHookListenerActionValidator>>()
                {
                    @Override
                    public Set<WebHookListenerActionValidator> apply(final WebHookPluginRegistration registration)
                    {
                        return registration.getValidators();
                    }
                })));
            }
        };
        queryParamsProviders = new ResettableLazyReference<Set<QueryParamsProvider>>()
        {
            @Override
            protected Set<QueryParamsProvider> create() throws Exception
            {
                return ImmutableSet.copyOf(concat(transform(cachedRegistrations.values(), new Function<WebHookPluginRegistration, Set<QueryParamsProvider>>()
                {
                    @Override
                    public Set<QueryParamsProvider> apply(final WebHookPluginRegistration registration)
                    {
                        return registration.getQueryParamsProviders();
                    }
                })));
            }
        };
        ;
    }

    @Override
    public void removeRegistration(final ModuleKey moduleKey)
    {
        cachedRegistrations.remove(moduleKey);
        webHookRegistry.remove(moduleKey);
        resetLazyReferences();
    }

    @Override
    public void addRegistration(final ModuleKey moduleKey, final WebHookPluginRegistration pluginRegistration)
    {
        cachedRegistrations.put(moduleKey, pluginRegistration);
        webHookRegistry.add(moduleKey, cachedRegistrations.get(moduleKey));
        resetLazyReferences();
    }

    private <T> ResettableLazyReference<ClassSpecificProcessors<T>> getReferenceToProcessors(final String name, final Function<WebHookPluginRegistration, Map<Class, T>> getProcessors)
    {
        return new ResettableLazyReference<ClassSpecificProcessors<T>>()
        {
            @Override
            protected ClassSpecificProcessors<T> create() throws Exception
            {
                return getProcessors(name, getProcessors);
            }
        };
    }

    private <T> ClassSpecificProcessors<T> getProcessors(String name, Function<WebHookPluginRegistration, Map<Class, T>> getProcessors)
    {
        ClassSpecificProcessors.Builder<T> processors = ClassSpecificProcessors.builder(name);
        for (WebHookPluginRegistration registration : cachedRegistrations.values())
        {
            for (Map.Entry<Class, T> processorEntry : getProcessors.apply(registration).entrySet())
            {
                processors.add(processorEntry.getKey(), processorEntry.getValue());
            }
        }
        return processors.build();
    }

    @Override
    public Set<WebHookPluginRegistration> getWebHookRegistrations()
    {
        return webHookRegistrations.get();
    }

    @Override
    public ClassSpecificProcessors<EventSerializer> getEventSerializers()
    {
        return eventSerializers.get();
    }

    @Override
    public ClassSpecificProcessors<UriVariablesProvider> getUriVariablesProviders()
    {
        return uriVariablesProviders.get();
    }

    @Override
    public WebHookRegistry getWebHookRegistry()
    {
        return webHookRegistry;
    }

    @Override
    public List<WebHookEventSection> getWebHookSections()
    {
        return webHookSection.get();
    }

    @Override
    public Set<RequestSigner> getRequestSigners()
    {
        return requestSigners.get();
    }

    @Override
    public Set<RequestSigner2> getRequestSigners2()
    {
        return requestSigners2.get();
    }


    @Override
    public Iterable<RegisteredWebHookEvent> getAllWebhooks()
    {
        return concat(transform(getWebHookSections(), new Function<WebHookEventSection, Iterable<RegisteredWebHookEvent>>()
        {
            @Override
            public Iterable<RegisteredWebHookEvent> apply(final WebHookEventSection input)
            {
                return concat(transform(input.getGroups(), new Function<WebHookEventGroup, Iterable<RegisteredWebHookEvent>>()
                {
                    @Override
                    public Iterable<RegisteredWebHookEvent> apply(final WebHookEventGroup input)
                    {
                        return input.getEvents();
                    }
                }));
            }
        }));
    }

    @Override
    public Iterable<WebHookListenerActionValidator> getValidators()
    {
        return validators.get();
    }

    public Set<QueryParamsProvider> getQueryParamsProviders()
    {
        return queryParamsProviders.get();
    }

    private void resetLazyReferences()
    {
        webHookRegistrations.reset();
        eventSerializers.reset();
        uriVariablesProviders.reset();
        webHookSection.reset();
        requestSigners.reset();
        validators.reset();
    }
}
