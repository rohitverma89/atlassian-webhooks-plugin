package com.atlassian.webhooks.plugin;

import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.spi.WebHookListenerAccessVoter;
import com.atlassian.webhooks.api.util.Channel;
import com.atlassian.webhooks.api.util.CompositeVote;
import com.atlassian.webhooks.api.util.Vote;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import javax.annotation.Nonnull;

public class OsgiWebHookListenerAccessVoter implements WebHookListenerAccessVoter
{
    private final WebHookListenerAccessVoter defaultVoter;
    private final ServiceTracker tracker;

    public OsgiWebHookListenerAccessVoter(BundleContext bundleContext, WebHookListenerAccessVoter defaultVoter)
    {
        this.defaultVoter = defaultVoter;

        tracker = createServiceTracker(bundleContext);
        tracker.open();
    }

    @Nonnull
    @Override
    public Vote canAdmin(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel)
    {
        CompositeVote compositeVote = new CompositeVote();
        for (WebHookListenerAccessVoter voter : getVoters())
        {
            compositeVote.add(voter.canAdmin(parameters, channel));
        }
        return compositeVote.getOutcome();
    }

    @Nonnull
    @Override
    public Vote canCreate(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel)
    {
        CompositeVote compositeVote = new CompositeVote();
        for (WebHookListenerAccessVoter voter : getVoters())
        {
            compositeVote.add(voter.canCreate(parameters, channel));
        }
        return compositeVote.getOutcome();
    }

    @Nonnull
    @Override
    public Vote canRead(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel)
    {
        CompositeVote compositeVote = new CompositeVote();
        for (WebHookListenerAccessVoter voter : getVoters())
        {
            compositeVote.add(voter.canRead(parameters, channel));
        }
        return compositeVote.getOutcome();
    }

    @Nonnull
    @Override
    public Vote canPublish(@Nonnull WebHookEvent webHookEvent, @Nonnull WebHookListener listener)
    {
        CompositeVote compositeVote = new CompositeVote();
        for (WebHookListenerAccessVoter voter : getVoters())
        {
            compositeVote.add(voter.canPublish(webHookEvent, listener));
        }
        return compositeVote.getOutcome();
    }

    public void destroy() {
        tracker.close();
    }

    protected ServiceTracker createServiceTracker(BundleContext bundleContext)
    {
        return new ServiceTracker(bundleContext, WebHookListenerAccessVoter.class.getName(), null);
    }

    private WebHookListenerAccessVoter[] getVoters()
    {
        Object[] services = tracker.getServices();

        int pluginVoterCount = services != null ? services.length : 0;
        WebHookListenerAccessVoter[] voters = new WebHookListenerAccessVoter[pluginVoterCount + 1];
        for (int i = 0; i < pluginVoterCount; ++i)
        {
            voters[i] = WebHookListenerAccessVoter.class.cast(services[i]);
        }
        // always include the defaultVoter
        voters[pluginVoterCount] = defaultVoter;
        return voters;
    }

}
