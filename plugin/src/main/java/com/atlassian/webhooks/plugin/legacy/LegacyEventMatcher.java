package com.atlassian.webhooks.plugin.legacy;

import com.atlassian.webhooks.spi.provider.EventMatcher;

public interface LegacyEventMatcher<T> extends EventMatcher, com.atlassian.webhooks.spi.EventMatcher<T> {}
