package com.atlassian.webhooks.plugin.uri;

import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;

import java.net.URI;

/**
 * Resolves the URI of the webhook listener.
 */
public interface UriVariablesResolver
{
    /**
     * Resolves the URI or webhook listener. The uri may be different depending on the event value. For instance, the listener
     * path may specify variable parameters in URI, these variable parameters are later replaced with values from the event.
     *
     * @param listener to which the uri the event will be published.
     * @param event which is going to be published.
     * @return resolved uri.
     */
    URI resolve(WebHookListener listener, WebHookEvent event);
}
