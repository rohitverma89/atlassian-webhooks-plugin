package com.atlassian.webhooks.plugin.uri;

import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import com.google.common.collect.ImmutableMap;
import org.osgi.framework.InvalidSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

@Named("uriVariablesResolver")
public class UriVariablesResolverImpl implements UriVariablesResolver
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final UrlVariableSubstitutor urlVariableSubstitutor;
    private final PluggableUriResolver pluggableUriResolver;
    private final WebHookPluginRegistrationContainer registrationContainer;

    @Autowired
    public UriVariablesResolverImpl(final UrlVariableSubstitutor urlVariableSubstitutor,
                                    final PluggableUriResolver pluggableUriResolver,
                                    final WebHookPluginRegistrationContainer registrationContainer)
    {
        this.urlVariableSubstitutor = urlVariableSubstitutor;
        this.pluggableUriResolver = pluggableUriResolver;
        this.registrationContainer = registrationContainer;
    }

    @Override
    public URI resolve(final WebHookListener listener, final WebHookEvent event)
    {
        try
        {
            final String substitutedUrl = substitute(listener.getParameters().getUrl(), event.getEvent());
            try
            {
                URI uri = new URI(substitutedUrl);
                return pluggableUriResolver.resolve(listener.getRegistrationDetails(), uri);
            }
            catch (URISyntaxException ex)
            {
                logger.error("Could not resolve uri for event '{}' and listener '{}'", event, listener);
                throw new RuntimeException("Could not resolve uri for event " + event + " and listener " + listener, ex);
            }
            catch (InvalidSyntaxException ex)
            {
                logger.error("Could not resolve uri for event '{}' and listener '{}'", event, listener);
                throw new RuntimeException("Could not resolve uri for event " + event + " and listener " + listener, ex);
            }
        }
        catch (Error error)
        {
            error.printStackTrace();
            return null;
        }
    }

    private String substitute(final String path, final Object event)
    {
        final Iterable<UriVariablesProvider> variableProviders = registrationContainer.getUriVariablesProviders().allForType(event.getClass());
        Map<String, Object> context = buildContext(variableProviders, event);
        return urlVariableSubstitutor.replace(path, context);
    }

    private Map<String, Object> buildContext(final Iterable<UriVariablesProvider> webHookUriSubstitutions, final Object event)
    {
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        for (UriVariablesProvider uriVariablesProvider : webHookUriSubstitutions)
        {
            builder.putAll(uriVariablesProvider.uriVariables(event));
        }
        return builder.build();
    }

}
