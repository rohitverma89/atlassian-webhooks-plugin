package com.atlassian.webhooks.plugin;

import com.atlassian.webhooks.api.publish.WebHookEvent;

public interface WebHookPublisher
{
    void publish(WebHookEvent webHookEvent);
}
