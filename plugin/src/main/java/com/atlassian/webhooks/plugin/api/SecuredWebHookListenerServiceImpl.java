package com.atlassian.webhooks.plugin.api;

import com.atlassian.fugue.Effect;
import com.atlassian.fugue.Option;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerService;
import com.atlassian.webhooks.api.register.listener.WebHookListenerServiceResponse;
import com.atlassian.webhooks.api.util.Channel;
import com.atlassian.webhooks.api.util.MessageCollection;
import com.atlassian.webhooks.plugin.event.WebHookEventDispatcher;
import com.atlassian.webhooks.plugin.store.WebHookListenerCachingStore;
import com.atlassian.webhooks.spi.WebHookListenerAccessVoter;
import com.atlassian.webhooks.spi.WebHookListenerActionValidator;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;

import static com.atlassian.webhooks.api.register.listener.WebHookListenerServiceResponse.error;
import static com.atlassian.webhooks.api.util.ListenerDuplicatePredicate.duplicateOf;
import static com.atlassian.webhooks.api.util.MessageCollection.Reason.FORBIDDEN;
import static com.atlassian.webhooks.api.util.MessageCollection.Reason.NOT_FOUND;
import static com.google.common.base.Preconditions.checkNotNull;

@Named ("securedWebHookListenerService")
public class SecuredWebHookListenerServiceImpl implements SecuredWebHookListenerService
{
    private final WebHookListenerAccessVoter accessVoter;
    private final I18nResolver i18n;
    private final WebHookListenerActionValidator webHookListenerActionValidator;
    private final WebHookListenerCachingStore webHookListenerCachingStore;
    private final WebHookEventDispatcher webHookEventDispatcher;

    @Inject
    public SecuredWebHookListenerServiceImpl(WebHookListenerAccessVoter accessVoter, I18nResolver i18nResolver,
            WebHookListenerActionValidator webHookListenerActionValidator,
            WebHookListenerCachingStore webHookListenerCachingStore,
            WebHookEventDispatcher webHookEventDispatcher)
    {
        this.accessVoter = checkNotNull(accessVoter, "accessVoter");
        this.i18n = checkNotNull(i18nResolver, "i18nResolver");
        this.webHookListenerActionValidator = checkNotNull(webHookListenerActionValidator, "webHookListenerActionValidator");
        this.webHookListenerCachingStore = checkNotNull(webHookListenerCachingStore, "webHookListenerCachingStore");
        this.webHookEventDispatcher = checkNotNull(webHookEventDispatcher, "webHookEventDispatcher");
    }

    @Override
    public Iterable<PersistentWebHookListener> getAllWebHookListeners(final @Nonnull Channel channel)
    {
        checkNotNull(channel, "channel");
        return Iterables.filter(webHookListenerCachingStore.getAllWebHookListeners(), new Predicate<PersistentWebHookListener>()
        {
            @Override
            public boolean apply(PersistentWebHookListener input)
            {
                return accessVoter.canRead(input, channel).isAllowed();
            }
        });
    }

    @Override
    public Option<PersistentWebHookListener> getWebHookListener(@Nonnull final Channel channel, int id)
    {
        checkNotNull(channel, "channel");
        return webHookListenerCachingStore.getWebHookListener(checkNotNull(id, "id")).flatMap(new Function<PersistentWebHookListener, Option<PersistentWebHookListener>>()
        {
            @Override
            public Option<PersistentWebHookListener> apply(final PersistentWebHookListener webHookListener)
            {
                return accessVoter.canRead(webHookListener, channel).isAllowed() ? Option.some(webHookListener) : Option.<PersistentWebHookListener>none();
            }
        });
    }

    @Override
    public WebHookListenerServiceResponse registerWebHookListener(@Nonnull Channel channel,
            @Nonnull PersistentWebHookListener listener)
    {
        checkNotNull(channel, "channel");
        checkNotNull(listener, "listener");

        final MessageCollection messageCollection = validateCreateWebHookListener(channel, listener);

        if (!messageCollection.isEmpty())
        {
            return error(messageCollection);
        }
        else
        {
            final PersistentWebHookListener registeredListener = webHookListenerCachingStore.registerWebHookListener(listener, channel.toRegistrationMethod());
            webHookEventDispatcher.webHookCreated(registeredListener, channel.toRegistrationMethod());
            return WebHookListenerServiceResponse.ok(registeredListener);
        }
    }

    @Override
    public WebHookListenerServiceResponse updateWebHookListener(@Nonnull final Channel channel, final int id,
            @Nonnull final WebHookListenerService.WebHookListenerUpdateInput listenerUpdateParams)
    {
        checkNotNull(channel, "channel");
        checkNotNull(listenerUpdateParams, "listener");

        return getWebHookListener(channel, id).fold(new Supplier<WebHookListenerServiceResponse>()
        {
            @Override
            public WebHookListenerServiceResponse get()
            {
                return error(MessageCollection.of(i18n.getText("webhooks.does.not.exist", id), NOT_FOUND));
            }
        }, new Function<PersistentWebHookListener, WebHookListenerServiceResponse>()
        {
            @Override
            public WebHookListenerServiceResponse apply(final PersistentWebHookListener existingListener)
            {
                PersistentWebHookListener listenerToUpdate = createListenerToUpdate(existingListener, listenerUpdateParams);
                final MessageCollection messageCollection = validateUpdateWebHookListener(channel, listenerToUpdate);
                if (!messageCollection.isEmpty())
                {
                    return error(messageCollection);
                }
                else
                {
                    final PersistentWebHookListener updatedListener = webHookListenerCachingStore.updateWebHookListener(listenerToUpdate);
                    webHookEventDispatcher.webHookEdited(updatedListener, channel.toRegistrationMethod());

                    return WebHookListenerServiceResponse.ok(updatedListener);
                }
            }
        });
    }

    @Override
    public MessageCollection deleteWebHookListener(@Nonnull Channel channel, int id)
    {
        checkNotNull(channel, "channel");

        final Option<PersistentWebHookListener> webHookListener = getWebHookListener(channel, id);

        final MessageCollection messageCollection = validateDeleteWebHookListener(channel, id, webHookListener);

        if (messageCollection.isEmpty())
        {
            webHookListenerCachingStore.removeWebHookListener(id);
            webHookEventDispatcher.webHookDeleted(webHookListener.get(), channel.toRegistrationMethod());
        }
        return messageCollection;
    }

    private MessageCollection validateCreateWebHookListener(@Nonnull Channel channel, @Nonnull PersistentWebHookListener listener)
    {
        if (!accessVoter.canCreate(listener, channel).isAllowed())
        {
            return MessageCollection.of(i18n.getText("webhooks.create.insufficient.privileges", listener.getName()), FORBIDDEN);
        }

        final MessageCollection.Builder builder = MessageCollection.builder();
        validateUniqueRegistration(listener).foreach(new Effect<MessageCollection>()
        {
            @Override
            public void apply(final MessageCollection messageCollection)
            {
                builder.addAll(messageCollection);
            }
        });

        builder.addAll(webHookListenerActionValidator.validateWebHookRegistration(listener));


        return builder.build();
    }

    private MessageCollection validateUpdateWebHookListener(final Channel channel, final PersistentWebHookListener listener)
    {
        final MessageCollection.Builder builder = MessageCollection.builder();
        validateUniqueRegistration(checkNotNull(listener, "listener")).foreach(new Effect<MessageCollection>()
        {
            @Override
            public void apply(final MessageCollection messageCollection)
            {
                builder.addAll(messageCollection);
            }
        });
        builder.addAll(webHookListenerActionValidator.validateWebHookUpdate(listener));
        if (!canAdmin(channel, listener.getId().get()))
        {
            builder.addMessage(i18n.getText("webhooks.update.insufficient.privileges", listener.getName()), FORBIDDEN);
        }
        return builder.build();
    }

    private MessageCollection validateDeleteWebHookListener(@Nonnull final Channel channel, final int id, @Nonnull final Option<PersistentWebHookListener> webHookListener)
    {

        if (webHookListener.isEmpty())
        {
            return MessageCollection.of(i18n.getText("webhooks.does.not.exist", id), NOT_FOUND);
        }

        MessageCollection.Builder builder = MessageCollection.builder();

        builder.addAll(webHookListenerActionValidator.validateWebHookRemoval(webHookListener.get()));
        if (!accessVoter.canAdmin(webHookListener.get(), channel).isAllowed())
        {
            builder.addMessage(i18n.createMessage("webhooks.delete.insufficient.privileges", webHookListener.get().getName()), FORBIDDEN);
        }
        return builder.build();
    }

    private boolean canAdmin(Channel channel, int id)
    {
        Option<PersistentWebHookListener> parameters = getWebHookListener(channel, id);
        return parameters.isDefined() && accessVoter.canAdmin(parameters.get(), channel).isAllowed();
    }

    private PersistentWebHookListener createListenerToUpdate(final PersistentWebHookListener existingListener,
            final WebHookListenerService.WebHookListenerUpdateInput listenerUpdateParams)
    {
        final PersistentWebHookListener.Builder updatedListenerBuilder = PersistentWebHookListener.existing(existingListener.getId().get())
                .setEnabled(listenerUpdateParams.getEnabled().getOrElse(existingListener.isEnabled()))
                .setUrl(listenerUpdateParams.getUrl().getOrElse(existingListener.getUrl()))
                .setListenerName(listenerUpdateParams.getName().getOrElse(existingListener.getName()))
                .setDescription(listenerUpdateParams.getDescription().getOrElse(existingListener.getDescription()))
                .setExcludeBody(listenerUpdateParams.getExcludeBody().getOrElse(existingListener.isExcludeBody()))
                .addWebHookIds(listenerUpdateParams.getEvents().getOrElse(existingListener.getEvents()));
        if (listenerUpdateParams.getFilters().isDefined())
        {
            updatedListenerBuilder.addFilters(listenerUpdateParams.getFilters().get());
        }
        else
        {
            updatedListenerBuilder.addFiltersTypeRich(existingListener.getFilters());
        }
        return updatedListenerBuilder.build();
    }

    private Option<MessageCollection> validateUniqueRegistration(PersistentWebHookListener listener)
    {
        return Option.option(Iterables.find(webHookListenerCachingStore.getAllWebHookListeners(), duplicateOf(listener), null)).flatMap(new Function<PersistentWebHookListener, Option<? extends MessageCollection>>()
        {
            @Override
            public Option<? extends MessageCollection> apply(final PersistentWebHookListener listener)
            {
                return Option.some(MessageCollection.of(i18n.getText("webhooks.submit.duplicate.info"), MessageCollection.Reason.CONFLICT));
            }
        });
    }
}
