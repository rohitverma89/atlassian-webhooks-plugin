package com.atlassian.webhooks.plugin.validator;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.util.ErrorMessage;
import com.atlassian.webhooks.api.util.MessageCollection;
import com.atlassian.webhooks.plugin.module.CloudConditionResolver;
import com.atlassian.webhooks.plugin.uri.UrlVariableSubstitutor;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import javax.inject.Named;

import static com.atlassian.webhooks.api.util.MessageCollection.Reason.VALIDATION_FAILED;

/**
 * Validates if the given url:
 *  - contains allowed URI substitution template
 *  - is targeting one of the allowed schemes (http, https)
 *  - for our cloud offer, is using one of the opened ports: 80 or 443
 */
@Named("urlValidator")
public class UrlValidator
{
    private static final String URL_FIELD = "url";
    private static final List<String> ALLOWED_SCHEMES = ImmutableList.of("HTTP", "HTTPS");
    private static final Map<String, Integer> ALLOWED_CLOUD_SCHEMES = ImmutableMap.of("HTTP", 80, "HTTPS", 443);

    private final UrlVariableSubstitutor urlVariableSubstitutor;
    private final I18nResolver i18n;
    private final CloudConditionResolver cloudConditionResolver;

    @Autowired
    public UrlValidator(final UrlVariableSubstitutor urlVariableSubstitutor,
            final I18nResolver i18n,
            final CloudConditionResolver cloudConditionResolver)
    {
        this.urlVariableSubstitutor = urlVariableSubstitutor;
        this.i18n = i18n;
        this.cloudConditionResolver = cloudConditionResolver;
    }

    public MessageCollection validate(final PersistentWebHookListener registrationParameters)
    {
        final MessageCollection.Builder builder = MessageCollection.builder();
        final String registrationUrl = registrationParameters.getUrl();

        if (StringUtils.isBlank(registrationUrl))
        {
            return builder.addMessage(new ErrorMessage(URL_FIELD, i18n.getText("webhooks.empty.url")), VALIDATION_FAILED).build();
        }
        final String replacedUri = urlVariableSubstitutor.replace(registrationUrl, ImmutableMap.<String, Object>of());
        try
        {
            final URI uri = new URI(replacedUri);
            if (!isProtocolValid(uri))
            {
                builder.addMessage(new ErrorMessage(URL_FIELD, i18n.getText("webhooks.invalid.url.protocol", registrationUrl)), VALIDATION_FAILED);
            }
            if (!isPortValid(uri))
            {
                builder.addMessage(new ErrorMessage(URL_FIELD, i18n.getText("webhooks.invalid.url.ondemand.ports")), VALIDATION_FAILED);
            }

        }
        catch (URISyntaxException e)
        {
            builder.addMessage(new ErrorMessage(URL_FIELD, i18n.getText("webhooks.invalid.url", registrationUrl)), VALIDATION_FAILED);
        }
        return builder.build();
    }

    private boolean isPortValid(final URI uri)
    {
        if (cloudConditionResolver.isCloud())
        {
            final Integer portForScheme = ALLOWED_CLOUD_SCHEMES.get(Objects.firstNonNull(uri.getScheme(), "").toUpperCase());
            final Integer port = uri.getPort();
            return port.equals(portForScheme) || port == -1;
        }
        return true;
    }

    private boolean isProtocolValid(final URI uri)
    {
        final String scheme = uri.getScheme();
        return scheme != null && ALLOWED_SCHEMES.contains(scheme.toUpperCase());
    }
}
