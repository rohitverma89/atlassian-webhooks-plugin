package com.atlassian.webhooks.plugin.store;

import com.atlassian.fugue.Option;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;
import com.atlassian.webhooks.spi.WebHookListenerStore;
import com.google.common.base.Function;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import java.util.Collection;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Delegates all store methods to the service found in the bundleContext.
 */
public class OsgiWebHookListenerStore implements WebHookListenerStore
{
    private final BundleContext bundleContext;
    private final DefaultWebHookListenerStore defaultListenerStore;

    public OsgiWebHookListenerStore(final BundleContext bundleContext, final DefaultWebHookListenerStore defaultListenerStore)
    {
        this.bundleContext = checkNotNull(bundleContext);
        this.defaultListenerStore = checkNotNull(defaultListenerStore);
    }

    @Override
    public PersistentWebHookListener addWebHook(final PersistentWebHookListener listener, final RegistrationMethod registrationMethod)
    {
        return executeStoreFunction(new Function<WebHookListenerStore, PersistentWebHookListener>()
        {
            @Override
            public PersistentWebHookListener apply(final WebHookListenerStore store)
            {
                return store.addWebHook(listener, registrationMethod);
            }
        });
    }

    @Override
    public PersistentWebHookListener updateWebHook(final PersistentWebHookListener webHookListener)
    {
        return executeStoreFunction(new Function<WebHookListenerStore, PersistentWebHookListener>()
        {
            @Override
            public PersistentWebHookListener apply(final WebHookListenerStore store)
            {
                return store.updateWebHook(webHookListener);
            }
        });
    }

    @Override
    public Option<PersistentWebHookListener> getWebHook(final int id)
    {
        return executeStoreFunction(new Function<WebHookListenerStore, Option<PersistentWebHookListener>>()
        {
            @Override
            public Option<PersistentWebHookListener> apply(WebHookListenerStore store)
            {
                return store.getWebHook(id);
            }
        });
    }

    @Override
    public void removeWebHook(final int id)
    {
        executeStoreFunction(new Function<WebHookListenerStore, Void>()
        {
            @Override
            public Void apply(final WebHookListenerStore store)
            {
                store.removeWebHook(id);
                return null;
            }
        });
    }

    @Override
    public Collection<PersistentWebHookListener> getAllWebHooks()
    {
        return executeStoreFunction(new Function<WebHookListenerStore, Collection<PersistentWebHookListener>>()
        {
            @Override
            public Collection<PersistentWebHookListener> apply(final WebHookListenerStore store)
            {
                return store.getAllWebHooks();
            }
        });
    }

    private <T> T executeStoreFunction(Function<WebHookListenerStore, T> storeFunction)
    {
        ServiceReference serviceReference = bundleContext.getServiceReference(WebHookListenerStore.class.getName());
        if (serviceReference != null)
        {
            try
            {
                WebHookListenerStore webHookListenerStore = (WebHookListenerStore) bundleContext.getService(serviceReference);
                return storeFunction.apply(webHookListenerStore);
            }
            finally
            {
                bundleContext.ungetService(serviceReference);
            }
        }
        else
        {
            return storeFunction.apply(defaultListenerStore);
        }
    }
}
