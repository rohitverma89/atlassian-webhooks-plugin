package com.atlassian.webhooks.plugin.module;

import com.atlassian.webhooks.api.register.RegisteredWebHookEvent;
import com.atlassian.webhooks.api.register.WebHookEventSection;
import com.atlassian.webhooks.api.register.WebHookPluginRegistration;
import com.atlassian.webhooks.spi.EventSerializer;
import com.atlassian.webhooks.spi.RequestSigner;
import com.atlassian.webhooks.spi.RequestSigner2;
import com.atlassian.webhooks.spi.QueryParamsProvider;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import com.atlassian.webhooks.spi.WebHookListenerActionValidator;

import java.util.List;
import java.util.Set;

/**
 * Holds all modules which registered webhooks.
 */
public interface WebHookPluginRegistrationContainer
{
    void removeRegistration(ModuleKey moduleKey);

    void addRegistration(ModuleKey moduleKey, WebHookPluginRegistration pluginRegistration);

    Set<WebHookPluginRegistration> getWebHookRegistrations();

    ClassSpecificProcessors<EventSerializer> getEventSerializers();

    ClassSpecificProcessors<UriVariablesProvider> getUriVariablesProviders();

    WebHookRegistry getWebHookRegistry();

    List<WebHookEventSection> getWebHookSections();

    Set<RequestSigner> getRequestSigners();

    Set<RequestSigner2> getRequestSigners2();

    Set<QueryParamsProvider> getQueryParamsProviders();

    Iterable<RegisteredWebHookEvent> getAllWebhooks();

    Iterable<WebHookListenerActionValidator> getValidators();
}
