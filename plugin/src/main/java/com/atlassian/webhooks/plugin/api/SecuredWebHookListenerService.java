package com.atlassian.webhooks.plugin.api;

import com.atlassian.fugue.Option;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerService;
import com.atlassian.webhooks.api.register.listener.WebHookListenerServiceResponse;
import com.atlassian.webhooks.api.util.Channel;
import com.atlassian.webhooks.api.util.MessageCollection;

import java.util.Collection;
import java.util.Map;
import javax.annotation.Nonnull;

/**
 * Secure version of {@link com.atlassian.webhooks.api.register.listener.WebHookListenerService} that applies
 * authorisation. Each of the methods takes a {@link Channel channel} to support channel-specific authorisation rules.
 * Plugins that can provide their own {@link com.atlassian.webhooks.spi.WebHookListenerAccessVoter} to participate in
 * the authorisation logic.
 */
public interface SecuredWebHookListenerService
{

    /**
     * Returns all WebHook listeners.
     *
     * @param channel REST, SERVICE or UI
     * @return a collection of WebHook listeners.
     */
    Iterable<PersistentWebHookListener> getAllWebHookListeners(@Nonnull Channel channel);

    /**
     * Returns a WebHook listener with given id.
     *
     * @param channel REST, SERVICE or UI
     * @param id The WebHook listener id.
     * @return The WebHook listener for given id, else None.
     */
    Option<PersistentWebHookListener> getWebHookListener(@Nonnull Channel channel, int id);

    /**
     * Registers a new WebHook listener.
     *
     * @param channel REST, SERVICE or UI
     * @param registrationParameters The parameters of WebHook listener to register.
     * @return parameters of the registered WebHook listener or message collection.
     */
    WebHookListenerServiceResponse registerWebHookListener(@Nonnull Channel channel,
            @Nonnull PersistentWebHookListener registrationParameters);

    /**
     * Updates a WebHook listener with given id.
     *
     * @param channel REST, SERVICE or UI
     * @param registrationParameters The parameters of WebHook listener to update.
     * @return parameters of the updated WebHook listener.
     */
    WebHookListenerServiceResponse updateWebHookListener(@Nonnull Channel channel, int id,
            @Nonnull WebHookListenerService.WebHookListenerUpdateInput registrationParameters);

    /**
     * Deletes a WebHook listener with given id.
     *
     * @param channel REST, SERVICE or UI
     * @param id Id of WebHook listener to remove.
     * @return a message collection with validation errors.
     * @throws IllegalArgumentException if WebHook with given id doesn't exist.
     */
    MessageCollection deleteWebHookListener(@Nonnull Channel channel, int id);

}
