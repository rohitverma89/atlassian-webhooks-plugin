package com.atlassian.webhooks.plugin.module;

import com.atlassian.webhooks.api.util.TypeRichString;

public class WebHookKey extends TypeRichString
{
    public WebHookKey(final String value)
    {
        super(value);
    }
}
