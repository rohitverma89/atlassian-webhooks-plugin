package com.atlassian.webhooks.plugin.store;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.fugue.Option;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;
import com.atlassian.webhooks.api.util.Filter;
import com.atlassian.webhooks.api.util.SectionKey;
import com.atlassian.webhooks.spi.WebHookListenerStore;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.java.ao.DBParam;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;

public class DefaultWebHookListenerStore implements WebHookListenerStore
{
    private final ActiveObjects ao;
    private final UserManager userManager;
    private final I18nResolver i18n;

    public DefaultWebHookListenerStore(final ActiveObjects ao, final UserManager userManager, final I18nResolver i18n)
    {
        this.ao = ao;
        this.userManager = userManager;
        this.i18n = i18n;
    }

    @Override
    public PersistentWebHookListener addWebHook(final PersistentWebHookListener listener, final RegistrationMethod registrationMethod)
    {

        final WebHookListenerAO webHookListenerAO = ao.executeInTransaction(new TransactionCallback<WebHookListenerAO>()
        {
            @Override
            public WebHookListenerAO doInTransaction()
            {
                final WebHookListenerAO webHookListenerAO = ao.create(WebHookListenerAO.class,
                        new DBParam("LAST_UPDATED_USER", getUserKey()),
                        new DBParam("URL", listener.getUrl()),
                        new DBParam("LAST_UPDATED", new Date()),
                        new DBParam("NAME", listener.getName()),
                        new DBParam("DESCRIPTION", listener.getDescription()),
                        new DBParam("EXCLUDE_BODY", listener.isExcludeBody()),
                        new DBParam("FILTERS", DbParamMarshaler.marshalFilters(listener.getFilters())),
                        new DBParam("REGISTRATION_METHOD", registrationMethod.toString()),
                        new DBParam("EVENTS", DbParamMarshaler.marshalEvents(listener.getEvents())),
                        new DBParam("ENABLED", listener.isEnabled())
                );
                webHookListenerAO.save();
                return webHookListenerAO;
            }
        });
        return createWebHookListenerParameters(webHookListenerAO);
    }

    @Override
    public PersistentWebHookListener updateWebHook(final PersistentWebHookListener listener)
    {
        final WebHookListenerAO updatedWebHookListener = ao.executeInTransaction(new TransactionCallback<WebHookListenerAO>()
        {
            @Override
            public WebHookListenerAO doInTransaction()
            {
                final WebHookListenerAO webHookAO = ao.get(WebHookListenerAO.class, listener.getId().get());
                if (webHookAO == null)
                {
                    throw new IllegalArgumentException(i18n.getText("webhooks.invalid.webhook.id"));
                }

                webHookAO.setName(listener.getName());
                webHookAO.setUrl(listener.getUrl());
                webHookAO.setDescription(listener.getDescription());
                webHookAO.setEvents(DbParamMarshaler.marshalEvents(listener.getEvents()));
                webHookAO.setEnabled(listener.isEnabled());
                webHookAO.setLastUpdatedUser(getUserKey());
                webHookAO.setLastUpdated(new Date());
                webHookAO.setExcludeBody(listener.isExcludeBody());
                webHookAO.setFilters(DbParamMarshaler.marshalFilters(listener.getFilters()));

                webHookAO.save();

                return webHookAO;
            }
        });
        return createWebHookListenerParameters(updatedWebHookListener);
    }

    @Override
    public Option<PersistentWebHookListener> getWebHook(final int id)
    {
        return ao.executeInTransaction(new TransactionCallback<Option<PersistentWebHookListener>>()
        {
            @Override
            public Option<PersistentWebHookListener> doInTransaction()
            {
                return Option.option(createWebHookListenerParameters(ao.get(WebHookListenerAO.class, id)));
            }
        });
    }

    @Override
    public void removeWebHook(final int id)
    {
        ao.executeInTransaction(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                final WebHookListenerAO webHookListenerAO = ao.get(WebHookListenerAO.class, id);
                if (webHookListenerAO == null)
                {
                    throw new IllegalArgumentException(i18n.getText("webhooks.invalid.webhook.id"));
                }
                ao.delete(webHookListenerAO);
                return webHookListenerAO;
            }
        });
    }

    @Override
    public Collection<PersistentWebHookListener> getAllWebHooks()
    {
        return newArrayList(transform(ao.executeInTransaction(new TransactionCallback<Collection<WebHookListenerAO>>()
                {
                    @Override
                    public Collection<WebHookListenerAO> doInTransaction()
                    {
                        return Arrays.asList(ao.find(WebHookListenerAO.class));
                    }
                }), new Function<WebHookListenerAO, PersistentWebHookListener>()
                {
                    @Override
                    public PersistentWebHookListener apply(final WebHookListenerAO webhookDao)
                    {
                        return createWebHookListenerParameters(webhookDao);
                    }
                }
        ));
    }

    public static class DbParamMarshaler
    {
        public static String marshalEvents(final Iterable<String> events)
        {
            return events != null ? new JSONArray(Lists.newArrayList(events)).toString() : StringUtils.EMPTY;
        }

        public static String marshalFilters(final Map<SectionKey, Filter> params)
        {
            Map<SectionKey, String> map = Maps.transformValues(params, new Function<Filter, String>()
            {
                @Override
                public String apply(Filter input)
                {
                    return input.getValue();
                }
            });

            return new JSONObject(map).toString();
        }

        public static Collection<String> unmarshalEvents(final String events)
        {
            try
            {
                final JSONArray jsonArray = events != null ? new JSONArray(events) : new JSONArray();
                final ImmutableList.Builder<String> builder = ImmutableList.builder();
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    builder.add(jsonArray.getString(i));
                }
                return builder.build();
            }
            catch (JSONException e)
            {
                throw new RuntimeException(e);
            }

        }

        public static Map<String, String> unmarshalFilters(final String parameters)
        {
            try
            {
                final JSONObject jsonObject = StringUtils.isNotBlank(parameters) ? new JSONObject(parameters) : new JSONObject();
                final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
                String[] names = JSONObject.getNames(jsonObject);
                if (names != null)
                {
                    for (String key : names)
                    {
                        builder.put(key, jsonObject.getString(key));
                    }
                }
                return builder.build();
            }
            catch (JSONException e)
            {
                throw new RuntimeException(e);
            }
        }
    }

    private static PersistentWebHookListener createWebHookListenerParameters(final WebHookListenerAO webHookListenerAO)
    {
        return PersistentWebHookListener
                .existing(webHookListenerAO.getID())
                .addWebHookIds(DbParamMarshaler.unmarshalEvents(webHookListenerAO.getEvents()))
                .addFilters(DbParamMarshaler.unmarshalFilters(webHookListenerAO.getFilters()))
                .setEnabled(webHookListenerAO.isEnabled())
                .setExcludeBody(webHookListenerAO.isExcludeBody())
                .setLastUpdated(webHookListenerAO.getLastUpdated())
                .setLastUpdatedByUser(webHookListenerAO.getLastUpdatedUser())
                .setListenerName(webHookListenerAO.getName())
                .setUrl(webHookListenerAO.getUrl())
                .setDescription(webHookListenerAO.getDescription())
                .build();
    }

    private String getUserKey()
    {
        final UserKey userKey = userManager.getRemoteUserKey();
        return userKey != null ? userKey.getStringValue() : StringUtils.EMPTY;
    }
}
