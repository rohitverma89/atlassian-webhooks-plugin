package com.atlassian.webhooks.plugin.web;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.templaterenderer.TemplateRenderer;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WebHookAdminServlet extends HttpServlet
{
    private static final String WEBHOOKS_ADMIN_TEMPLATE_PATH = "templates/webhooks-admin.vm";
    private static final String NO_ADMIN_PRIVILEGES_TEMPLATE_PATH = "templates/no_admin_privileges.vm";
    private static final String TEXT_HTML_CHARSET_UTF_8 = "text/html; charset=utf-8";

    private final TemplateRenderer templateRenderer;
    private final UserManager userManager;
    private final WebSudoManager webSudoManager;
    private final AdminPageContextBuilder contextBuilder;

    public WebHookAdminServlet(final TemplateRenderer templateRenderer, final UserManager userManager, final WebSudoManager webSudoManager, final AdminPageContextBuilder contextBuilder)
    {
        this.templateRenderer = templateRenderer;
        this.userManager = userManager;
        this.webSudoManager = webSudoManager;
        this.contextBuilder = contextBuilder;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        try
        {
            // Enable web sudo protection if needed and if the app we are running in supports it
            webSudoManager.willExecuteWebSudoRequest(req);
            if (userManager.isAdmin(userManager.getRemoteUserKey()) || userManager.isSystemAdmin(userManager.getRemoteUserKey()))
            {
                render(resp);
            }
            else
            {
                renderNoAdminPrivileges(resp);
            }
        }
        catch (WebSudoSessionException wse)
        {
            webSudoManager.enforceWebSudoProtection(req, resp);
        }
    }

    private void renderNoAdminPrivileges(HttpServletResponse response) throws IOException
    {
        response.setContentType(TEXT_HTML_CHARSET_UTF_8);
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        templateRenderer.render(NO_ADMIN_PRIVILEGES_TEMPLATE_PATH, response.getWriter());
    }

    private void render(HttpServletResponse response) throws IOException
    {
        response.setContentType(TEXT_HTML_CHARSET_UTF_8);
        templateRenderer.render(WEBHOOKS_ADMIN_TEMPLATE_PATH, contextBuilder.buildContext(), response.getWriter());
    }

}
