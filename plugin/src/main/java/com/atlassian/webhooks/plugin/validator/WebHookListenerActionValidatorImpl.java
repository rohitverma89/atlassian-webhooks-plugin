package com.atlassian.webhooks.plugin.validator;


import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.util.ErrorMessage;
import com.atlassian.webhooks.api.util.MessageCollection;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.spi.WebHookListenerActionValidator;
import com.google.common.base.Function;
import org.apache.commons.lang.StringUtils;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Evaluates the build in validation of webhooks create, update and removal.
 * Retrieves the product's implementation of {@link WebHookListenerActionValidator} from the bundleContext and runs
 * product specific checks of chosen webhooks actions.
 */
@Named("webHookListenerActionValidator")
public class WebHookListenerActionValidatorImpl implements WebHookListenerActionValidator
{
    private static final String NAME = "name";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final BundleContext bundleContext;
    private final EventsValidator eventsValidator;
    private final UrlValidator urlValidator;
    private final I18nResolver i18n;
    private final WebHookPluginRegistrationContainer container;

    @Inject
    public WebHookListenerActionValidatorImpl(BundleContext bundleContext, EventsValidator eventsValidator, final UrlValidator urlValidator, final I18nResolver i18n, final WebHookPluginRegistrationContainer container)
    {
        this.urlValidator = urlValidator;
        this.container = container;
        this.eventsValidator = checkNotNull(eventsValidator);
        this.bundleContext = checkNotNull(bundleContext);
        this.i18n = i18n;
    }

    @Override
    public MessageCollection validateWebHookRegistration(final PersistentWebHookListener listener)
    {
        return validateWebHookInput(listener, new Function<WebHookListenerActionValidator, MessageCollection>()
        {
            @Override
            public MessageCollection apply(final WebHookListenerActionValidator validator)
            {
                return validator.validateWebHookRegistration(listener);
            }
        });
    }

    @Override
    public MessageCollection validateWebHookRemoval(final PersistentWebHookListener registrationParameters)
    {
        return doValidation(new Function<WebHookListenerActionValidator, MessageCollection>()
        {
            @Override
            public MessageCollection apply(WebHookListenerActionValidator validator)
            {
                return validator.validateWebHookRemoval(registrationParameters);
            }
        });
    }

    @Override
    public MessageCollection validateWebHookUpdate(final PersistentWebHookListener listener)
    {
        return validateWebHookInput(listener, new Function<WebHookListenerActionValidator, MessageCollection>()
        {
            @Override
            public MessageCollection apply(final WebHookListenerActionValidator validator)
            {
                return validator.validateWebHookRegistration(listener);
            }
        });
    }

    public MessageCollection validateWebHookInput(final PersistentWebHookListener listener, Function<WebHookListenerActionValidator, MessageCollection> validationMethod)
    {
        final MessageCollection.Builder messageCollectionBuilder = MessageCollection.builder();
        messageCollectionBuilder.addAll(eventsValidator.validate(listener));
        messageCollectionBuilder.addAll(urlValidator.validate(listener));

        validateName(listener, messageCollectionBuilder);

        messageCollectionBuilder.addAll(doValidation(validationMethod));

        return messageCollectionBuilder.build();
    }

    private void validateName(final PersistentWebHookListener listener, final MessageCollection.Builder messageCollectionBuilder)
    {
        if (StringUtils.isEmpty(listener.getName()))
        {
            messageCollectionBuilder.addMessage(
                    new ErrorMessage(NAME, i18n.getText("webhooks.empty.field", i18n.getText("webhooks.name"))),
                    MessageCollection.Reason.VALIDATION_FAILED);
        }
    }

    private MessageCollection doValidation(Function<WebHookListenerActionValidator, MessageCollection> validationFunction)
    {
        final MessageCollection.Builder builder = MessageCollection.builder();

        for (WebHookListenerActionValidator validator : container.getValidators())
        {
            builder.addAll(validationFunction.apply(validator));
        }
        return builder.build();
    }
}
