package com.atlassian.webhooks.plugin.uri;

import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import org.osgi.framework.InvalidSyntaxException;

import java.net.URI;


public interface PluggableUriResolver
{
    /**
     * Gets a fully constructed URI for a relative path defined in the plugin with the given key.
     *
     * @param listenerDetails registration details of web hook listener
     * @param path the relative path
     * @return an absolute URI to the plugin path.
     */
    URI resolve(final WebHookListenerRegistrationDetails listenerDetails, final URI path) throws InvalidSyntaxException;
}
