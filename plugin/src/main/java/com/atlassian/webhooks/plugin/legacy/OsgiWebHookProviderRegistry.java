package com.atlassian.webhooks.plugin.legacy;

import com.atlassian.osgi.tracker.WaitableServiceTrackerCustomizer;
import com.atlassian.osgi.tracker.WaitableServiceTrackerFactory;
import com.atlassian.webhooks.api.register.RegisteredWebHookEvent;
import com.atlassian.webhooks.api.register.WebHookPluginRegistration;
import com.atlassian.webhooks.api.register.WebHookPluginRegistrationBuilder;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.plugin.module.ModuleKey;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.spi.EventMatcher;
import com.atlassian.webhooks.spi.EventSerializer;
import com.atlassian.webhooks.spi.provider.WebHookProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class OsgiWebHookProviderRegistry
{
    @Autowired
    public OsgiWebHookProviderRegistry(final WaitableServiceTrackerFactory factory,
            final WebHookPluginRegistrationContainer registrationContainer)
    {
        factory.create(WebHookProvider.class, new WebHookProviderWaitableServiceTrackerCustomizer(registrationContainer));
    }

    private static class WebHookProviderWaitableServiceTrackerCustomizer implements WaitableServiceTrackerCustomizer<WebHookProvider>
    {
        private final WebHookPluginRegistrationContainer registrationContainer;

        public WebHookProviderWaitableServiceTrackerCustomizer(final WebHookPluginRegistrationContainer registrationContainer) {this.registrationContainer = registrationContainer;}

        @Override
        public WebHookProvider adding(final WebHookProvider provider)
        {
            final WebHookRegistrarImpl registrar = new WebHookRegistrarImpl();

            provider.provide(registrar);

            final Set<WebHookRegistration> registrations = registrar.getRegistrations();

            WebHookPluginRegistrationBuilder pluginRegistration = WebHookPluginRegistration.builder();
            for (final WebHookRegistration webhook : registrations)
            {
                pluginRegistration.eventSerializer(webhook.getEventClass(), new EventSerializer<Object>()
                {
                    @Override
                    public String serialize(final Object event)
                    {
                        return webhook.getEventSerializer(event).getWebHookBody();
                    }
                });
                pluginRegistration.addWebHook(toNewWebhook(webhook));
            }

            registrationContainer.addRegistration(ModuleKey.of(provider), pluginRegistration.build());
            return provider;
        }

        @Override
        public void removed(final WebHookProvider webHookProvider)
        {
            registrationContainer.removeRegistration(ModuleKey.of(webHookProvider));
        }

        private RegisteredWebHookEvent<?> toNewWebhook(final WebHookRegistration webhook)
        {
            return RegisteredWebHookEvent.withId(webhook.getId()).firedWhen(webhook.getEventClass()).isMatchedBy(createLegacyMatcher(webhook));
        }

        private EventMatcher<Object> createLegacyMatcher(final WebHookRegistration webhook)
        {
            return new LegacyEventMatcher<Object>()
            {
                @Override
                public boolean matches(final Object event, final WebHookListener listener)
                {
                    return webhook.getEventMatcher().matches(event, listener);
                }

                @Override
                public boolean matches(final Object event, final Object listenerParameters)
                {
                    return webhook.getEventMatcher().matches(event, listenerParameters);
                }
            };
        }

    }
}
