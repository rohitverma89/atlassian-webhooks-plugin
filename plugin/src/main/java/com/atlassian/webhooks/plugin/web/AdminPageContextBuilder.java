package com.atlassian.webhooks.plugin.web;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.message.HelpPathResolver;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webhooks.api.register.WebHookEventSection;
import com.atlassian.webhooks.api.register.WebHookPluginRegistration;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainerImpl;
import com.atlassian.webhooks.plugin.web.util.HtmlSafeContent;
import com.atlassian.webhooks.plugin.web.util.RendererContextBuilder;
import com.atlassian.webhooks.spi.WebHooksHtmlPanel;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Component
final class AdminPageContextBuilder
{
    private final WebResourceManager webResourceManager;
    private final I18nResolver i18nResolver;
    private final WebHookPluginRegistrationContainerImpl webHookPluginRegistrationContainer;
    private final UriVariablesProviderPageContextResolver uriVariablesProviderPageContextResolver;
    private final HelpPathResolver helpPathResolver;

    @Autowired
    public AdminPageContextBuilder(final WebResourceManager webResourceManager, final I18nResolver i18nResolver,
        final WebHookPluginRegistrationContainerImpl webHookPluginRegistrationContainer,
        final UriVariablesProviderPageContextResolver uriVariablesProviderPageContextResolver,
        final HelpPathResolver helpPathResolver)
    {
        this.webResourceManager = webResourceManager;
        this.i18nResolver = i18nResolver;
        this.webHookPluginRegistrationContainer = webHookPluginRegistrationContainer;
        this.uriVariablesProviderPageContextResolver = uriVariablesProviderPageContextResolver;
        this.helpPathResolver = helpPathResolver;
    }

    public Map<String, Object> buildContext() throws IOException
    {
        webResourceManager.requireResourcesForContext("atl.plugins.webhooks.admin");
        final RendererContextBuilder builder = new RendererContextBuilder()
                .put("i18n", i18nResolver)
                .put("helpPath", helpPathResolver)
                .put("webResources", new HtmlSafeContent()
                {
                    public CharSequence get()
                    {
                        StringWriter writer = new StringWriter();
                        webResourceManager.includeResources(writer, UrlMode.AUTO);
                        return writer.toString();
                    }
                })
                .put("customPanels", getCustomPanels())
                .put("webResourceManager", webResourceManager)
                .put("urlVariablesMap", uriVariablesMapForTemplate())
                .put("urlVariables", uriVariablesProviderPageContextResolver.allUriVariables())
                .put("sections", getDisplayableSections());
        return builder.build();
    }

    private List<WebHooksHtmlPanel> getCustomPanels()
    {
        return ImmutableList.copyOf(Iterables.concat(Iterables.transform(webHookPluginRegistrationContainer.getWebHookRegistrations(), new Function<WebHookPluginRegistration, List<WebHooksHtmlPanel>>()
        {
            @Override
            public List<WebHooksHtmlPanel> apply(final WebHookPluginRegistration input)
            {
                return input.getPanels();
            }
        })));
    }

    private Map<String, String> uriVariablesMapForTemplate()
    {
        final Joiner joiner = Joiner.on(",");
        return Maps.transformValues(uriVariablesProviderPageContextResolver.uriVariablesForWebhooks(), new Function<Collection<String>, String>()
        {
            @Override
            public String apply(final Collection<String> input)
            {
                return joiner.join(input);
            }
        });
    }

    private List<WebHookEventSection> getDisplayableSections()
    {
        return ImmutableList.copyOf(Iterables.filter(webHookPluginRegistrationContainer.getWebHookSections(), new Predicate<WebHookEventSection>()
        {
            @Override
            public boolean apply(final WebHookEventSection webHookEventSection)
            {
                return StringUtils.isNotEmpty(webHookEventSection.getNameI18nKey());
            }
        }));
    }
}
