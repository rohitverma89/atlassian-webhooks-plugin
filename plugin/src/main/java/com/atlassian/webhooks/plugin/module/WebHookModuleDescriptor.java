package com.atlassian.webhooks.plugin.module;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.webhooks.api.register.listener.ModuleDescriptorWebHookListenerRegistry;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import javax.annotation.Nonnull;
import java.util.List;

import static com.atlassian.webhooks.api.register.listener.WebHookListenerParameters.Names.EXCLUDE_BODY;
import static com.atlassian.webhooks.api.register.listener.WebHookListenerParameters.Names.FILTER;
import static com.atlassian.webhooks.api.register.listener.WebHookListenerParameters.Names.PROPERTY_KEY;
import static com.google.common.base.Preconditions.checkNotNull;

public class WebHookModuleDescriptor extends AbstractModuleDescriptor<Void>
{
    private final ModuleDescriptorWebHookListenerRegistry webHookListenerRegistry;

    private String webhookEventId;
    private String url;
    private Boolean excludeBody = null;
    private String filter = null;
    private String webhookPluginKey;
    private List<String> propertyKeys = Lists.newArrayList();

    public WebHookModuleDescriptor(ModuleFactory moduleFactory, ModuleDescriptorWebHookListenerRegistry webHookListenerRegistry)
    {
        super(moduleFactory);
        this.webHookListenerRegistry = checkNotNull(webHookListenerRegistry);
    }

    @Override
    public Void getModule()
    {
        return null;
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException
    {
        super.init(plugin, element);
        webhookEventId = getOptionalAttribute(element, "event", getKey());
        url = getRequiredUriAttribute(element, "url");

        loadParameters(element);
    }

    private void loadParameters(Element element)
    {
        String excludeBodyStr = getOptionalAttribute(element, "excludeBody", this.excludeBody);
        excludeBody = excludeBodyStr != null ? Boolean.valueOf(excludeBodyStr) : null;
        filter = getOptionalAttribute(element, "filter", this.filter);

        List<Element> elements = element.elements("param");
        if (elements != null)
        {
            for (Element param : elements)
            {
                String paramKey = getRequiredAttribute(param, "name");
                if (EXCLUDE_BODY.equals(paramKey) || "excludeIssueDetails".equals(paramKey))
                {
                    excludeBody = validateUnambiguity(excludeBody, Boolean.valueOf(param.getText()), EXCLUDE_BODY);
                }
                if (FILTER.equals(paramKey) || "jql".equals(paramKey))
                {
                    filter = validateUnambiguity(filter, param.getText().trim(), FILTER);
                }
                if (PROPERTY_KEY.equals(paramKey)) {
                    propertyKeys.add(param.getText());
                }
            }
        }

        excludeBody = Objects.firstNonNull(excludeBody, Boolean.FALSE);
        filter = Strings.nullToEmpty(filter);
    }

    private <T> T validateUnambiguity(T paramValue, T newParamValue, String paramName)
    {
        if (paramValue != null && !newParamValue.equals(paramValue))
        {
            throw new IllegalStateException(paramName + " defined twice with different values: " + paramValue + ", " + newParamValue);
        }
        return newParamValue;
    }

    @Override
    public void enabled()
    {
        super.enabled();
        webHookListenerRegistry.register(webhookEventId, definedListener());
    }

    private WebHookListener definedListener()
    {
        return WebHookListener.fromModuleDescriptor(getWebhookPluginKey())
                .to(url)
                .excludeBody(excludeBody)
                .withFilter(filter)
                .withPropertyKeys(propertyKeys)
                .build();
    }

    @Override
    public void disabled()
    {
        webHookListenerRegistry.unregister(webhookEventId, definedListener());
        super.disabled();
    }

    private static String getOptionalAttribute(Element e, String name, Object defaultValue)
    {
        String value = e.attributeValue(name);
        return value != null ? value :
                defaultValue != null ? defaultValue.toString() : null;
    }

    private static String getRequiredUriAttribute(Element e, String name)
    {
        return getRequiredAttribute(e, name);
    }

    private static String getRequiredAttribute(Element e, String name)
    {
        String value = e.attributeValue(name);
        if (value == null)
        {
            throw new PluginParseException("Attribute '" + name + "' is required on '" + e.getName() + "'");
        }
        return value;
    }

    /**
     * allows for overriding the plugin key used for the actual webhook registration.
     * e.g. the pluginKey registered may be different than the plugin the moduel descriptor is registered to.
     * This is the case for all connect addons where the moduel descriptor is registered to the connect plugin
     * but the webhook needs to be registered with the addon's key
     *
     * @param webhookPluginKey
     */
    public void setWebhookPluginKey(String webhookPluginKey)
    {
        this.webhookPluginKey = webhookPluginKey;
    }

    public String getWebhookPluginKey()
    {
        return (StringUtils.isNotBlank(webhookPluginKey)) ? webhookPluginKey : getPluginKey();
    }
}
