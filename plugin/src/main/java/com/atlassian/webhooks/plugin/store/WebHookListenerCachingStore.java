package com.atlassian.webhooks.plugin.store;

import com.atlassian.event.api.EventListener;
import com.atlassian.fugue.Option;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.vcache.ExternalCacheSettingsBuilder;
import com.atlassian.vcache.StableReadExternalCache;
import com.atlassian.vcache.VCacheFactory;
import com.atlassian.vcache.marshallers.MarshallerFactory;
import com.atlassian.webhooks.api.events.WebHookClearListenerCacheEvent;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;
import com.atlassian.webhooks.spi.WebHookListenerStore;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static com.atlassian.webhooks.plugin.Version.MAJOR_VERSION;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Stores and caches WebHookListeners.
 */
@ExportAsService({LifecycleAware.class})
@Named("webHookListenerCachingStore")
public class WebHookListenerCachingStore implements LifecycleAware
{
    private static final String CACHE_NAME = WebHookListenerCachingStore.class.getName() + ".listeners";
    private static final String CACHE_KEY = MAJOR_VERSION;
    private static final Logger logger = LoggerFactory.getLogger(WebHookListenerCachingStore.class);

    private final WebHookListenerStore webHookListenerStore;
    private final VCacheFactory vcacheFactory;

    /**
     * A cached reference holding the state of webhook listeners. We use a CachedReference to a fully populated map
     * rather than a per entry cache in order to honour the {@link #getAllWebHookListeners()} method. This may
     * have performance implications if the list of webhooks grows large.
     */
    private volatile Optional<StableReadExternalCache<ImmutableMap<Integer, PersistentWebHookListener>>> maybeCache;

    @Inject
    public WebHookListenerCachingStore(@Named("webHookListenerStore") final WebHookListenerStore webHookListenerStore, final VCacheFactory vcacheFactory)
    {
        this.webHookListenerStore = checkNotNull(webHookListenerStore);
        this.vcacheFactory = checkNotNull(vcacheFactory);
        this.maybeCache = Optional.empty();
    }

    /**
     * Get a single WebHook Listener by id.
     *
     * @param id of the WebHook Listener.
     * @return the WebHook listener.
     */
    public Option<PersistentWebHookListener> getWebHookListener(final Integer id)
    {
        final Optional<PersistentWebHookListener> result = getListenersFromCache()
                .flatMap(listeners -> Optional.ofNullable(listeners.get(id)));
        return result.isPresent() ? Option.some(result.get()) : Option.none();
    }

    /**
     * Get a list of all listeners in the system
     *
     * @return collection of WebHook listeners.
     */
    public Iterable<PersistentWebHookListener> getAllWebHookListeners()
    {
        return getListenersFromCache()
                .map(Map::values)
                .orElse(Collections.emptyList());
    }

    /**
     * Add and caches a new WebHook listener and returns the newly created WebHook listener.
     *
     * @param listener parameters of the listener.
     * @param registrationMethod REST, UI or SERVICE.
     */
    public PersistentWebHookListener registerWebHookListener(PersistentWebHookListener listener, final RegistrationMethod registrationMethod)
    {
        final PersistentWebHookListener createdListener = webHookListenerStore.addWebHook(listener, registrationMethod);
        reset();
        return createdListener;
    }

    /**
     * Updates existing WebHook listener in db and cache and returns the newly created WebHook.
     *
     * @param listener parameters of the listener.
     * @throws IllegalArgumentException when listener with the specified id doesn't exist.
     */
    public PersistentWebHookListener updateWebHookListener(PersistentWebHookListener listener)
    {
        final PersistentWebHookListener listenerParameters = webHookListenerStore.updateWebHook(listener);
        reset();
        return listenerParameters;
    }

    /**
     * Removes single WebHook Listener by id from db and cache.
     *
     * @param id of the WebHook Listener.
     * @throws IllegalArgumentException the specified id does not exist
     */
    public void removeWebHookListener(final int id)
    {
        webHookListenerStore.removeWebHook(id);
        reset();
    }

    @Override
    public void onStart()
    {
        maybeCache = Optional.of(vcacheFactory.getStableReadExternalCache(
                CACHE_NAME,
                MarshallerFactory.serializableMarshaller(
                        (Class<ImmutableMap<Integer, PersistentWebHookListener>>) (Object) ImmutableMap.class,
                        getClass().getClassLoader() // We want the marshaller to see what we import from atlassian-webhooks-api.
                ),
                new ExternalCacheSettingsBuilder().entryCountHint(1).build()
        ));
    }

    @Override
    public void onStop() {
        maybeCache = Optional.empty();
    }

    @EventListener
    @SuppressWarnings("unused")
    public void onClearCacheEvent(final WebHookClearListenerCacheEvent clearCacheEvent)
    {
        reset();
    }

    private <T> Optional<Map<Integer, PersistentWebHookListener>> getListenersFromCache()
    {
        // Log message format is based on `com.atlassian.confluence.impl.vcache.SynchronousExternalCache`.
        final String LOG_PREFIX = "Failed to read entry from cache '" + CACHE_NAME + "': ";
        return maybeCache.map(cache -> cache.get(CACHE_KEY, this::load).exceptionally(th -> {
            logger.warn(LOG_PREFIX + th.getMessage());
            return load();
        }).toCompletableFuture().join());
    }

    private ImmutableMap<Integer, PersistentWebHookListener> load()
    {
        final Collection<PersistentWebHookListener> listeners = webHookListenerStore.getAllWebHooks();
        return Maps.uniqueIndex(listeners, listener -> listener.getId().get());
    }

    private void reset()
    {
        // Log message format is based on `com.atlassian.confluence.impl.vcache.SynchronousExternalCache`.
        final String LOG_PREFIX = "Failed to remove all entries from cache '" + CACHE_NAME + "': ";
        maybeCache.map(cache -> cache.removeAll().whenComplete((val, th) -> { if (th != null) {
            logger.error(LOG_PREFIX + th.getMessage());
        }}).toCompletableFuture().join());
    }
}
