package com.atlassian.webhooks.plugin.module;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.webhooks.api.util.TypeRichString;
import com.atlassian.webhooks.spi.provider.WebHookProvider;
import com.google.common.annotations.VisibleForTesting;

import static com.google.common.base.Preconditions.checkNotNull;

public final class ModuleKey extends TypeRichString
{
    @VisibleForTesting
    ModuleKey(String value)
    {
        super(value);
    }

    public static ModuleKey of(ModuleDescriptor<?> module)
    {
        return new ModuleKey(module.getCompleteKey());
    }

    @Deprecated
    public static ModuleKey of(WebHookProvider provider)
    {
        return new ModuleKey(provider.getClass().getName() + "-" + provider.hashCode());
    }
}
