package com.atlassian.webhooks.plugin.legacy;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.webhooks.spi.provider.ModuleDescriptorWebHookListenerRegistry;
import com.atlassian.webhooks.spi.provider.PluginModuleListenerParameters;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import java.net.URI;
import java.util.Set;
import javax.inject.Named;

@ExportAsService (ModuleDescriptorWebHookListenerRegistry.class)
@Named ("legacyModuleDescriptorWebHookListenerRegistry")
public class LegacyModuleDescriptorWebHookListenerRegistryImpl implements ModuleDescriptorWebHookListenerRegistry
{
    private final SetMultimap<String, LegacyListener> legacyListeners = HashMultimap.create();

    @Override
    public synchronized void register(final String webHookId, final String pluginKey, final URI uri, final PluginModuleListenerParameters consumerParams)
    {
        legacyListeners.put(webHookId, LegacyListener.of(uri, pluginKey, consumerParams));
    }

    @Override
    public synchronized void unregister(final String webHookId, final String pluginKey, final URI uri, final PluginModuleListenerParameters consumerParams)
    {
        legacyListeners.remove(webHookId, LegacyListener.of(uri, pluginKey, consumerParams));
    }

    public synchronized Set<LegacyListener> getListeners(final String webHookEventId)
    {
        return legacyListeners.get(webHookEventId);
    }
}
