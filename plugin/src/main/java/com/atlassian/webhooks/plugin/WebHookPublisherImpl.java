package com.atlassian.webhooks.plugin;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.webhooks.api.events.WebHookPublishedEvent;
import com.atlassian.webhooks.api.events.WebHookRejectedEvent;
import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.util.Vote;
import com.atlassian.webhooks.plugin.legacy.LegacyEventMatcher;
import com.atlassian.webhooks.plugin.legacy.LegacyListener;
import com.atlassian.webhooks.plugin.legacy.LegacyModuleDescriptorWebHookListenerRegistryImpl;
import com.atlassian.webhooks.spi.WebHookListenerAccessVoter;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.*;

import static com.google.common.base.Preconditions.checkNotNull;

public final class WebHookPublisherImpl implements WebHookPublisher
{
    private static final int PUBLISH_QUEUE_SIZE = 100;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final WebHookListenerAccessVoter accessVoter;
    private final WebHookListenerProvider listenerProvider;
    private final PublishTaskFactory publishTaskFactory;
    private final EventPublisher eventPublisher;
    private final Executor executor;
    private final LegacyModuleDescriptorWebHookListenerRegistryImpl legacyListeners;
    private final boolean ownExecutor;

    public WebHookPublisherImpl(WebHookListenerAccessVoter accessVoter, WebHookListenerProvider listenerProvider,
            PublishTaskFactory publishTaskFactory, EventPublisher eventPublisher, LegacyModuleDescriptorWebHookListenerRegistryImpl legacyListeners)
    {
        this(accessVoter, listenerProvider, publishTaskFactory, eventPublisher, legacyListeners,
                new ThreadPoolExecutor(3, 3, 0L, TimeUnit.MILLISECONDS,
                    new LinkedBlockingQueue<Runnable>(PUBLISH_QUEUE_SIZE),
                    new ThreadFactoryBuilder().setNameFormat("Web-Hook-Publisher-%d").setDaemon(true).build()),
                true);
    }

    public WebHookPublisherImpl(WebHookListenerAccessVoter accessVoter, WebHookListenerProvider listenerProvider,
            PublishTaskFactory publishTaskFactory, EventPublisher eventPublisher, LegacyModuleDescriptorWebHookListenerRegistryImpl legacyListeners, Executor executor)
    {
        this(accessVoter, listenerProvider, publishTaskFactory, eventPublisher, legacyListeners, executor, false);
    }

    WebHookPublisherImpl(WebHookListenerAccessVoter accessVoter, WebHookListenerProvider listenerProvider,
                                 PublishTaskFactory publishTaskFactory, EventPublisher eventPublisher, LegacyModuleDescriptorWebHookListenerRegistryImpl legacyListeners, Executor executor, boolean ownExecutor)
    {
        this.accessVoter = checkNotNull(accessVoter, "accessVoter");
        this.listenerProvider = checkNotNull(listenerProvider, "listenerProvider");
        this.publishTaskFactory = checkNotNull(publishTaskFactory, "publishTaskFactory");
        this.eventPublisher = checkNotNull(eventPublisher, "eventPublisher");
        this.legacyListeners = checkNotNull(legacyListeners, "legacyListeners");
        this.executor = executor;
        this.ownExecutor = ownExecutor;
    }

    public void destroy()
    {
        if (ownExecutor)
        {
            List<Runnable> runnables = ((ThreadPoolExecutor) executor).shutdownNow();
            if (!runnables.isEmpty())
            {
                logger.warn("Not publishing {} web hook event(s) due to shutdown");
            }
        }
    }

    @Override
    public void publish(WebHookEvent webHookEvent)
    {
        for (WebHookListener listener : listenerProvider.getListeners(webHookEvent))
        {
            if (match(webHookEvent, listener) && Vote.ALLOW.equals(accessVoter.canPublish(webHookEvent, listener)))
            {
                publish(webHookEvent, listener);
            }
        }

        for (LegacyListener legacyListener : legacyListeners.getListeners(webHookEvent.getId()))
        {
            if (legacyMatch(webHookEvent, legacyListener) && Vote.ALLOW.equals(accessVoter.canPublish(webHookEvent, legacyListener.getListener())))
            {
                publish(webHookEvent, legacyListener.getListener());
            }
        }
    }

    private boolean legacyMatch(final WebHookEvent webHookEvent, final LegacyListener legacyListener)
    {
        if (webHookEvent.getEventMatcher() instanceof LegacyEventMatcher)
        {
            LegacyEventMatcher matcher = (LegacyEventMatcher) webHookEvent.getEventMatcher();
            return matcher.matches(webHookEvent.getEvent(), legacyListener.getLegacyParameters());
        }
        else
        {
            return match(webHookEvent, legacyListener.getListener()); // match normally if we are dealing with new webhook matcher and legacy listener
        }
    }

    private boolean match(WebHookEvent webHookEvent, WebHookListener listener)
    {
        return webHookEvent.getEventMatcher().matches(webHookEvent.getEvent(), listener);
    }

    private void publish(WebHookEvent webHookEvent, WebHookListener listener)
    {
        final PublishTask publishTask = publishTaskFactory.getPublishTask(webHookEvent, listener);
        try
        {
            executor.execute(publishTask);
            eventPublisher.publish(new WebHookPublishedEvent(webHookEvent.getId(), listener.getRegistrationDetails()));
        }
        catch (RejectedExecutionException ex)
        {
            logger.warn("Executor rejected the web hook '{}' saying '{}'", publishTask, ex.getMessage());
            logger.debug("Here is the full exception", ex);
            eventPublisher.publish(new WebHookRejectedEvent(webHookEvent.getId(), listener.getRegistrationDetails(), ex.getMessage()));
        }
    }

}
