package com.atlassian.webhooks.plugin.module;

import com.atlassian.webhooks.plugin.util.ClassInfo;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.util.Map;

/**
 * This is a collection of T-s which can act on specific classes.
 * <p/>
 * <p>You can think of it as a {@code Map<Class, T>}
 * with the ability to return T-s for specific classes.</p>
 * <p/>
 * <p>It takes class hierarchies into consideration,
 * so if you have a class A extends B, and a processor
 * for B, then after requesting a processor for A, the
 * most specific matching processor will be returned, in
 * this case: the one for B.</p>
 *
 * @param <T> type of processors
 */
public class ClassSpecificProcessors<T>
{
    private final Map<Class, T> map;
    private final String name;

    @VisibleForTesting
    ClassSpecificProcessors(Map<Class, T> map)
    {
        this(map, "Processor");
    }

    @VisibleForTesting
    private ClassSpecificProcessors(Map<Class, T> map, String name)
    {
        this.map = ImmutableMap.copyOf(map);
        this.name = name;
    }

    public static <T> Builder<T> builder(String processorName)
    {
        return new Builder<T>(processorName);
    }

    /**
     * Returns a processor suitable for the specified type.
     * @param type class (or subclass of a class or class implementing interface) for which the processor was registered
     *
     * @return the best matching processor or absent if none is found
     */
    public Optional<T> forType(Class<?> type)
    {
        for(ClassInfo.HierarchyLevel hierarchyHierarchyLevel : ClassInfo.of(type).typeHierarchy())
        {
            Sets.SetView<Class<?>> matching = Sets.intersection(hierarchyHierarchyLevel.getTypes(), map.keySet());
            if (matching.size() > 1)
            {
                throw new IllegalStateException(String.format(
                        "Don't know what to choose for %s, %ss are registered for the following types: %s", type, name, matching));
            }
            if (matching.size() == 1)
            {
                return Optional.of(map.get(matching.iterator().next()));
            }
        }
        return Optional.absent();
    }

    /**
     * Returns all processors that can act on objects of the specified type.
     *
     * @param type class of objects that the returned processors can act on
     * @return iterable of processors
     */
    public Iterable<T> allForType(final Class<?> type)
    {
        return Iterables.transform(Iterables.filter(map.entrySet(), new Predicate<Map.Entry<Class, T>>()
        {
            @Override
            public boolean apply(Map.Entry<Class, T> input)
            {
                return input.getKey().isAssignableFrom(type);
            }
        }), new Function<Map.Entry<Class, T>, T>()
        {
            @Override
            public T apply(Map.Entry<Class, T> input)
            {
                return input.getValue();
            }
        });
    }

    static class Builder<T>
    {
        private final Map<Class, T> builder = Maps.newHashMap();
        private final String processorName;

        private Builder(String processorName)
        {
            this.processorName = processorName;
        }

        public Builder<T> add(Class type, T processor)
        {
            if (builder.containsKey(type))
            {
                Class registeredProcessor = builder.get(type).getClass();
                Class newProcessor = processor.getClass();
                if (!registeredProcessor.equals(newProcessor))
                {
                    throw new IllegalStateException(
                            String.format("Tried to register two %ss (%s and %s) for event class %s", processorName, registeredProcessor.getName(), newProcessor.getName(), type)
                                    + ", but at the moment only one \" + processorName + \" per event class is supported\")");
                }
            }
            builder.put(type, processor);
            return this;
        }

        public ClassSpecificProcessors<T> build()
        {
            return new ClassSpecificProcessors<T>(builder, processorName);
        }
    }
}
