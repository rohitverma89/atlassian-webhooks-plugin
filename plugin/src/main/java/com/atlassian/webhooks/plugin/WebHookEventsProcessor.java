package com.atlassian.webhooks.plugin;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.api.publish.WebHookEvent;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.google.common.base.Preconditions.*;

public final class WebHookEventsProcessor implements InitializingBean, DisposableBean
{
    private final EventPublisher eventPublisher;
    private final WebHookPublisher webHookPublisher;
    private final WebHookPluginRegistrationContainer container;
    private final PluginEventManager pluginEventManager;

    public WebHookEventsProcessor(EventPublisher eventPublisher, PluginEventManager pluginEventManager,
                                  WebHookPublisher webHookPublisher, final WebHookPluginRegistrationContainer container)
    {
        this.eventPublisher = checkNotNull(eventPublisher);
        this.pluginEventManager = checkNotNull(pluginEventManager);
        this.webHookPublisher = checkNotNull(webHookPublisher);
        this.container = checkNotNull(container);
    }

    @EventListener
    public void onEvent(final Object event)
    {
        doOnEvent(event);
    }

    private void doOnEvent(Object event)
    {
        for (WebHookEvent webHookEvent : getWebHooksForEvent(event))
        {
            webHookPublisher.publish(webHookEvent);
        }
    }

    private Iterable<WebHookEvent> getWebHooksForEvent(final Object event)
    {
        return container.getWebHookRegistry().getWebHooks(event);
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        eventPublisher.register(this);
        pluginEventManager.register(this);
    }

    @Override
    public void destroy() throws Exception
    {
        pluginEventManager.unregister(this);
        eventPublisher.unregister(this);
    }
}
