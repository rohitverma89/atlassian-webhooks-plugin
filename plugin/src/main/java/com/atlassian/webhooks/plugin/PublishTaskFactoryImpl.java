package com.atlassian.webhooks.plugin;

import com.atlassian.fugue.Effect;
import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.httpclient.api.Request;
import com.atlassian.httpclient.api.Response;
import com.atlassian.httpclient.api.ResponseTransformation;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.uri.Uri;
import com.atlassian.uri.UriBuilder;
import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerOrigin;
import com.atlassian.webhooks.plugin.module.WebHookEventSerializer;
import com.atlassian.webhooks.plugin.uri.UriVariablesResolver;
import com.atlassian.webhooks.spi.QueryParamsProvider;
import com.atlassian.webhooks.spi.RequestSigner2;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.collect.Multimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.Map;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;
import static java.util.Optional.ofNullable;

public final class PublishTaskFactoryImpl implements PublishTaskFactory
{
    private static final int ONE_MINUTE = 60 * 1000;

    private final HttpClient httpClient;
    private final RequestSigner2 requestSigner;
    private final UserManager userManager;
    private final UriVariablesResolver uriVariablesResolver;
    private final WebHookEventSerializer webHookEventSerializer;
    private final QueryParamsProvider queryParamsProvider;

    public PublishTaskFactoryImpl(HttpClient httpClient,
            RequestSigner2 requestSigner,
            UserManager userManager,
            UriVariablesResolver uriVariablesResolver,
            WebHookEventSerializer webHookEventSerializer,
            QueryParamsProvider queryParamsProvider)
    {
        this.webHookEventSerializer = webHookEventSerializer;
        this.queryParamsProvider = queryParamsProvider;
        this.httpClient = checkNotNull(httpClient);
        this.requestSigner = checkNotNull(requestSigner);
        this.userManager = checkNotNull(userManager);
        this.uriVariablesResolver = checkNotNull(uriVariablesResolver);
    }

    @Override
    public PublishTask getPublishTask(WebHookEvent webHookEvent, WebHookListener listener)
    {
        Optional<UserProfile> user = ofNullable(userManager.getRemoteUser());
        return new PublishTaskImpl(
                httpClient,
                requestSigner,
                listener,
                addQueryParams(getListenerUri(webHookEvent, listener), user, listener),
                user,
                getEventBody(webHookEvent, listener)
        );
    }

    @VisibleForTesting
    URI addQueryParams(URI uri, Optional<UserProfile> user, WebHookListener listener)
    {
        Uri parsedUri = Uri.fromJavaUri(uri);
        UriBuilder uriBuilder = new UriBuilder()
                .setScheme(parsedUri.getScheme())
                .setAuthority(parsedUri.getAuthority())
                .setPath(parsedUri.getPath())
                .setQuery(parsedUri.getQuery());

        Multimap<String, String> queryParams = queryParamsProvider.provideQueryParams(user, listener.getRegistrationDetails());
        for (Map.Entry<String, String> param : queryParams.entries())
        {
            uriBuilder.addQueryParameter(param.getKey(), param.getValue());
        }
        return uriBuilder.toUri().toJavaUri();
    }

    private String getEventBody(WebHookEvent webHookEvent, WebHookListener listener)
    {
        return webHookEventSerializer.serialize(webHookEvent, listener);
    }

    private URI getListenerUri(final WebHookEvent webHookEvent, final WebHookListener listener)
    {
        return uriVariablesResolver.resolve(listener, webHookEvent);
    }

    static final class PublishTaskImpl implements PublishTask
    {
        private final Logger logger = LoggerFactory.getLogger(this.getClass());

        // The token bucket has a max size of 5 and gains one token per minute. So you can log once per minute on average.
        // Or up to 5 times in one minute if you haven't logged in a few minutes.
        private final ThrottlingLogger throttlingLogger = new ThrottlingLogger(logger, new TokenBucket(1, ONE_MINUTE, 5));

        private final HttpClient httpClient;
        private final RequestSigner2 requestSigner;
        private final WebHookListener listener;
        private final URI uri;
        private final String body;
        private final Optional<UserProfile> user;

        PublishTaskImpl(HttpClient httpClient,
                RequestSigner2 requestSigner,
                WebHookListener listener,
                URI uri,
                Optional<UserProfile> user,
                String body)
        {
            this.httpClient = checkNotNull(httpClient);
            this.requestSigner = checkNotNull(requestSigner);
            this.listener = checkNotNull(listener);
            this.uri = checkNotNull(uri);
            this.user = user;
            this.body = checkNotNull(body);
        }

        @Override
        public void run()
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Posting to web hook at '{}', body is:\n{}\n", uri, body);
            }

            final Request.Builder request = isNullOrEmpty(body) ? httpClient.newRequest(uri) : httpClient.newRequest(uri, "application/json", body);
            setRequestAttributes(request);

            // Signing is made in just before sending the request, because token could expire.
            requestSigner.sign(uri, user, listener.getRegistrationDetails(), request);
            request.post().transform(logResult());
        }

        private ResponseTransformation<Object> logResult()
        {
            return httpClient.transformation()
                    .successful(new Function<Response, Object>()
                    {
                        @Override
                        public Object apply(final Response response)
                        {
                            logger.debug("WebHook successfully sent");
                            return null;
                        }
                    })
                    .clientError(logError("Client error"))
                    .serverError(logError("Server error"))
                    .build();
        }

        private Function<Response, Void> logError(final String errorName)
        {
            return new Function<Response, Void>()
            {
                @Override
                public Void apply(final Response response)
                {
                    return throttlingLogger.use(new Effect<Logger>()
                    {
                        @Override
                        public void apply(final Logger logger)
                        {
                            logger.warn(errorName + " - {} when posting to web hook at '{}'\n", response.getStatusCode(), uri);
                            logger.debug("Body is {}\n", body);
                        }
                    });
                }
            };
        }

        private void setRequestAttributes(Request.Builder request)
        {
            // attributes capture optional properties sent to analytics
            request.setAttribute("purpose", "web-hook-notification");
            if (listenerPluginKey() != null)
            {
                request.setAttribute("pluginKey", listenerPluginKey());
            }
        }

        private String listenerPluginKey()
        {
            return listener.getRegistrationDetails().getOrigin() == WebHookListenerOrigin.MODULE_DESCRIPTOR ?
                    listener.getRegistrationDetails().getModuleDescriptorDetails().get().getPluginKey() : null;
        }

        @Override
        public String toString()
        {
            Objects.ToStringHelper toStringHelper = Objects.toStringHelper(PublishTask.class)
                    .add("listenerKey", listener)
                    .add("uri", uri)
                    .add("body", body);
            if (user.isPresent())
            {
                toStringHelper.add("userName", user.get().getUsername())
                        .add("userKey", user.get().getUserKey().getStringValue());
            }
            return toStringHelper.toString();
        }
    }
}
