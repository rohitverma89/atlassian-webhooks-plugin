package com.atlassian.webhooks.plugin.uri;

import com.atlassian.fugue.Option;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.webhooks.api.register.listener.WebHookListenerOrigin;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import com.atlassian.webhooks.spi.UriResolver;
import com.atlassian.webhooks.spi.plugin.PluginUriResolver;
import com.google.common.base.Optional;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URI;
import javax.inject.Named;

import static com.atlassian.fugue.Option.none;

@Named ("pluggableUriResolver")
public class PluggableUriResolverImpl implements PluggableUriResolver
{
    private final BundleContext bundleContext;
    private final ApplicationProperties applicationProperties;
    private static final Logger log = LoggerFactory.getLogger(PluggableUriResolverImpl.class);

    @Autowired
    public PluggableUriResolverImpl(final BundleContext bundleContext, ApplicationProperties applicationProperties)
    {
        this.bundleContext = bundleContext;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public URI resolve(final WebHookListenerRegistrationDetails listenerDetails, final URI path)
            throws InvalidSyntaxException
    {
        if (listenerDetails.getOrigin() == WebHookListenerOrigin.PERSISTENT_STORE)
        {
            return path;
        }
        else
        {
            return resolveByPlugins(listenerDetails, path).getOrElse(addBaseUrlIfRelative(path));
        }
    }

    private Option<URI> resolveByPlugins(final WebHookListenerRegistrationDetails listenerDetails, final URI path)
            throws InvalidSyntaxException
    {
        // new resolution
        final ServiceReference[] serviceReferences = bundleContext.getAllServiceReferences(UriResolver.class.getName(), null);
        if (serviceReferences != null)
        {
            for (ServiceReference serviceReference : serviceReferences)
            {
                try
                {
                    final UriResolver uriResolver = (UriResolver) bundleContext.getService(serviceReference);

                    final Option<URI> uri = safelyGetUriFromPluggableResolver(listenerDetails, path, uriResolver);
                    if (uri.isDefined())
                    {
                        return uri;
                    }
                }
                finally
                {
                    bundleContext.ungetService(serviceReference);
                }
            }
        }

        final ServiceReference[] legacyServiceReferences = bundleContext.getAllServiceReferences(PluginUriResolver.class.getName(), null);
        // legacy resolution
        if (legacyServiceReferences != null)
        {
            for (ServiceReference serviceReference : legacyServiceReferences)
            {
                try
                {
                    final PluginUriResolver uriResolver = (PluginUriResolver) bundleContext.getService(serviceReference);
                    if (listenerDetails.getModuleDescriptorDetails().isDefined())
                    {
                        final Optional<URI> uri = safelyGetUriFromPlugabblePluginUriResolver(listenerDetails, path, uriResolver);
                        if (uri.isPresent())
                        {
                            return Option.some(uri.get());
                        }
                    }
                }
                finally
                {
                    bundleContext.ungetService(serviceReference);
                }
            }
        }

        return none();
    }

    private Optional<URI> safelyGetUriFromPlugabblePluginUriResolver(WebHookListenerRegistrationDetails listenerDetails, URI path, PluginUriResolver uriResolver)
    {
        try
        {
            return uriResolver.getUri(listenerDetails.getModuleDescriptorDetails().get().getPluginKey(), path);
        }
        catch (RuntimeException e)
        {
            log.warn("uri resolver has thrown a runtime exception", e);
            return Optional.absent();
        }
    }

    private Option<URI> safelyGetUriFromPluggableResolver(WebHookListenerRegistrationDetails listenerDetails, URI path, UriResolver uriResolver)
    {
        try
        {
            return uriResolver.getUri(listenerDetails, path);
        }
        catch (RuntimeException e)
        {
            log.warn("uri resolver has thrown a runtime exception", e);
            return Option.none();
        }
    }

    private URI addBaseUrlIfRelative(URI path)
    {
        return path.toString().startsWith("/") ? URI.create(applicationProperties.getBaseUrl(UrlMode.CANONICAL) + path.toString()) : path;
    }
}
