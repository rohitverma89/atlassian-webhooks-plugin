package com.atlassian.webhooks.plugin.api;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.util.Channel;
import com.atlassian.webhooks.api.util.Vote;
import com.atlassian.webhooks.spi.WebHookListenerAccessVoter;

import javax.annotation.Nonnull;

/**
 * Default AccessVoter that allows ADMINs and callers over the SERVICE channel all access. For any other situations,
 * ABSTAIN is returned.
 */
public class DefaultWebHookListenerAccessVoter implements WebHookListenerAccessVoter
{
    private final UserManager userManager;

    public DefaultWebHookListenerAccessVoter(UserManager userManager)
    {
        this.userManager = userManager;
    }

    @Nonnull
    @Override
    public Vote canCreate(@Nonnull PersistentWebHookListener listener, @Nonnull Channel channel)
    {
        return getDefaultVote(channel);
    }

    @Nonnull
    @Override
    public Vote canRead(@Nonnull PersistentWebHookListener listener, @Nonnull Channel channel)
    {
        return getDefaultVote(channel);
    }

    @Nonnull
    @Override
    public Vote canAdmin(@Nonnull PersistentWebHookListener listener, @Nonnull Channel channel)
    {
        return getDefaultVote(channel);
    }

    @Nonnull
    @Override
    public Vote canPublish(@Nonnull WebHookEvent webHookEvent, @Nonnull WebHookListener listener)
    {
        return Vote.ALLOW;
    }

    /**
     * @param channel the channel
     * @return ALLOW if the channel is SERVICE or if the current user is an ADMIN
     */
    private Vote getDefaultVote(Channel channel)
    {
        if (channel == Channel.SERVICE)
        {
            return Vote.ALLOW;
        }

        UserKey key = userManager.getRemoteUserKey();
        if (key != null && userManager.isAdmin(key))
        {
            return Vote.ALLOW;
        }

        return Vote.ABSTAIN;
    }
}
