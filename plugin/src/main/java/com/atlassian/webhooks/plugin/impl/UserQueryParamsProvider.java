package com.atlassian.webhooks.plugin.impl;

import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import com.atlassian.webhooks.spi.QueryParamsProvider;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import java.util.Optional;
import java.util.function.Function;
import javax.inject.Named;

@Named ("userQueryParamProvider")
public class UserQueryParamsProvider implements QueryParamsProvider
{
    @Override
    public Multimap<String, String> provideQueryParams(Optional<UserProfile> user, WebHookListenerRegistrationDetails registrationDetails)
    {
        return user.map(new Function<UserProfile, Multimap<String, String>>()
        {
            @Override
            public Multimap<String, String> apply(UserProfile user)
            {
                return ImmutableMultimap.of(
                        "user_id", user.getUsername(),
                        "user_key", user.getUserKey().getStringValue()
                );
            }
        }).orElse(ImmutableMultimap.<String, String>of());
    }
}
