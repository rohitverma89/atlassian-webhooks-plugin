package com.atlassian.webhooks.plugin.module;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;

import javax.inject.Inject;
import javax.inject.Named;

@ModuleType (ListableModuleDescriptorFactory.class)
@Named("webHookRegistrationTypeFactory")
public class WebHookRegistrationTypeFactory extends SingleModuleDescriptorFactory<WebHookRegistrationFactoryModuleDescriptor>
{
    @Inject
    public WebHookRegistrationTypeFactory(final HostContainer hostContainer)
    {
        super(hostContainer, "webhook-plugin-registration-factory", WebHookRegistrationFactoryModuleDescriptor.class);
    }
}
