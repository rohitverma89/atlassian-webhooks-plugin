package com.atlassian.webhooks.plugin.store;


import net.java.ao.Entity;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;

import java.util.Date;

public interface WebHookListenerAO extends Entity
{
    @NotNull
    @StringLength (StringLength.UNLIMITED)
    String getUrl();
    void setUrl(String url);

    @NotNull
    @StringLength(StringLength.UNLIMITED)
    String getName();
    void setName(String name);

    @StringLength(StringLength.UNLIMITED)
    String getDescription();
    void setDescription(String description);

    String getLastUpdatedUser();
    void setLastUpdatedUser(String username);

    @NotNull
    java.util.Date getLastUpdated();
    void setLastUpdated(Date updated);

    boolean isExcludeBody();
    void setExcludeBody(boolean excludeBody);

    @StringLength(StringLength.UNLIMITED)
    String getFilters();
    void setFilters(String filtersJson);

    // This is a legacy column, not used anymore. Nothing should be in there, but we leave it just in case
    @StringLength(StringLength.UNLIMITED)
    String getParameters();
    void setParameters(String parameters);

    // Was this created via REST, UI or SERVICE
    @NotNull
    String getRegistrationMethod();
    void setRegistrationMethod(String method);

    @StringLength(StringLength.UNLIMITED)
    String getEvents();
    void setEvents(String events);

    boolean isEnabled();
    void setEnabled(boolean enabled);
}
