package com.atlassian.webhooks.plugin.impl;

import com.atlassian.fugue.Effect;
import com.atlassian.httpclient.api.Request;
import com.atlassian.osgi.tracker.WaitableServiceTrackerCustomizer;
import com.atlassian.osgi.tracker.WaitableServiceTrackerFactory;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.spi.RequestSigner;
import com.atlassian.webhooks.spi.RequestSigner2;
import com.google.common.annotations.VisibleForTesting;

import java.net.URI;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Signer implementation which gets all singers (including implementations of deprecated interfaces) and
 * passes {@link com.atlassian.httpclient.api.Request.Builder} to these components for signing.
 */
@Named ("requestSigner")
public final class RequestSignerImpl implements RequestSigner2
{
    private final WebHookPluginRegistrationContainer container;
    private final Set<com.atlassian.webhooks.spi.plugin.RequestSigner> legacyRequestSigners;
    private final RequestSignerWaitableServiceTrackerCustomizer tracker;

    @Inject
    public RequestSignerImpl(final WaitableServiceTrackerFactory factory, final WebHookPluginRegistrationContainer container)
    {
        this.container = container;
        this.legacyRequestSigners = new HashSet<com.atlassian.webhooks.spi.plugin.RequestSigner>();
        this.tracker = new RequestSignerWaitableServiceTrackerCustomizer();
        factory.create(com.atlassian.webhooks.spi.plugin.RequestSigner.class, tracker);
    }

    @Override
    public void sign(final URI uri, final Optional<UserProfile> userProfile, final WebHookListenerRegistrationDetails registrationDetails, final Request.Builder request)
    {
        for (RequestSigner requestSigner : container.getRequestSigners())
        {
            requestSigner.sign(uri, registrationDetails, request);
        }

        for (RequestSigner2 requestSigner2 : container.getRequestSigners2())
        {
            requestSigner2.sign(uri, userProfile, registrationDetails, request);
        }

        for (final com.atlassian.webhooks.spi.plugin.RequestSigner legacyRequestSigner : legacyRequestSigners)
        {
            registrationDetails.getModuleDescriptorDetails().foreach(new Effect<WebHookListenerRegistrationDetails.ModuleDescriptorRegistrationDetails>()
            {
                @Override
                public void apply(final WebHookListenerRegistrationDetails.ModuleDescriptorRegistrationDetails moduleDescriptorRegistrationDetails)
                {
                    legacyRequestSigner.sign(uri, moduleDescriptorRegistrationDetails.getPluginKey(), request);
                }
            });
        }
    }

    @VisibleForTesting
    public class RequestSignerWaitableServiceTrackerCustomizer implements WaitableServiceTrackerCustomizer<com.atlassian.webhooks.spi.plugin.RequestSigner>
    {
        @Override
        public com.atlassian.webhooks.spi.plugin.RequestSigner adding(final com.atlassian.webhooks.spi.plugin.RequestSigner requestSigner)
        {
            legacyRequestSigners.add(requestSigner);
            return requestSigner;
        }

        @Override
        public void removed(final com.atlassian.webhooks.spi.plugin.RequestSigner requestSigner)
        {
            legacyRequestSigners.remove(requestSigner);
        }
    }

    @VisibleForTesting
    public RequestSignerWaitableServiceTrackerCustomizer getTracker()
    {
        return tracker;
    }
}
