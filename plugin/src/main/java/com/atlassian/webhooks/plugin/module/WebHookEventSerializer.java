package com.atlassian.webhooks.plugin.module;

import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.util.EventSerializers;
import com.atlassian.webhooks.spi.EventSerializer;
import com.google.common.annotations.VisibleForTesting;

import javax.inject.Inject;
import javax.inject.Named;

@Named("webHookEventSerializer")
public class WebHookEventSerializer
{
    private final WebHookPluginRegistrationContainer container;

    @Inject
    public WebHookEventSerializer(WebHookPluginRegistrationContainer container)
    {
        this.container = container;
    }

    public String serialize(WebHookEvent webHookEvent, WebHookListener listener)
    {
        if (listener.getParameters().isExcludeBody())
        {
            return "";
        }
        else
        {
            EventSerializer serializer = serializerFor(webHookEvent.getEvent().getClass());
            return serializer.serialize(webHookEvent.getEvent(), listener.getParameters()::getPropertyKeys);
        }

    }

    @VisibleForTesting
    <T> EventSerializer<? super T> serializerFor(Class<T> eventClass)
    {
        return container.getEventSerializers().forType(eventClass).or(EventSerializers.REFLECTION);
    }
}
