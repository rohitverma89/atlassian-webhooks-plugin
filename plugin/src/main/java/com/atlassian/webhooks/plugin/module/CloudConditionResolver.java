package com.atlassian.webhooks.plugin.module;

/**
 * Checks if the plugin is running on Cloud or BTF instance. It returns true if all implementations of
 * {@link com.atlassian.webhooks.spi.WebHookPluginRegistrationFactory.CloudCondition} return true (preferably there
 * is just one, or multiple returning the same answer).
 * By default it returns false.
 */
public interface CloudConditionResolver
{
    boolean isCloud();
}
