package com.atlassian.webhooks.plugin.module;

import com.atlassian.webhooks.spi.UriVariablesProvider;

public interface WebHookUriSubstitutions
{
    /**
     * Returns all implementations of {@link com.atlassian.webhooks.spi.UriVariablesProvider}
     * for the given event class.
     */
    <E> Iterable<UriVariablesProvider> substitutionForEventClass(Class<E> eventClass);
}
