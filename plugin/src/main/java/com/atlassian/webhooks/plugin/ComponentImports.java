package com.atlassian.webhooks.plugin;


import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.HelpPathResolver;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.vcache.VCacheFactory;

import javax.inject.Named;

/**
 * Helper class to configure all OSGI imported services
 */
@Named("componentImports")
class ComponentImports
{
    @ComponentImport("ao") ActiveObjects ao;
    @ComponentImport("applicationProperties") ApplicationProperties applicationProperties;
    @ComponentImport("eventPublisher") EventPublisher eventPublisher;
    @ComponentImport("httpClient") HttpClient httpClient;
    @ComponentImport("i18nResolver") I18nResolver i18nResolver;
    @ComponentImport("helpPathResolver") HelpPathResolver helpPathResolver;
    @ComponentImport("moduleFactory") ModuleFactory moduleFactory;
    @ComponentImport("pluginAccessor") PluginAccessor pluginAccessor;
    @ComponentImport("pluginEventManager") PluginEventManager pluginEventManager;
    @ComponentImport("templateRenderer") TemplateRenderer templateRenderer;
    @ComponentImport("userManager") UserManager userManager;
    @ComponentImport("vcacheFactory") VCacheFactory vcacheFactory;
    @ComponentImport("webResourceManager") WebResourceManager webResourceManager;
    @ComponentImport("webSudoManager") WebSudoManager webSudoManager;
}
