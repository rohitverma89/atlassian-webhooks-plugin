var webhooks = (function () {

    var TEMPLATES = atl.plugins.webhooks.admin.templates;

    var events = _.extend({}, Backbone.Events);
    var sections = [];

    var dateFormatter = function (date) {
        return date.toLocaleDateString() + " " + date.toLocaleTimeString();
    };

    var WebHookModel = Backbone.Model.extend({
        url: function () {
            return this.get("self") || this.collection.url;
        },
        defaults: {
            name: AJS.I18n.getText('webhooks.add.newwebhookname'),
            description: '',
            url: 'http://example.com/rest/webhooks/webhook1',
            lastUpdatedUser: '',
            lastUpdatedDisplayName: '',
            events: [],
            enabled: true,
            filters: {}
        },
        idAttribute: "self"
    });

    var WebHooksCollection = Backbone.Collection.extend({
        url: AJS.contextPath() + '/rest/webhooks/1.0/webhook',
        model: WebHookModel,
        comparator: function (left, right) {
            return left.get("name").localeCompare(right.get("name"))
        },
        initialize: function () {
            this.on("change:name", function () {
                this.sort()
            }, this)
        }
    });

    var SelectionModel = Backbone.Model.extend({
        select: function (model) {
            if (this.isLocked()) {
                var answer = confirm(AJS.I18n.getText("webhooks.leave.page.confirmation"));
                if (!answer) return;
            }
            this.set("selection", model);
        },
        getSelected: function () {
            return this.get("selection");
        },
        lock: function () {
            this.set("locked", true);
        },
        unlock: function () {
            this.unset("locked");
        },
        isLocked: function () {
            return this.has("locked");
        },
        onSelectionChange: function (callback, that) {
            this.on("change:selection", callback, that)
        },
        onLockChange: function (callback, that) {
            this.on("change:locked", callback, that)
        }
    });

    var WebHookRow = Backbone.View.extend({
        tagName: 'li',
        events: {
            "click": "rowClicked"
        },
        initialize: function () {
            this.selectionModel = this.options.selectionModel;
            this.selectionModel.onSelectionChange(this.selectionChanged, this);
            this.model.on("remove", this.modelRemoved, this);
            this.model.on("change", this.change, this);
        },
        render: function () {
            this.$el.html(TEMPLATES.webhookRow(this.model.attributes));
            this.$el.toggleClass("webhook-disabled", !this.model.get("enabled"));

            this.selectionChanged(this.selectionModel, this.selectionModel.getSelected()); // handle event handlers reorder
            return this;
        },
        change: function () {
            this.$el.toggleClass("webhook-disabled", !this.model.get("enabled"));
        },
        rowClicked: function () {
            this.selectionModel.select(this.model);
        },
        selectionChanged: function (selectionModel, selectedModel) {
            this.$el.toggleClass("highlighted", selectedModel === this.model);
        },
        modelRemoved: function () {
            this.destroy();
            this.remove();
        },
        destroy: function () {
            this.undelegateEvents();
            this.selectionModel.off(null, null, this);
            this.model.off(null, null, this);
        }
    });

    var WebHookDetailsView = Backbone.View.extend({
        el: ".webhook-details",
        events: {
            "submit form": "submit",
            "click #webhook-edit": "editMode",
            "click #webhook-delete": "deleteWebhook",
            "click #webhook-disabled": "toggleEnable",
            "click #webhook-enabled": "toggleEnable",
            "click #webhook-cancel": "cancel"
        },
        initialize: function () {
            var $el = this.$el;
            this.statusEnabled = true;
            this.$form = $el.find("form");
            this.$globalGessage = $el.find("#webhook-global-message");
            this.$enabledLozenge = this.$form.find('#webhook-enabled-lozenge');
            this.$disabledLozenge = this.$form.find('#webhook-disabled-lozenge');
            this.$editedDate = $el.find("#webhook-edited-date");
            this.$editedBy = $el.find("#webhook-edited-by");
            this.$name = $el.find("#webhook-name");
            this.$nameDisplay = $el.find("#webhook-name-display");
            this.$enabled = $el.find("#webhook-enabled");
            this.$disabled = $el.find("#webhook-disabled");
            this.$url = $el.find("#webhook-url");
            this.$urlDisplay = $el.find("#webhook-url-display");
            this.$description = $el.find("#webhook-description");
            this.$descriptionDisplay = $el.find("#webhook-description-display");
            this.$eventCheckboxes = $el.find(".webhook-checkbox");
            this.$noEventsSelected = $el.find("#webhook-no-events-selected");
            this.$sectionDisplay = $el.find(".display-mode-display .webhook-section");
            this.$groupDisplay = $el.find(".display-mode-display .webhook-group");
            this.$eventDisplay = $el.find(".display-mode-display .webhook-group li");
            this.$excludeDetails = $el.find("#webhook-exclude-details");
            this.$excludeDetailsDisplay = $el.find("#webhook-exclude-details-display");
            this.$submit = this.$form.find('#webhook-submit');

            this.selectedModel = undefined;
            this.selectionModel = this.model;
            this.selectionModel.onSelectionChange(this.selectionChanged, this);

            this.$form.on("cancel", function () {
                return false
            }); // so dirty form warning works after cancel

            events.trigger("sections:init", selectionModel);

            var $ = AJS.$;

            this.renderAvailableVariables = function() {

                var variablesMap = {};
                $(".webhook-event").find("input:checked").each(function () {
                    variablesMap[$(this).attr("data-event-type")] = $(this).attr("urlVariables").split(",");
                });

                var sum = _.uniq(_.flatten(_.values(variablesMap)));
                var intersection = _.reduce(_.values(variablesMap), function (memo, newArray) {
                    return _.intersection(memo, newArray);
                }, sum);
                var other = _.difference(sum, intersection);

                $("li.variable").each(function () {
                    $(this).removeClass("variable-intersection");
                    $(this).removeClass("variable-some");
                    $(this).addClass("variable-unavailable")
                    $(this).attr("title", AJS.I18n.getText("webhooks.variables.usage.none"));
                    if (_.indexOf(intersection, $(this).attr("variable")) != -1) {
                        $(this).addClass("variable-intersection");
                        $(this).removeClass("variable-unavailable");
                        $(this).attr("title", AJS.I18n.getText("webhooks.variables.usage.all"));
                    }
                    if (_.indexOf(other, $(this).attr("variable")) != -1) {
                        $(this).addClass("variable-some");
                        $(this).removeClass("variable-unavailable");
                        $(this).attr("title", AJS.I18n.getText("webhooks.variables.usage.some"));
                    }
                });

            }

            var urlInput = this.$url;

            $el.find("[urlVariables]").change(this.renderAvailableVariables);
            $el.find(".variable").click(function () {
                var variable = $(this).text();
                urlInput.val(urlInput.val() + variable);
                urlInput.focus();
            });
        },
        render: function () {

            var renderSectionFilters = function (filtersMap, selectionModel) {
                _.each(_.pairs(sections), function (section) {
                    section[1].setFilter(filtersMap[section[0]] || "");
                });
            };

            var that = this;

            // this setting the height of the webhooks navigation to 100% of webhook section height
            var sectionHeight = AJS.$("section.aui-page-panel-content").outerHeight();
            var headerHeight = AJS.$(".webhooks .aui-page-header").outerHeight();
            AJS.$(".webhooks .aui-page-panel-nav").height(sectionHeight - headerHeight);

            this.$form.find(".error").empty();
            this.$el.find(".1buttons-container").toggle(!!this.selectedModel);
            if (this.selectedModel) {
                var model = this.selectedModel;
                this.statusEnabled = model.get("enabled");

                if (this.statusEnabled) {
                    this.$enabledLozenge.show();
                    this.$disabledLozenge.hide();
                } else {
                    this.$enabledLozenge.hide();
                    this.$disabledLozenge.show();
                }

                this.$editedBy.html(model.get("lastUpdatedDisplayName"));

                var rawUpdatedDate = model.get("lastUpdated");
                var updatedDate = new Date(rawUpdatedDate);

                var updatedDateString = dateFormatter(updatedDate);
                this.$editedDate.html(updatedDateString);

                this.$name.val(model.get("name"));
                this.$nameDisplay.text(model.get("name"));
                this.$url.val(model.get("url"));
                this.$urlDisplay.text(model.get("url")).attr({href: model.get("url")});
                this.$description.val(model.get("description"));
                this.$descriptionDisplay.text(model.get("description"));

                this.renderStatusButtons();
                this.renderEvents();

                this.$excludeDetails.prop("checked", this.selectedModel.get("excludeBody"));
                var excludeDetailsDisplayText = this.selectedModel.get("excludeBody") ? AJS.I18n.getText('common.words.yes') : AJS.I18n.getText('common.words.no');
                this.$excludeDetailsDisplay.text(excludeDetailsDisplayText);

                renderSectionFilters(model.get("filters"), this.selectionModel);
                this.renderAvailableVariables();

                this.$submit.val(model.isNew() ? AJS.I18n.getText('webhooks.create') : AJS.I18n.getText('webhooks.save'));
                this.$el.find('input.text, textarea').each(function (idx, el) {
                    el.defaultValue = el.value
                });

                this.$globalGessage.empty();
            } else {
                this.$name.val("");
                this.$nameDisplay.text("");
                this.$url.val("");
                this.$urlDisplay.text("");
                this.statusEnabled = true;
                this.$eventCheckboxes.each(function () {
                    that.$(this).prop("checked", false);
                });
                renderSectionFilters({}, this.selectionModel);
            }
        },
        renderStatusButtons: function () {
            this.$enabled.toggleClass("active", this.statusEnabled);
            this.$enabled.prop("disabled", this.statusEnabled);
            this.$disabled.toggleClass("active", !this.statusEnabled);
            this.$disabled.prop("disabled", !this.statusEnabled);
        },
        renderEvents: function () {
            var that = this;
            var events = this.selectedModel.get("events");

            this.$noEventsSelected.css("display", events.length == 0 ? "" : "none");

            this.$eventCheckboxes.each(function () {
                var $checkbox = that.$(this);
                $checkbox.prop("checked", _.indexOf(events, $checkbox.attr('data-event-type')) != -1);
            });

            this.$eventDisplay.removeClass("show");
            this.$eventDisplay.removeClass("last");
            this.$eventDisplay.each(function () {
                var $event = that.$(this);
                var exist = _.indexOf(events, $event.attr('data-event-type')) != -1;
                if (exist) {
                    $event.addClass("show");
                }
            });

            this.$groupDisplay.removeClass("show");
            this.$groupDisplay.each(function () {
                var $group = that.$(this);
                var events = $group.find("li.show");
                if (events.size() > 0) {
                    $group.addClass("show");
                    events.last().addClass("last");
                }
            });

            this.$sectionDisplay.removeClass("show");
            this.$sectionDisplay.each(function () {
                var $section = that.$(this);
                if ($section.find(".webhook-group.show").size() > 0) {
                    $section.addClass("show");
                }
            });
        },
        editMode: function () {
            this.selectionModel.lock();
            this.$form.addClass("display-mode-edit").removeClass("display-mode-display");
            this.$noEventsSelected.css("display", "none");
        },
        displayMode: function () {
            this.selectionModel.unlock();
            this.$form.addClass("display-mode-display").removeClass("display-mode-edit");
        },
        toggleEnable: function (e) {
            this.statusEnabled = !this.statusEnabled;
            this.renderStatusButtons();
            return false;
        },
        getEvents: function () {
            var events = [];
            var that = this;
            this.$(".webhook-event").find("input:checked").each(function (checkbox) {
                events.push(that.$(this).attr("data-event-type"));
            });
            return events;
        },
        submit: function () {

            var getFilters = function () {
                var result = {};
                _.each(_.pairs(sections), function (section) {
                    result[section[0]] = section[1].getFilter();
                });
                return result;
            };

            this.$form.find('.error').empty();
            var that = this;
            var wasNew = this.selectedModel.isNew();

            this.selectedModel.save({
                name: this.$name.val(),
                description: this.$description.val(),
                url: this.$url.val(),
                events: this.getEvents(),
                excludeBody: this.$excludeDetails.is(":checked"),
                enabled: this.statusEnabled,
                filters: getFilters()
            }, {
                wait: true, success: submitSuccess, error: submitError
            });

            return false; // so dirty form warning works after first submit

            function scrollTop() {
                AJS.$('html, body').animate({scrollTop: AJS.$('.webhooks').offset().top}, 400);
            }

            function submitSuccess(model, response) {
                var successMessage;
                if (wasNew) {
                    successMessage = AJS.I18n.getText("webhooks.create.success", model.get("name"));
                } else {
                    successMessage = AJS.I18n.getText("webhooks.update.success", model.get("name"));
                }
                displaySuccessMessage(successMessage);
                that.displayMode();
                scrollTop();
            }

            function submitError(model, response) {
                try {
                    var errorObject = AJS.$.parseJSON(response.responseText);
                } catch (parseError) {
                    errorObject = {};
                }
                if (response.status == 404) {
                    displayErrorMessage(AJS.I18n.getText("webhooks.submit.notfound"));
                } else if (response.status == 400 || response.status == 409) {
                    var fieldErrors = errorObject.messages || {};
                    _.each(fieldErrors, function (error, index) {
                        if (error.arguments) {
                            that.$el.find("#webhook-" + error.key).siblings(".error").text(error.arguments[0]);
                        } else {
                            AJS.messages.error(that.$globalGessage, {
                                title: AJS.I18n.getText("webhooks.submit.duplicate.title"),
                                body: "<p>" + AJS.escapeHtml(error.key) + "</p>",
                                closeable: false});
                        }
                    });
                } else {
                    displayErrorMessage(AJS.I18n.getText("webhooks.submit.error", response.status, response.statusText))

                }
                scrollTop();
            }
        }, cancel: function () {
            this.displayMode();
            if (this.selectedModel.isNew()) {
                this.selectedModel.destroy();
            } else {
                this.render();
            }
        },
        deleteWebhook: function () {
            this.selectedModel && deleteWebhook(this.selectedModel);
        },
        selectionChanged: function (selectionModel, selectedModel) {
            if (this.selectedModel && this.selectedModel.isNew()) {
                this.selectedModel.destroy();
            }
            this.displayMode();
            if (this.selectedModel) {
                this.selectedModel.off(null, null, this); // unbind any listeners created by this view
            }

            this.selectedModel = selectedModel;

            if (this.selectedModel) {
                this.selectedModel.on("change", this.render, this);
                selectedModel.isNew() && this.editMode();
            }

            this.render();
        }

    });

    var WebHooksNav = Backbone.View.extend({
        tagName: 'ul',
        className: 'aui-nav',
        initialize: function () {
            this.model.on('add', this.modelAdded, this);
            this.model.on('reset', this.reset, this);
            this.$rowsCollection = [];
        },
        reset: function () {
            this.$el.empty();
            _.each(this.$rowsCollection, function (webHookRow) {
                webHookRow.destroy()
            });
            this.$rowsCollection = [];

            var that = this;
            this.model.each(function (model) {
                var webHookRow = new WebHookRow({model: model, selectionModel: that.options.selectionModel});
                that.$el.append(webHookRow.render().$el);
                that.$rowsCollection.push(webHookRow);
            });
        },
        render: function () {
            return this;
        },
        modelAdded: function (model, collection, options) {
            var webHookRow = new WebHookRow({model: model, selectionModel: this.options.selectionModel});
            this.$el.prepend(webHookRow.render().$el);
            this.$rowsCollection.push(webHookRow);
        }
    });

    var AddWebhookButton = Backbone.View.extend({
        events: { "click": "addWebhook" },
        initialize: function () {
            this.model.onLockChange(this.render, this)
        },
        render: function () {
            this.$el.toggleClass("disabled", this.model.isLocked())
        },
        addWebhook: function () {
            !this.model.isLocked() && this.options.webHooksModel.add({/* defaults*/}, {at: 0});
            return false;
        }
    });

    var GlobalPageView = Backbone.View.extend({
        el: ".webhooks",
        initialize: function () {
            this.model.on('reset add remove', this.countChanged, this)
        },
        countChanged: function (collection) {
            var empty = this.model.length == 0;
            this.$el.find(".on-webhooks-absent").toggleClass("hidden", !empty);
            this.$el.find(".on-webhooks-present").toggleClass("hidden", empty);
        }
    });

    var webHooksModel = new WebHooksCollection;
    var selectionModel = new SelectionModel;

    webHooksModel.on('remove', function (removedModel) {
        if (selectionModel.getSelected() === removedModel) {
            selectionModel.select(webHooksModel.at(0));
        }
    });
    webHooksModel.on('add', function (model, collection, options) {
        selectionModel.select(model);
    });

    AJS.$(function () {
        new AddWebhookButton({model: selectionModel, el: "#add-webhook", webHooksModel: webHooksModel});
        new WebHookDetailsView({model: selectionModel}).render();
        AJS.$(".aui-navgroup-inner").append(new WebHooksNav({model: webHooksModel, selectionModel: selectionModel}).render().$el);
        new GlobalPageView({model: webHooksModel});

        webHooksModel.fetch({
            error: function (model, response) {
                displayErrorMessage(AJS.I18n.getText("webhooks.fetch.error", response.status, response.statusText))
            },
            success: function (model) {
                selectionModel.select(model.at(0))
            }
        });

        AJS.$("#webhook-submit, #webhook-edit").click(function () {
            AJS.$("#webhook-global-message").empty();
        });
    });

    function deleteWebhook(model) {
        if (selectionModel.isLocked()) {
            return;
        }

        var popup = new AJS.Dialog();

        function destroyModel() {
            popup.remove();
            model.destroy({
                wait: true,
                success: function (model, response) {
                    displaySuccessMessage(AJS.I18n.getText("webhooks.delete.success", model.get("name")));
                },
                error: function (model, response) {
                    if (response.status == 409) {
                        var errorResponse = AJS.$.parseJSON(response.responseText);
                        var messages = errorResponse.messages;
                        displayErrorMessageNoEscape(_.pluck(messages, 'arguments').join("<br />"));
                    } else {
                        displayErrorMessage(AJS.I18n.getText("webhooks.delete.error", model.get("name"), response.status, response.statusText));
                    }
                }
            });
        }

        popup.addHeader(AJS.I18n.getText('webhooks.delete.title'))
            .addPanel('warning-message', AJS.$('<p>').text(AJS.I18n.getText('webhooks.delete.confirm', model.get('name'))))
            .addButton(AJS.I18n.getText('webhooks.delete'), destroyModel, 'aui-button')
            .addCancel(AJS.I18n.getText('webhooks.cancel'), function () {
                popup.remove();
            }).show().updateHeight();
    }

    function displaySuccessMessage(message) {
        AJS.messages.success(AJS.$("#webhook-global-message"), {
            body: AJS.escapeHtml(message),
            closeable: true
        });
    }

    function displayErrorMessage(message) {
        AJS.messages.error(AJS.$("#webhook-global-message"), {
            body: AJS.escapeHtml(message),
            closeable: true
        });
    }

    function displayErrorMessageNoEscape(message) {
        AJS.messages.error(AJS.$("#webhook-global-message"), {
            body: message,
            closeable: true
        });
    }

    return {
        addFilter: function (sectionName, getFilter, setFilter) {
            sections[sectionName] = {
                getFilter: getFilter,
                setFilter: setFilter
            }
        },
        setDateFormatter: function (customDateFormatter) {
            dateFormatter = customDateFormatter;
        },
        onInit: function (callback) {
            events.on("sections:init", callback);
        }
    };
})();
