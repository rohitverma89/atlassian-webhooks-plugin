package com.atlassian.webhooks.plugin;

import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.plugin.impl.CompositeQueryParamsProvider;
import com.atlassian.webhooks.plugin.impl.UserQueryParamsProvider;
import com.atlassian.webhooks.plugin.module.WebHookEventSerializer;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.plugin.uri.UriVariablesResolver;
import com.atlassian.webhooks.spi.RequestSigner2;
import com.google.common.base.Optional;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PublishTaskFactoryImplTest
{
    private static final String USER_NAME = "scooby";
    private static final String USER_KEY = "ruh-roh";

    private UserProfile user;
    private WebHookListener webHookListener = WebHookListener.fromPersistentStore(42, "name", Optional.of("1")).to("/").build();
    private PublishTaskFactoryImpl publishTaskFactory;

    @Before
    public void setup()
    {
        user = mock(UserProfile.class);
        when(user.getUsername()).thenReturn(USER_NAME);
        when(user.getUserKey()).thenReturn(new UserKey(USER_KEY));
        CompositeQueryParamsProvider queryParamsProvider = new CompositeQueryParamsProvider(mock(WebHookPluginRegistrationContainer.class), new UserQueryParamsProvider());
        publishTaskFactory = new PublishTaskFactoryImpl(
                mock(HttpClient.class),
                mock(RequestSigner2.class),
                mock(UserManager.class),
                mock(UriVariablesResolver.class),
                mock(WebHookEventSerializer.class),
                queryParamsProvider
        );
    }

    @Test
    public void testUriParametersParse()
    {
        String uri = addQueryParams("http://poszter.herokuapp.com/?thisShouldStayInUrl", user);

        assertThat(uri, startsWith("http://poszter.herokuapp.com/?thisShouldStayInUrl"));
        assertThat(uri, containsString("user_id=" + USER_NAME));
        assertThat(uri, containsString("user_key=" + USER_KEY));
    }

    @Test
    public void testUriParametersAndFragmentParse()
    {
        String uri = addQueryParams("http://poszter.herokuapp.com/?thisShouldStayInUrl#ThisShouldGone", user);

        assertThat(uri, startsWith("http://poszter.herokuapp.com/?thisShouldStayInUrl"));
        assertThat(uri, not(containsString("ThisShouldGone")));
        assertThat(uri, containsString("user_id=" + USER_NAME));
        assertThat(uri, containsString("user_key=" + USER_KEY));
    }

    @Test
    public void testRequestUriInAnonymousContext()
    {
        String uri = addQueryParams("http://some-url.com?", null);

        assertThat(uri, not(containsString("user_key")));
        assertThat(uri, not(containsString("user_id")));
    }

    private String addQueryParams(String uri, UserProfile user)
    {
        return publishTaskFactory.addQueryParams(URI.create(uri), java.util.Optional.ofNullable(user), webHookListener).toString();
    }
}
