package com.atlassian.webhooks.plugin.service;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.Plugin;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.vcache.internal.core.ThreadLocalRequestContextSupplier;
import com.atlassian.webhooks.api.events.WebHookListenerCreatedEvent;
import com.atlassian.webhooks.api.events.WebHookListenerDeletedEvent;
import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerService;
import com.atlassian.webhooks.api.register.listener.WebHookListenerServiceResponse;
import com.atlassian.webhooks.api.util.Channel;
import com.atlassian.webhooks.api.util.MessageCollection;
import com.atlassian.webhooks.api.util.Vote;
import com.atlassian.webhooks.plugin.PluginProperties;
import com.atlassian.webhooks.plugin.TestVCacheFactory;
import com.atlassian.webhooks.plugin.api.SecuredWebHookListenerService;
import com.atlassian.webhooks.plugin.api.SecuredWebHookListenerServiceImpl;
import com.atlassian.webhooks.plugin.event.WebHookEventDispatcher;
import com.atlassian.webhooks.plugin.rest.WebHookListenerJsonBean;
import com.atlassian.webhooks.plugin.store.MockWebHookListenerStore;
import com.atlassian.webhooks.plugin.store.WebHookListenerCachingStore;
import com.atlassian.webhooks.spi.WebHookListenerAccessVoter;
import com.atlassian.webhooks.spi.WebHookListenerActionValidator;
import com.atlassian.webhooks.spi.WebHookListenerStore;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Map;

import static com.atlassian.webhooks.api.util.ListenerDuplicatePredicate.duplicateOf;
import static com.google.common.collect.Lists.newArrayList;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SecuredWebHookListenerServiceTest
{
    public static final String WEB_HOOK_NAME = "webhook name";
    public static final String ILLEGAL_WEBHOOK_NAME = "illegal_webhook_name";
    public static final String PERMANENT_WEBHOOK_NAME = "permanent_webhook_name";
    public static final String TARGET_URL = "http://webhook_target_url.com";
    public static final String DESCRIPTION = "webhook_description";
    public static final List<String> EVENTS = newArrayList("webhook_event");
    public static final boolean EXCLUDE_BODY = false;
    public static final Map<String, String> FILTERS = ImmutableMap.of("default", "");
    public static final String USER_KEY = "user_key";

    private final ThreadLocalRequestContextSupplier requestContextSupplier = ThreadLocalRequestContextSupplier.strictSupplier();

    @Mock
    private I18nResolver i18nResolver;
    private MockAccessVoter accessVoter;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private UserManager userManager;

    private SecuredWebHookListenerService service;

    @Before
    public void setUp()
    {
        accessVoter = new MockAccessVoter();
        when(userManager.getRemoteUserKey()).thenReturn(new UserKey(USER_KEY));
        WebHookListenerStore store = new MockWebHookListenerStore(userManager);
        WebHookListenerCachingStore webHookListenerCachingStore = new WebHookListenerCachingStore(store, TestVCacheFactory.create(requestContextSupplier));
        WebHookEventDispatcher webHookEventDispatcher = new WebHookEventDispatcher(eventPublisher);

        this.service = new SecuredWebHookListenerServiceImpl(accessVoter, i18nResolver, new Validator(), webHookListenerCachingStore, webHookEventDispatcher);

        Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn(PluginProperties.PLUGIN_KEY);
        webHookListenerCachingStore.onStart();
    }

    @Before
    public void initThread()
    {
        requestContextSupplier.initThread("tenant-123");
    }

    @After
    public void clearThread()
    {
        requestContextSupplier.clearThread();
    }

    @Test
    public void testWebHookRegistration()
    {
        PersistentWebHookListener persistentWebHookListener = new WebHookListenerJsonBean(WEB_HOOK_NAME, TARGET_URL, DESCRIPTION, EXCLUDE_BODY, FILTERS, EVENTS, true).toRegisteredWebHookListener();
        final WebHookListenerServiceResponse webHookListenerServiceResponse = service.registerWebHookListener(Channel.SERVICE, persistentWebHookListener);

        assertNotNull("Response from service can't be null", webHookListenerServiceResponse);
        assertTrue("Adding new listener didn't create any error messages", webHookListenerServiceResponse.getMessageCollection().isEmpty());
        assertTrue("Registered listener was returned from service", webHookListenerServiceResponse.getRegisteredListener().isDefined());
        assertEquals(WEB_HOOK_NAME, webHookListenerServiceResponse.getRegisteredListener().get().getName());

        assertThat(service.getAllWebHookListeners(Channel.SERVICE), containsListener(persistentWebHookListener));
    }

    @Test
    public void testWebHookRegistrationNoPermission()
    {
        accessVoter.vote = Vote.DENY;

        PersistentWebHookListener webHookListenerParameters = new WebHookListenerJsonBean(WEB_HOOK_NAME, TARGET_URL, DESCRIPTION, EXCLUDE_BODY, FILTERS, EVENTS, true).toRegisteredWebHookListener();
        final WebHookListenerServiceResponse webHookListenerServiceResponse = service.registerWebHookListener(Channel.SERVICE, webHookListenerParameters);

        assertNotNull("Response from service can't be null", webHookListenerServiceResponse);
        assertFalse("Adding new listener create an error message", webHookListenerServiceResponse.getMessageCollection().isEmpty());

        verifyZeroInteractions(eventPublisher);
    }

    @Test
    public void testWebHookRegistrationAndUpdate()
    {
        PersistentWebHookListener webHookListenerParameters = new WebHookListenerJsonBean(WEB_HOOK_NAME, TARGET_URL, DESCRIPTION, EXCLUDE_BODY, FILTERS, EVENTS, true).toRegisteredWebHookListener();
        WebHookListenerServiceResponse webHookListenerServiceResponse = service.registerWebHookListener(Channel.SERVICE, webHookListenerParameters);

        assertNotNull("Response from service can't be null", webHookListenerServiceResponse);
        assertTrue("Adding new listener didn't create any error messages", webHookListenerServiceResponse.getMessageCollection().isEmpty());
        assertTrue("Registered listener was returned from service", webHookListenerServiceResponse.getRegisteredListener().isDefined());
        assertEquals(WEB_HOOK_NAME, webHookListenerServiceResponse.getRegisteredListener().get().getName());
        Integer id = webHookListenerServiceResponse.getRegisteredListener().get().getId().get();

        final WebHookListenerService.WebHookListenerUpdateInput updateInput = WebHookListenerService.WebHookListenerUpdateInput.builder()
                .setDescription(DESCRIPTION)
                .setName(WEB_HOOK_NAME)
                .setUrl(TARGET_URL)
                .setExcludeBody(EXCLUDE_BODY)
                .setFilters(FILTERS)
                .setEvents(newArrayList("event"))
                .setEnabled(true)
                .build();

        webHookListenerServiceResponse = service.updateWebHookListener(Channel.SERVICE, id, updateInput);
        assertNotNull("Response from service can't be null", webHookListenerServiceResponse);
        assertTrue("Adding new listener didn't create any error messages", webHookListenerServiceResponse.getMessageCollection().isEmpty());
        assertTrue("Registered listener was returned from service", webHookListenerServiceResponse.getRegisteredListener().isDefined());
        assertEquals(WEB_HOOK_NAME, webHookListenerServiceResponse.getRegisteredListener().get().getName());
        assertEquals(id, webHookListenerServiceResponse.getRegisteredListener().get().getId().get());

//        assertThat("Service should contain updated listener", service.getAllWebHookListeners(Channel.SERVICE), new ContainsWebHookListenerMatcher(we));
        assertThat("Service shouldn't contain the first version of listener", service.getAllWebHookListeners(Channel.SERVICE), CoreMatchers.not(new ContainsWebHookListenerMatcher(webHookListenerParameters)));
    }

    @Test
    public void testUpdateOfWebHookSetsUserName()
    {
        PersistentWebHookListener webHookListenerParameters = new WebHookListenerJsonBean(WEB_HOOK_NAME, TARGET_URL, DESCRIPTION, EXCLUDE_BODY, FILTERS, EVENTS, true).toRegisteredWebHookListener();
        WebHookListenerServiceResponse webHookListenerServiceResponse = service.registerWebHookListener(Channel.SERVICE, webHookListenerParameters);

        assertNotNull(webHookListenerServiceResponse);
        assertTrue("Adding new listener didn't create any error messages", webHookListenerServiceResponse.getMessageCollection().isEmpty());
        assertTrue("Registered listener was returned from service", webHookListenerServiceResponse.getRegisteredListener().isDefined());

        PersistentWebHookListener webHookListener = webHookListenerServiceResponse.getRegisteredListener().get();

        assertEquals(USER_KEY, webHookListener.getLastUpdatedByUser());

        when(userManager.getRemoteUserKey()).thenReturn(new UserKey("different_user_key"));
        webHookListenerServiceResponse =
                service.updateWebHookListener(Channel.SERVICE, webHookListener.getId().get(), WebHookListenerService.WebHookListenerUpdateInput.builder().setName("changed_name").build());

        assertNotNull("Response from service can't be null", webHookListenerServiceResponse);
        assertTrue("Adding new listener didn't create any error messages", webHookListenerServiceResponse.getMessageCollection().isEmpty());
        assertTrue("Registered listener was returned from service", webHookListenerServiceResponse.getRegisteredListener().isDefined());
        assertEquals("different_user_key", webHookListenerServiceResponse.getRegisteredListener().get().getLastUpdatedByUser());
    }

    @Test
    public void testWebHookCreatedEventRaised()
    {
        PersistentWebHookListener webHookListenerParameters = new WebHookListenerJsonBean(WEB_HOOK_NAME, TARGET_URL, DESCRIPTION, EXCLUDE_BODY, FILTERS, EVENTS, true).toRegisteredWebHookListener();
        service.registerWebHookListener(Channel.SERVICE, webHookListenerParameters);
        verify(eventPublisher, times(1)).publish(argThat(new WebHookEventFiredMatcher(WebHookListenerCreatedEvent.class)));
    }

    @Test
    public void testDeletingWebHookListener()
    {
        PersistentWebHookListener webHookListenerParameters = new WebHookListenerJsonBean(WEB_HOOK_NAME, TARGET_URL, DESCRIPTION, EXCLUDE_BODY, FILTERS, EVENTS, true).toRegisteredWebHookListener();
        WebHookListenerServiceResponse webHookListenerServiceResponse = service.registerWebHookListener(Channel.SERVICE, webHookListenerParameters);

        assertNotNull("Response from service can't be null", webHookListenerServiceResponse);
        assertTrue("Adding new listener didn't create any error messages", webHookListenerServiceResponse.getMessageCollection().isEmpty());
        assertTrue("Registered listener was returned from service", webHookListenerServiceResponse.getRegisteredListener().isDefined());
        verify(eventPublisher, times(1)).publish(argThat(new WebHookEventFiredMatcher(WebHookListenerCreatedEvent.class)));

        final MessageCollection messageCollection = service.deleteWebHookListener(Channel.SERVICE, webHookListenerServiceResponse.getRegisteredListener().get().getId().get());
        verify(eventPublisher, times(1)).publish(argThat(new WebHookEventFiredMatcher(WebHookListenerDeletedEvent.class)));
        assertTrue("Deleting listener didn't create any error messages", messageCollection.isEmpty());
        assertThat(Lists.newArrayList(service.getAllWebHookListeners(Channel.SERVICE)), IsCollectionWithSize.hasSize(0));
    }

    @Test
    public void testCreatingWebHookWithIllegalParameters()
    {
        PersistentWebHookListener webHookListenerParameters = new WebHookListenerJsonBean(ILLEGAL_WEBHOOK_NAME, TARGET_URL, DESCRIPTION, EXCLUDE_BODY, FILTERS, EVENTS, true).toRegisteredWebHookListener();
        WebHookListenerServiceResponse webHookListenerServiceResponse = service.registerWebHookListener(Channel.SERVICE, webHookListenerParameters);
        assertNotNull("Response from service can't be null", webHookListenerServiceResponse);

        MessageCollection messageCollection = webHookListenerServiceResponse.getMessageCollection();

        assertFalse("Adding new listener created an error messages", messageCollection.isEmpty());
        assertEquals(messageCollection.getMessages().get(0).getKey(), "illegal.name");
    }

    @Test
    public void testDeletingWebHookWithIllegalParameters()
    {
        PersistentWebHookListener webHookListenerParameters = new WebHookListenerJsonBean(PERMANENT_WEBHOOK_NAME, TARGET_URL, DESCRIPTION, EXCLUDE_BODY, FILTERS, EVENTS, true).toRegisteredWebHookListener();
        WebHookListenerServiceResponse webHookListenerServiceResponse = service.registerWebHookListener(Channel.SERVICE, webHookListenerParameters);
        assertNotNull("Response from service can't be null", webHookListenerServiceResponse);
        assertTrue("Adding new listener didn't create any error messages", webHookListenerServiceResponse.getMessageCollection().isEmpty());
        assertTrue("Registered listener was returned from service", webHookListenerServiceResponse.getRegisteredListener().isDefined());

        MessageCollection messageCollection = service.deleteWebHookListener(Channel.SERVICE, webHookListenerServiceResponse.getRegisteredListener().get().getId().get());
        assertFalse("Removing new listener created an error messages", messageCollection.isEmpty());
        assertEquals(messageCollection.getMessages().get(0).getKey(), "cant.delete.webhook");

        assertThat(newArrayList(service.getAllWebHookListeners(Channel.SERVICE)), IsCollectionWithSize.hasSize(1));
    }

    private static class Validator implements WebHookListenerActionValidator
    {
        @Override
        public MessageCollection validateWebHookRegistration(final PersistentWebHookListener registrationParameters)
        {
            if (registrationParameters.getName().equals(ILLEGAL_WEBHOOK_NAME))
            {
                return MessageCollection.of("illegal.name", MessageCollection.Reason.VALIDATION_FAILED);
            }
            return MessageCollection.empty();
        }

        @Override
        public MessageCollection validateWebHookUpdate(final PersistentWebHookListener registrationParameters)
        {
            return MessageCollection.empty();
        }

        @Override
        public MessageCollection validateWebHookRemoval(final PersistentWebHookListener registrationParameters)
        {
            if (registrationParameters.getName().equals(PERMANENT_WEBHOOK_NAME))
            {
                return MessageCollection.of("cant.delete.webhook", MessageCollection.Reason.CONFLICT);
            }
            return MessageCollection.empty();
        }
    }

    private static Matcher<Iterable<PersistentWebHookListener>> containsListener(PersistentWebHookListener listener)
    {
        return new ContainsWebHookListenerMatcher(listener);
    }

    private static class ContainsWebHookListenerMatcher extends TypeSafeMatcher<Iterable<PersistentWebHookListener>>
    {
        private final PersistentWebHookListener webHookListenerParameters;

        public ContainsWebHookListenerMatcher(final PersistentWebHookListener webHookListenerParameters)
        {
            this.webHookListenerParameters = webHookListenerParameters;
        }

        @Override
        public boolean matchesSafely(final Iterable<PersistentWebHookListener> webHookListenerParametersCollection)
        {
            return Iterables.tryFind(webHookListenerParametersCollection, duplicateOf(webHookListenerParameters)).isPresent();
        }

        @Override
        public void describeTo(final Description description)
        {
            description.appendText("Service doesn't contain webhook " + webHookListenerParameters);
        }
    }

    private static class MockAccessVoter implements WebHookListenerAccessVoter
    {
        private Vote vote = Vote.ALLOW;

        @Nonnull
        @Override
        public Vote canCreate(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel)
        {
            return vote;
        }

        @Nonnull
        @Override
        public Vote canRead(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel)
        {
            return vote;
        }

        @Nonnull
        @Override
        public Vote canAdmin(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel)
        {
            return vote;
        }

        @Nonnull
        @Override
        public Vote canPublish(@Nonnull WebHookEvent webHookEvent, @Nonnull WebHookListener listener)
        {
            return vote;
        }
    }

    private static class WebHookEventFiredMatcher extends TypeSafeMatcher<Object>
    {
        private final Class<?> webHookCreatedEventClass;

        public WebHookEventFiredMatcher(final Class<?> webHookCreatedEventClass)
        {
            this.webHookCreatedEventClass = webHookCreatedEventClass;
        }

        @Override
        public boolean matchesSafely(final Object o)
        {
            return webHookCreatedEventClass.isInstance(o);
        }

        @Override
        public void describeTo(final Description description)
        {
            description.appendText("Event isn't a subclass of " + webHookCreatedEventClass.getName());
        }
    }
}
