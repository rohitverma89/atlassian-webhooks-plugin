package com.atlassian.webhooks.plugin.impl;

import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.spi.QueryParamsProvider;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CompositeQueryParamsProviderTest
{
    private static final String userFullName = "UserFullName";
    private static final String userName = "UserName";
    private static final String addOnKey1 = "addOnKey1";
    private static final String addOnKey2 = "addOnKey2";

    private UserProfile userProfile;
    private WebHookListenerRegistrationDetails registrationDetails;

    @Before
    public void setUp() throws Exception
    {
        userProfile = mock(UserProfile.class);
        when(userProfile.getFullName()).thenReturn(userFullName);
        when(userProfile.getUsername()).thenReturn(userName);

        registrationDetails = WebHookListenerRegistrationDetails.moduleDescriptor(addOnKey1);
    }

    @Test
    public void paramsAreCombinedFromProvidersThatComeFromContainerAndConstructor()
    {
        QueryParamsProvider queryParamsProvider = createQueryParamsProviderImpl(
                new UserFullNameQueryParamProvider(),
                new AddOnKeyQueryParamProvider()
        );

        Multimap<String, String> params = queryParamsProvider.provideQueryParams(Optional.of(userProfile), registrationDetails);

        assertThat(params, is((Multimap<String, String>) ImmutableMultimap.of(
                "user-name", userFullName,
                "add-key", addOnKey1
        )));
    }

    @Test
    public void multipleParamsWithSameKeyAreSupported()
    {
        QueryParamsProvider queryParamsProvider = createQueryParamsProviderImpl(
                new UserFullNameQueryParamProvider(),
                new AddOnKeyQueryParamProvider(),
                new AddOnKeyAndUserNameQueryParamProvider()
        );

        Multimap<String, String> params = queryParamsProvider.provideQueryParams(Optional.of(userProfile), registrationDetails);

        assertThat(params, is((Multimap<String, String>) ImmutableMultimap.of(
                "user-name", userName,
                "user-name", userFullName,
                "add-key", addOnKey1,
                "add-key", addOnKey2
        )));
    }

    @Test
    public void providerThatThrowExceptionsAreIgnored()
    {
        QueryParamsProvider queryParamsProvider = createQueryParamsProviderImpl(
                new UserFullNameQueryParamProvider(),
                new ThrowingExceptionQueryParamProvider(),
                new AddOnKeyQueryParamProvider()
        );

        Multimap<String, String> params = queryParamsProvider.provideQueryParams(Optional.of(userProfile),  registrationDetails);

        assertThat(params, is((Multimap<String, String>) ImmutableMultimap.of(
                "user-name", userFullName,
                "add-key", addOnKey1
        )));
    }

    private CompositeQueryParamsProvider createQueryParamsProviderImpl(QueryParamsProvider constructorQueryParamsProvider, QueryParamsProvider... containerProviders)
    {
        WebHookPluginRegistrationContainer container = mock(WebHookPluginRegistrationContainer.class);
        when(container.getQueryParamsProviders()).thenReturn(ImmutableSet.copyOf(containerProviders));
        return new CompositeQueryParamsProvider(container, constructorQueryParamsProvider);
    }

    private static class UserFullNameQueryParamProvider implements QueryParamsProvider
    {

        @Override
        public Multimap<String, String> provideQueryParams(Optional<UserProfile> userProfile, WebHookListenerRegistrationDetails registrationDetails)
        {
            return ImmutableMultimap.of("user-name", userProfile.get().getFullName());
        }
    }

    private static class AddOnKeyQueryParamProvider implements QueryParamsProvider
    {

        @Override
        public Multimap<String, String> provideQueryParams(Optional<UserProfile> userProfile, WebHookListenerRegistrationDetails registrationDetails)
        {
            return ImmutableMultimap.of("add-key", registrationDetails.getModuleDescriptorDetails().get().getPluginKey());
        }
    }

    private static class AddOnKeyAndUserNameQueryParamProvider implements QueryParamsProvider
    {

        @Override
        public Multimap<String, String> provideQueryParams(Optional<UserProfile> userProfile, WebHookListenerRegistrationDetails registrationDetails)
        {
            return ImmutableMultimap.of(
                    "user-name", userProfile.get().getUsername(),
                    "add-key", addOnKey2
            );
        }
    }

    private static class ThrowingExceptionQueryParamProvider implements QueryParamsProvider
    {

        @Override
        public Multimap<String, String> provideQueryParams(final Optional<UserProfile> userProfile, final WebHookListenerRegistrationDetails registrationDetails)
        {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}