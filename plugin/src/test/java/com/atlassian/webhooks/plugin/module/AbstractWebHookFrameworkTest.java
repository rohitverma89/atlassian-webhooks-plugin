package com.atlassian.webhooks.plugin.module;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.webhooks.api.register.*;
import com.atlassian.webhooks.api.util.EventMatchers;
import com.atlassian.webhooks.spi.WebHookPluginRegistrationFactory;
import com.google.common.base.Function;
import com.google.common.collect.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AbstractWebHookFrameworkTest
{
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private PluginEventManager pluginEventManager;

    @InjectMocks
    protected WebHookPluginRegistrationContainerImpl container;

    protected void addModule(WebHookPluginRegistration registration)
    {
        container.tracker.onPluginModuleEnabled(new PluginModuleEnabledEvent(descriptorFor(registration)));
    }

    protected void removeModule(WebHookPluginRegistration registration)
    {
        container.tracker.onPluginModuleDisabled(new PluginModuleDisabledEvent(descriptorFor(registration), false));
    }

    private final Map<WebHookPluginRegistration, WebHookRegistrationFactoryModuleDescriptor> descriptors = Maps.newHashMap();

    protected final List<RegisteredWebHookEvent> getRegisteredEvents()
    {
        return ImmutableList.copyOf(concat(transform(concat(transform(container.getWebHookSections(), new Function<WebHookEventSection, Iterable<WebHookEventGroup>>()
        {
            @Override
            public Iterable<WebHookEventGroup> apply(final WebHookEventSection input)
            {
                return input.getGroups();
            }
        })), new Function<WebHookEventGroup, Iterable<RegisteredWebHookEvent>>()
        {
            @Override
            public Iterable<RegisteredWebHookEvent> apply(final WebHookEventGroup input)
            {
                return input.getEvents();
            }
        })));
    }

    private WebHookRegistrationFactoryModuleDescriptor descriptorFor(final WebHookPluginRegistration registration)
    {
        if (!descriptors.containsKey(registration))
        {
            WebHookRegistrationFactoryModuleDescriptor descriptor = mock(WebHookRegistrationFactoryModuleDescriptor.class);
            when(descriptor.getModule()).thenReturn(new WebHookPluginRegistrationFactory()
            {
                @Override
                public WebHookPluginRegistration createPluginRegistration()
                {
                    return registration;
                }
            });
            when(descriptor.getCompleteKey()).thenReturn(String.valueOf(registration.hashCode()));
            descriptors.put(registration, descriptor);
        }
        return descriptors.get(registration);
    }

    protected WebHookPluginRegistrationBuilder registration(RegisteredWebHookEvent... webhooks)
    {
        WebHookPluginRegistrationBuilder builer = WebHookPluginRegistration.builder();
        for (RegisteredWebHookEvent webhook : webhooks)
        {
            builer.addWebHook(webhook);
        }
        return builer;
    }

    protected RegisteredWebHookEvent webHook(String id)
    {
        return webHook(id, Object.class);
    }

    protected <T> RegisteredWebHookEvent<T> webHook(String id, Class<T> firedWhen)
    {
        return RegisteredWebHookEvent.withId(id).andDisplayName("").firedWhen(firedWhen).isMatchedBy(EventMatchers.ALWAYS_TRUE);
    }
}
