package com.atlassian.webhooks.plugin.uri;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class UrlVariableSubstitutorTest
{
    private final UrlVariableSubstitutor substitutor = new UrlVariableSubstitutor();

    @Test
    public void substituteVariableWithDollarSign()
    {
        final ImmutableMap<String, Boolean> ctx = ImmutableMap.of("variable", true);
        final String replaced = substitutor.replace("${variable}", ctx);
        assertThat(replaced, equalTo("true"));
    }

    @Test
    public void substituteVariableWithoutDollarSign()
    {
        final ImmutableMap<String, Boolean> ctx = ImmutableMap.of("variable", true);
        final String replaced = substitutor.replace("{variable}", ctx);
        assertThat(replaced, equalTo("true"));
    }

    @Test
    public void noMatchedVariableInCtx()
    {
        final ImmutableMap<String, Boolean> ctx = ImmutableMap.of("variable", true);
        final String replaced = substitutor.replace("{variable2}+${variable3}", ctx);
        assertThat(replaced, equalTo("+"));
    }

    @Test
    public void substituteVariableFromNestedMap()
    {
        ImmutableMap<String, ImmutableMap<String, Integer>> ctx = ImmutableMap.of("nested", ImmutableMap.of("var", 2));
        final String replaced = substitutor.replace("{nested.var}", ctx);
        assertThat(replaced, equalTo("2"));
    }

    @Test
    public void substituteMultipleVariablesFromNestedMap()
    {
        final ImmutableMap<String, ImmutableMap<String, Integer>> ctx = ImmutableMap.of("nested", ImmutableMap.of("var", 2, "var2", 3));
        final String replaced = substitutor.replace("{nested.var}+${nested.var2}", ctx);
        assertThat(replaced, equalTo("2+3"));
    }

    @Test
    public void firstElementOfArrayPicked()
    {
        final ImmutableMap<String, Object> ctx = ImmutableMap.<String, Object>of("nested", new Boolean[] {true, false});
        final String replaced = substitutor.replace("{nested}", ctx);
        assertThat(replaced, equalTo("true"));
    }
}