package com.atlassian.webhooks.plugin.uri;

import com.atlassian.fugue.Option;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import com.atlassian.webhooks.spi.UriResolver;
import com.atlassian.webhooks.spi.plugin.PluginUriResolver;
import com.google.common.base.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import java.net.URI;
import java.net.URISyntaxException;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails.moduleDescriptor;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class PluggableUriResolverImplTest
{
    public static final String JIRA_BASE_URL = "http://jira:8080";
    private PluggableUriResolverImpl pluginUriResolver;

    @Mock
    private BundleContext bundleContext;
    @Mock
    private ApplicationProperties applicationProperties;

    @Before
    public void setUp()
    {
        this.pluginUriResolver = new PluggableUriResolverImpl(bundleContext, applicationProperties);
        when(applicationProperties.getBaseUrl(any(UrlMode.class))).thenReturn(JIRA_BASE_URL);
    }


    @Test
    public void resolvesUriWithResolverFromBundleContext() throws InvalidSyntaxException
    {
        UriResolver testUriResolverImpl = new UriResolver()
        {
            @Override
            public Option<URI> getUri(final WebHookListenerRegistrationDetails listenerRegistrationDetails, final URI path)
            {
                try
                {
                    return some(new URI(path.toString() + listenerRegistrationDetails.getModuleDescriptorDetails().get().getPluginKey()));
                }
                catch (URISyntaxException e)
                {
                    return none();
                }
            }
        };
        final ServiceReference serviceReference = mock(ServiceReference.class);
        when(bundleContext.getAllServiceReferences(UriResolver.class.getName(), null))
                .thenReturn(new ServiceReference[] { serviceReference });
        when(bundleContext.getService(serviceReference)).thenReturn(testUriResolverImpl);

        final URI resolvedUri = pluginUriResolver.resolve(moduleDescriptor("pluginKey"), URI.create("http://example.com/"));

        assertThat(resolvedUri.toString(), equalTo("http://example.com/pluginKey"));
        verify(bundleContext).ungetService(serviceReference);
    }


    @Test
    public void resolvesUriWithLegacyResolverFromBundleContext() throws InvalidSyntaxException
    {
        PluginUriResolver testUriResolverImpl = new PluginUriResolver()
        {
            @Override
            public Optional<URI> getUri(final String pluginKey, final URI path)
            {
                try
                {
                    return Optional.of(new URI(path.toString() + pluginKey));
                }
                catch (URISyntaxException e)
                {
                    return Optional.absent();
                }
            }
        };
        final ServiceReference serviceReference = mock(ServiceReference.class);
        when(bundleContext.getAllServiceReferences(PluginUriResolver.class.getName(), null))
                .thenReturn(new ServiceReference[] { serviceReference });
        when(bundleContext.getService(serviceReference)).thenReturn(testUriResolverImpl);

        final URI resolvedUri = pluginUriResolver.resolve(moduleDescriptor("pluginKey"), URI.create("http://example.com/"));

        assertThat(resolvedUri.toString(), equalTo("http://example.com/pluginKey"));
        verify(bundleContext).ungetService(serviceReference);
    }

    @Test
    public void relativeUrlsFromPersistentStoreAreLeftAsTheyAre() throws InvalidSyntaxException
    {
        URI relativeUrl = URI.create("/relative");
        URI resolved = pluginUriResolver.resolve(WebHookListenerRegistrationDetails.persistentStore(42, "name", Optional.of("1")), relativeUrl);
        assertThat(resolved, equalTo(relativeUrl));
    }

    @Test
    public void relativeUrlsFromModuleDescriptorArePrefixedWithBaseUrl() throws InvalidSyntaxException
    {
        URI relativeUrl = URI.create("/relative");
        URI resolved = pluginUriResolver.resolve(moduleDescriptor("pluginKey"), relativeUrl);
        assertThat(resolved, equalTo(URI.create(JIRA_BASE_URL + relativeUrl.toString())));
    }

    @Test
    public void resolvesUriWithoutResolversInBundleContext() throws InvalidSyntaxException
    {
        when(bundleContext.getAllServiceReferences(PluginUriResolver.class.getName(), null))
                .thenReturn(new ServiceReference[] { });

        final URI resolvedUri = pluginUriResolver.resolve(moduleDescriptor("pluginKey"), URI.create("http://example.com/"));

        assertThat(resolvedUri.toString(), equalTo("http://example.com/"));
    }
}