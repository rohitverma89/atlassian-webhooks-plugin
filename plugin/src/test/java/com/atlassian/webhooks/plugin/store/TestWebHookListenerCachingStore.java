package com.atlassian.webhooks.plugin.store;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.Plugin;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.vcache.internal.core.ThreadLocalRequestContextSupplier;
import com.atlassian.webhooks.api.events.WebHookClearListenerCacheEvent;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;
import com.atlassian.webhooks.plugin.PluginProperties;
import com.atlassian.webhooks.plugin.TestVCacheFactory;
import com.atlassian.webhooks.spi.WebHookListenerStore;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static com.atlassian.webhooks.plugin.service.SecuredWebHookListenerServiceTest.EVENTS;
import static com.atlassian.webhooks.plugin.service.SecuredWebHookListenerServiceTest.EXCLUDE_BODY;
import static com.atlassian.webhooks.plugin.service.SecuredWebHookListenerServiceTest.FILTERS;
import static com.atlassian.webhooks.plugin.service.SecuredWebHookListenerServiceTest.TARGET_URL;
import static com.atlassian.webhooks.plugin.service.SecuredWebHookListenerServiceTest.USER_KEY;
import static com.atlassian.webhooks.plugin.service.SecuredWebHookListenerServiceTest.WEB_HOOK_NAME;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestWebHookListenerCachingStore
{
    public static final PersistentWebHookListener TEST_LISTENER = PersistentWebHookListener
            .newlyCreated()
            .setListenerName(WEB_HOOK_NAME)
            .addWebHookIds(EVENTS)
            .setExcludeBody(EXCLUDE_BODY)
            .addFilters(FILTERS)
            .setUrl(TARGET_URL)
            .build();

    private final ThreadLocalRequestContextSupplier requestContextSupplier = ThreadLocalRequestContextSupplier.strictSupplier();

    private WebHookListenerCachingStore webHookListenerCachingStore;
    private WebHookListenerStore webHookListenerStore;

    @Mock
    private UserManager userManager;

    @Before
    public void setup()
    {
        when(userManager.getRemoteUserKey()).thenReturn(new UserKey(USER_KEY));
        webHookListenerStore = Mockito.spy(new MockWebHookListenerStore(userManager));
        webHookListenerCachingStore = new WebHookListenerCachingStore(webHookListenerStore, TestVCacheFactory.create(requestContextSupplier));
    }

    @Before
    public void initThread()
    {
        requestContextSupplier.initThread("tenant-123");
    }

    @After
    public void clearThread()
    {
        requestContextSupplier.clearThread();
    }

    @Test
    public void testRetrievingWebHooksBeforePluginFullyStarted()
    {
        webHookListenerCachingStore.registerWebHookListener(TEST_LISTENER, RegistrationMethod.SERVICE);
        assertThat(newArrayList(webHookListenerCachingStore.getAllWebHookListeners()), IsCollectionWithSize.hasSize(0));
        verify(webHookListenerStore, times(0)).getAllWebHooks();

        final Plugin plugin = Mockito.mock(Plugin.class);
        when(plugin.getKey()).thenReturn(PluginProperties.PLUGIN_KEY);
        webHookListenerCachingStore.onStart();

        assertThat(newArrayList(webHookListenerCachingStore.getAllWebHookListeners()), IsCollectionWithSize.hasSize(1));
        verify(webHookListenerStore, times(1)).getAllWebHooks();
    }

    @Test
    public void testWebHookListenerSurvivesClearCacheEvent()
    {
        webHookListenerCachingStore.registerWebHookListener(TEST_LISTENER, RegistrationMethod.SERVICE);
        assertThat(newArrayList(webHookListenerCachingStore.getAllWebHookListeners()), IsCollectionWithSize.hasSize(0));

        webHookListenerCachingStore.onStart();
        webHookListenerCachingStore.onClearCacheEvent(new WebHookClearListenerCacheEvent());

        assertThat(newArrayList(webHookListenerCachingStore.getAllWebHookListeners()), IsCollectionWithSize.hasSize(1));
        verify(webHookListenerStore, times(1)).getAllWebHooks();
    }

    @Test
    public void testWebHookListenerUsesCache()
    {
        webHookListenerCachingStore.registerWebHookListener(TEST_LISTENER, RegistrationMethod.SERVICE);
        webHookListenerCachingStore.onStart();

        assertThat(newArrayList(webHookListenerCachingStore.getAllWebHookListeners()), IsCollectionWithSize.hasSize(1));
        verify(webHookListenerStore, times(1)).getAllWebHooks();

        assertThat(newArrayList(webHookListenerCachingStore.getAllWebHookListeners()), IsCollectionWithSize.hasSize(1));
        verify(webHookListenerStore, times(1)).getAllWebHooks();
    }

    @Test
    public void webHookListenerPresentAfterCacheClearAfterGet()
    {
        final PersistentWebHookListener webHookListenerParameters = webHookListenerCachingStore.registerWebHookListener(TEST_LISTENER, RegistrationMethod.SERVICE);

        webHookListenerCachingStore.onStart();

        final Option<PersistentWebHookListener> webHookListenerBefore =
                webHookListenerCachingStore.getWebHookListener(webHookListenerParameters.getId().get());
        assertThat(webHookListenerBefore.isDefined(), is(true));

        webHookListenerCachingStore.onClearCacheEvent(new WebHookClearListenerCacheEvent());

        final Option<PersistentWebHookListener> webHookListenerAfter =
                webHookListenerCachingStore.getWebHookListener(webHookListenerParameters.getId().get());
        assertThat(webHookListenerAfter.isDefined(), is(true));
    }

    @Test
    public void updateRemoveRaceSynthetic() throws Exception
    {
        webHookListenerCachingStore.onStart();
        final PersistentWebHookListener listener = webHookListenerCachingStore.registerWebHookListener(TEST_LISTENER, RegistrationMethod.SERVICE);

        final Integer id = listener.getId().get();

        final Answer<Object> removeQuicklyAfterDbCall= invocationOnMock -> {
            final Object result = invocationOnMock.callRealMethod();
            webHookListenerCachingStore.removeWebHookListener(id);
            return result;
        };

        doAnswer(removeQuicklyAfterDbCall).when(webHookListenerStore).updateWebHook(listener);

        webHookListenerCachingStore.updateWebHookListener(listener);

        final Option<PersistentWebHookListener> fromCache = webHookListenerCachingStore.getWebHookListener(id);
        final Option<PersistentWebHookListener> fromStore = webHookListenerStore.getWebHook(id);
        assertThat(fromCache.isDefined() == fromStore.isDefined(), is(true));
    }

    @Test
    public void updateRemoveRaceThreaded() throws Exception
    {
        webHookListenerCachingStore.onStart();
        final PersistentWebHookListener listener = webHookListenerCachingStore.registerWebHookListener(TEST_LISTENER, RegistrationMethod.SERVICE);

        final Integer id = listener.getId().get();
        final Thread updateThread = new Thread()
        {
            @Override
            public void run()
            {
                initThread();
                webHookListenerCachingStore.updateWebHookListener(listener);
                synchronized (id)
                {
                    id.notify();
                }
                clearThread();
            }
        };

        final Answer<Object> delayAfterDbCall = invocationOnMock -> {
            final Object result = invocationOnMock.callRealMethod();
            synchronized (id)
            {
                id.notify();
                id.wait();
            }
            return result;
        };
        doAnswer(delayAfterDbCall)
                .when(webHookListenerStore).updateWebHook(listener);

        synchronized (id)
        {
            updateThread.start();
            id.wait();
            webHookListenerCachingStore.removeWebHookListener(id);
            id.notify();
            id.wait();
        }

        final Option<PersistentWebHookListener> fromCache = webHookListenerCachingStore.getWebHookListener(id);
        final Option<PersistentWebHookListener> fromStore = webHookListenerStore.getWebHook(id);
        assertThat(fromCache.isDefined() == fromStore.isDefined(), is(true));
    }

    @Test
    public void updateRemoveRaceSleep() throws Exception
    {
        final int grain = 1;
        webHookListenerCachingStore.onStart();
        final PersistentWebHookListener listener = webHookListenerCachingStore.registerWebHookListener(TEST_LISTENER, RegistrationMethod.SERVICE);

        final Integer id = listener.getId().get();
        final Thread updateThread = new Thread()
        {
            @Override
            public void run()
            {
                initThread();
                webHookListenerCachingStore.updateWebHookListener(listener);
                clearThread();
            }
        };

        final Answer<Object> sleepAfterDbCall = invocationOnMock -> {
            final Object result = invocationOnMock.callRealMethod();
            Thread.sleep(2 * grain);
            return result;
        };
        doAnswer(sleepAfterDbCall)
                .when(webHookListenerStore).updateWebHook(listener);

        updateThread.start();
        Thread.sleep(1 * grain);
        webHookListenerCachingStore.removeWebHookListener(id);
        Thread.sleep(2 * grain);

        final Option<PersistentWebHookListener> fromCache = webHookListenerCachingStore.getWebHookListener(id);
        final Option<PersistentWebHookListener> fromStore = webHookListenerStore.getWebHook(id);
        assertThat(fromCache.isDefined() == fromStore.isDefined(), is(true));
    }
}
