package com.atlassian.webhooks.plugin.validator;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.Message;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.util.MessageCollection;
import com.atlassian.webhooks.plugin.module.CloudConditionResolver;
import com.atlassian.webhooks.plugin.uri.UrlVariableSubstitutor;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.Serializable;
import java.util.List;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith (Parameterized.class)
public final class UrlValidatorTest
{
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private CloudConditionResolver cloudConditionResolver;

    private UrlValidator urlValidator;

    private final boolean inTheCloud;

    public UrlValidatorTest(final boolean inTheCloud)
    {
        this.inTheCloud = inTheCloud;
    }


    @Parameterized.Parameters(name = "isCloud: {0}")
    public static List<Boolean[]> cloudCondition() {
        return Lists.newArrayList(new Boolean[] { Boolean.TRUE } , new Boolean[] { Boolean.FALSE });
    }


    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        doAnswer(new Answer<String>()
        {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable
            {
                return (String) invocation.getArguments()[0];
            }
        }).when(i18nResolver).getText(Matchers.anyString());
        doAnswer(new Answer<String>()
        {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable
            {
                return (String) invocation.getArguments()[0];
            }
        }).when(i18nResolver).getText(Matchers.anyString(), Matchers.any(Serializable.class));
        this.urlValidator = new UrlValidator(new UrlVariableSubstitutor(), i18nResolver, cloudConditionResolver);
        when(cloudConditionResolver.isCloud()).thenReturn(inTheCloud);
    }

    @Test
    public void nullUrl()
    {
        final MessageCollection validate = urlValidator.validate(uri(null));
        assertThat(validate, errorCollection("webhooks.empty.url"));
    }

    @Test
    public void malformedUrl()
    {
        final MessageCollection validate = urlValidator.validate(uri("xyz"));
        assertThat(validate, errorCollection("webhooks.invalid.url.protocol"));
    }

    @Test
    public void urlWithInvalidContextParam()
    {
        final MessageCollection validate = urlValidator.validate(uri("http://example.com/${invalid.template"));
        assertThat(validate, errorCollection("webhooks.invalid.url"));
    }

    @Test
    public void urlWithValidContextParam()
    {
        final MessageCollection validate = urlValidator.validate(uri("http://example.com/${valid.template}"));
        assertThat(validate.getMessages(), org.hamcrest.Matchers.<Message>empty());
    }

    @Test
    public void urlWithInvalidScheme()
    {
        final MessageCollection validate = urlValidator.validate(uri("httpX://example.com/${valid.template}"));
        assertThat(validate, errorCollection("webhooks.invalid.url.protocol"));
    }

    @Test
    public void emptyUri()
    {
        final MessageCollection validate = urlValidator.validate(uri(""));
        assertThat(validate, errorCollection("webhooks.empty.url"));
    }

    @Test
    public void invalidPortOnCloud()
    {
        when(cloudConditionResolver.isCloud()).thenReturn(true);
        final MessageCollection validate = urlValidator.validate(uri("http://example.com:81/${valid.template}"));

        assertThat(validate, errorCollection("webhooks.invalid.url.ondemand.ports"));
    }

    @Test
    public void httpsPortForHttpScheme()
    {
        when(cloudConditionResolver.isCloud()).thenReturn(true);
        final MessageCollection validate = urlValidator.validate(uri("http://example.com:443/${valid.template}"));

        assertThat(validate, errorCollection("webhooks.invalid.url.ondemand.ports"));
    }

    @Test
    public void httpPortForHttpsScheme()
    {
        when(cloudConditionResolver.isCloud()).thenReturn(true);
        final MessageCollection validate = urlValidator.validate(uri("https://example.com:80/${valid.template}"));
        assertThat(validate, errorCollection("webhooks.invalid.url.ondemand.ports"));
    }

    private static PersistentWebHookListener uri(final String uri)
    {
        return PersistentWebHookListener.existing(42).setListenerName("name").setUrl(uri).build();
    }

    private static Matcher<MessageCollection> errorCollection(final String expectedMessage)
    {
        return new TypeSafeMatcher<MessageCollection>()
        {
            @Override
            protected boolean matchesSafely(final MessageCollection messageCollection)
            {
                return Iterables.any(messageCollection.getMessages(), new Predicate<Message>()
                {
                    @Override
                    public boolean apply(final Message message)
                    {
                        final Serializable arg = message.getArguments()[0];
                        return arg.toString().equals(expectedMessage);
                    }
                });
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("Error collection does not contain ").appendValue(expectedMessage).appendText("message");
            }
        };
    }
}