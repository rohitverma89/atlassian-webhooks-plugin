package com.atlassian.webhooks.plugin;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.webhooks.api.events.WebHookPublishedEvent;
import com.atlassian.webhooks.api.events.WebHookRejectedEvent;
import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.util.EventMatchers;
import com.atlassian.webhooks.api.util.Vote;
import com.atlassian.webhooks.plugin.legacy.LegacyListener;
import com.atlassian.webhooks.plugin.legacy.LegacyModuleDescriptorWebHookListenerRegistryImpl;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public final class WebHookPublisherImplTest
{
    private WebHookPublisherImpl publisher;
    @Mock
    private OsgiWebHookListenerAccessVoter accessVoter;
    @Mock
    private WebHookListenerProvider listenerProvider;
    @Mock
    private PublishTaskFactory publishTaskFactory;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private LegacyModuleDescriptorWebHookListenerRegistryImpl legacyListeners;
    @Mock
    private Executor executor;

    @Before
    public void setup()
    {
        when(accessVoter.canPublish(any(WebHookEvent.class), any(WebHookListener.class))).thenReturn(Vote.ALLOW);
        when(legacyListeners.getListeners(anyString())).thenReturn(Collections.<LegacyListener>emptySet());
        publisher = new WebHookPublisherImpl(accessVoter, listenerProvider, publishTaskFactory, eventPublisher, legacyListeners, executor, false);
    }

    @Test
    public void testPublishWithNoListeners() throws Exception
    {
        when(listenerProvider.getListeners(Matchers.<WebHookEvent>any())).thenReturn(ImmutableList.<WebHookListener>of());

        publisher.publish(mock(WebHookEvent.class));

        verifyZeroInteractions(publishTaskFactory, eventPublisher, executor);
    }

    @Test
    public void testPublishWithListeners() throws Exception
    {
        final WebHookEvent event = mock(WebHookEvent.class);
        final WebHookListener listener = listener("/");
        final PublishTask publishTask = mock(PublishTask.class);

        when(listenerProvider.getListeners(event)).thenReturn(ImmutableList.of(listener));
        when(publishTaskFactory.getPublishTask(event, listener)).thenReturn(publishTask);
        when(event.getEventMatcher()).thenReturn(EventMatchers.ALWAYS_TRUE);

        publisher.publish(event);

        verify(publishTaskFactory).getPublishTask(event, listener);
        verify(executor).execute(publishTask);
        verify(eventPublisher).publish(isA(WebHookPublishedEvent.class));
    }

    @Test
    public void testPublishWithSomeListenersNotPermitted() throws Exception
    {
        final WebHookEvent event = mock(WebHookEvent.class);

        final WebHookListener listener1 = listener("/1");
        final WebHookListener listener2 = listener("/2");
        final WebHookListener listener3 = listener("/3");
        final PublishTask publishTask1 = mock(PublishTask.class);
        final PublishTask publishTask2 = mock(PublishTask.class);
        final PublishTask publishTask3 = mock(PublishTask.class);

        // mock that listener2 is DENIED
        when(accessVoter.canPublish(event, listener2)).thenReturn(Vote.DENY);
        when(accessVoter.canPublish(event, listener1)).thenReturn(Vote.ALLOW);
        when(accessVoter.canPublish(event, listener3)).thenReturn(Vote.ALLOW);

        when(listenerProvider.getListeners(event)).thenReturn(ImmutableList.of(listener1, listener2, listener3));
        when(publishTaskFactory.getPublishTask(event, listener1)).thenReturn(publishTask1);
        when(publishTaskFactory.getPublishTask(event, listener2)).thenReturn(publishTask2);
        when(publishTaskFactory.getPublishTask(event, listener3)).thenReturn(publishTask3);
        when(event.getEventMatcher()).thenReturn(EventMatchers.ALWAYS_TRUE);

        publisher.publish(event);

        verify(publishTaskFactory).getPublishTask(event, listener1);
        verify(publishTaskFactory, never()).getPublishTask(event, listener2);
        verify(publishTaskFactory).getPublishTask(event, listener3);

        verify(eventPublisher, times(2)).publish(isA(WebHookPublishedEvent.class));
    }

    @Test
    public void testPublishWithFullQueue() throws Exception
    {
        final WebHookEvent event = mock(WebHookEvent.class);
        when(event.getId()).thenReturn("webhook_id");
        final WebHookListener listener = WebHookListener.fromPersistentStore(42, "name", Optional.of("1")).to("/").build();
        final PublishTask publishTask = mock(PublishTask.class);

        when(listenerProvider.getListeners(event)).thenReturn(ImmutableList.of(listener));
        when(publishTaskFactory.getPublishTask(event, listener)).thenReturn(publishTask);
        doThrow(RejectedExecutionException.class).when(executor).execute(publishTask);
        when(event.getEventMatcher()).thenReturn(EventMatchers.ALWAYS_TRUE);

        publisher.publish(event);

        verify(publishTaskFactory).getPublishTask(event, listener);
        verify(executor).execute(publishTask);
        verify(eventPublisher).publish(isA(WebHookRejectedEvent.class));
    }

    @Test
    public void testPublishWithEventNotMatching() throws Exception
    {
        final WebHookEvent event = mock(WebHookEvent.class);
        when(event.getId()).thenReturn("webhook_id");
        final WebHookListener listener = WebHookListener.fromPersistentStore(42, "name", Optional.of("1")).to("/").build();

        when(listenerProvider.getListeners(event)).thenReturn(ImmutableList.of(listener));
        when(event.getEventMatcher()).thenReturn(EventMatchers.ALWAYS_FALSE);

        publisher.publish(event);

        verifyZeroInteractions(publishTaskFactory, executor, eventPublisher);
    }

    private WebHookListener listener(String path) throws URISyntaxException
    {
        return WebHookListener.fromPersistentStore(42, "name", Optional.of("1")).to(path).build();
    }
}
