package com.atlassian.webhooks.plugin.module;

import com.atlassian.fugue.Pair;
import com.atlassian.webhooks.api.register.RegisteredWebHookEvent;
import com.atlassian.webhooks.api.register.WebHookPluginRegistration;
import com.atlassian.webhooks.api.register.WebHookPluginRegistrationBuilder;
import com.atlassian.webhooks.api.util.SectionKey;
import com.atlassian.webhooks.api.util.TypeRichString;
import com.atlassian.webhooks.spi.EventSerializer;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.atlassian.fugue.Pair.pair;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

@RunWith (MockitoJUnitRunner.class)
public class TestWebHookPluginRegistrationContainerImpl extends AbstractWebHookFrameworkTest
{
    @Test
    public void whenModuleIsAddedAndThenRegisteredWebHooksCanBeRetrieved()
    {
        RegisteredWebHookEvent webhook1 = webHook("test");
        WebHookPluginRegistration registration = registration(webhook1).build();
        addModule(registration);
        assertRegisteredWebHooks(webhook1);
        assertThat(container.getWebHookSections(), hasSize(1));
    }

    @Test
    public void whenModuleIsAddedAndThenRemovedThenRegisteredWebHooksAreCleared()
    {
        WebHookPluginRegistration registration = registration(webHook("test")).build();
        addModule(registration);
        removeModule(registration);
        assertRegisteredWebHooks();
        assertThat(container.getWebHookSections(), hasSize(0));
    }

    @Test
    public void multipleModulesCanRegisterSystemWebHooks()
    {
        RegisteredWebHookEvent webHook1 = webHook("test1");
        WebHookPluginRegistration module1 = WebHookPluginRegistration.builder().addWebHook(webHook1).build();
        RegisteredWebHookEvent webHook2 = webHook("test2");
        WebHookPluginRegistration module2 = WebHookPluginRegistration.builder().addWebHook(webHook2).build();
        addModule(module1);
        addModule(module2);
        assertThat(getRegisteredEvents(), hasSize(2));
        assertThat(getRegisteredEvents(), hasItems(webHook1, webHook2));
        removeModule(module1);
        assertThat(getRegisteredEvents(), hasSize(1));
        assertThat(getRegisteredEvents(), hasItem(webHook2));
        removeModule(module2);
        assertThat(getRegisteredEvents(), hasSize(0));
    }

    @Test
    public void sectionIsReturnedCorrectlyForWebHookEvent()
    {
        WebHookPluginRegistration registration1 = registration(webHook("1"), webHook("2")).build();
        WebHookPluginRegistration registration2 = registration(webHook("3"), webHook("4")).build();
        addModule(registration1);
        addModule(registration2);
        assertThat(container.getWebHookRegistry().sectionOf("1"), equalTo(new SectionKey(registration1.getSections().iterator().next().getKey())));
        assertThat(container.getWebHookRegistry().sectionOf("4"), equalTo(new SectionKey(registration2.getSections().iterator().next().getKey())));
    }

    @Test
    public void whenMultipleModulesAreAddedAndRemovedContainerTracksRegisteredWebHooksCorrectly()
    {
        RegisteredWebHookEvent[] firstModuleWebHooks = { webHook("1a"), webHook("2a"), webHook("3a"), webHook("a") };
        RegisteredWebHookEvent[] secondModuleWebHooks = { webHook("1b"), webHook("2b"), webHook("3b") };
        RegisteredWebHookEvent[] thirdModuleWebHooks = { webHook("1c"), webHook("2c") };

        WebHookPluginRegistration plugin1 = registration(firstModuleWebHooks).build();
        WebHookPluginRegistration plugin2 = registration(secondModuleWebHooks).build();
        WebHookPluginRegistration plugin3 = registration(thirdModuleWebHooks).build();

        addModule(plugin1);
        assertRegisteredWebHooks(firstModuleWebHooks);

        addModule(plugin2);
        assertRegisteredWebHooks(addAll(firstModuleWebHooks, secondModuleWebHooks));

        removeModule(plugin1);
        assertRegisteredWebHooks(secondModuleWebHooks);

        addModule(plugin3);
        assertRegisteredWebHooks(addAll(secondModuleWebHooks, thirdModuleWebHooks));

        removeModule(plugin2);
        removeModule(plugin3);
        assertRegisteredWebHooks();

        addModule(plugin1);
        addModule(plugin2);
        addModule(plugin3);
        assertRegisteredWebHooks(addAll(addAll(firstModuleWebHooks, secondModuleWebHooks), thirdModuleWebHooks));
    }

    @Test
    public void registeredSerializersAreReturned()
    {
        testRegisteredProcessors(EventSerializer.class, new Function<RegistrationTriple<EventSerializer>, WebHookPluginRegistrationBuilder>()
        {
            @Override
            public WebHookPluginRegistrationBuilder apply(RegistrationTriple<EventSerializer> input)
            {
                return input.builder.eventSerializer(input.type, input.processor);
            }
        }, new Function<WebHookPluginRegistrationContainerImpl, ClassSpecificProcessors<EventSerializer>>()
        {
            @Override
            public ClassSpecificProcessors<EventSerializer> apply(WebHookPluginRegistrationContainerImpl input)
            {
                return input.getEventSerializers();
            }
        });
    }

    @Test
    public void registeredUriVariablesResolversAreReturned()
    {
        testRegisteredProcessors(UriVariablesProvider.class, new Function<RegistrationTriple<UriVariablesProvider>, WebHookPluginRegistrationBuilder>()
        {
            @Override
            public WebHookPluginRegistrationBuilder apply(RegistrationTriple<UriVariablesProvider> input)
            {
                return input.builder.variablesProvider(input.type, input.processor);
            }
        }, new Function<WebHookPluginRegistrationContainerImpl, ClassSpecificProcessors<UriVariablesProvider>>()
        {
            @Override
            public ClassSpecificProcessors<UriVariablesProvider> apply(WebHookPluginRegistrationContainerImpl input)
            {
                return input.getUriVariablesProviders();
            }
        });
    }

    private <T> void testRegisteredProcessors(Class<T> processorType, Function<RegistrationTriple<T>, WebHookPluginRegistrationBuilder> registerProcessor, Function<WebHookPluginRegistrationContainerImpl, ClassSpecificProcessors<T>> getProcessors)
    {
        Pair<Class<A>, T> processor1 = pair(A.class, mock(processorType));
        Pair<Class<B>, T> processor2 = pair(B.class, mock(processorType));
        Pair<Class<C>, T> processor3 = pair(C.class, mock(processorType));

        WebHookPluginRegistration registration = registration(registerProcessor, processor1, processor2);
        WebHookPluginRegistration registration2 = registration(registerProcessor, processor3);

        addModule(registration);
        assertThat(getProcessors.apply(container).forType(A.class).get(), (Matcher) equalTo(processor1.right()));
        assertThat(getProcessors.apply(container).forType(B.class).get(), (Matcher) equalTo(processor2.right()));
        assertThat(Lists.newArrayList(getProcessors.apply(container).allForType(All.class)), hasSize(2));

        addModule(registration2);
        assertThat(Lists.newArrayList(getProcessors.apply(container).allForType(All.class)), hasSize(3));

        removeModule(registration);
        assertThat(Lists.newArrayList(getProcessors.apply(container).allForType(All.class)), hasSize(1));
        assertThat(getProcessors.apply(container).forType(C.class).get(), (Matcher) equalTo(processor3.right()));

        removeModule(registration2);
        assertThat(Lists.newArrayList(getProcessors.apply(container).allForType(All.class)), hasSize(0));
    }

    private void assertRegisteredWebHooks(RegisteredWebHookEvent... webhooks)
    {
        String[] webHookIds = Iterables.toArray(transform(Arrays.asList(webhooks), new Function<RegisteredWebHookEvent, String>()
        {
            @Override
            public String apply(RegisteredWebHookEvent input)
            {
                return input.getId();
            }
        }), String.class);
        assertThat(webHookIdsInRegistry(), containsInAnyOrder(webHookIds));
        assertThat(registeredWebHooks(), containsInAnyOrder(webhooks));
    }

    private List<RegisteredWebHookEvent> registeredWebHooks()
    {
        return newArrayList(concat(transform(container.getWebHookRegistrations(), new Function<WebHookPluginRegistration, Iterable<RegisteredWebHookEvent>>()
        {
            @Override
            public Iterable<RegisteredWebHookEvent> apply(WebHookPluginRegistration input)
            {
                return input.getRegistrations();
            }
        })));
    }

    private Iterable<String> webHookIdsInRegistry()
    {
        return Iterables.transform(container.getWebHookRegistry().getWebHookIds(), TypeRichString.GET_VALUE);
    }

    private <T> WebHookPluginRegistration registration(Function<RegistrationTriple<T>, WebHookPluginRegistrationBuilder> register, Pair... processors)
    {
        WebHookPluginRegistrationBuilder builder = WebHookPluginRegistration.builder();
        for (Pair<Class, T> processor : processors)
        {
            builder = register.apply(new RegistrationTriple<T>(builder, processor.left(), processor.right()));
        }
        return builder.build();
    }

    private static class RegistrationTriple<T>
    {
        private final WebHookPluginRegistrationBuilder builder;
        private final Class type;
        private final T processor;

        private RegistrationTriple(WebHookPluginRegistrationBuilder builder, Class type, T processor)
        {
            this.builder = builder;
            this.type = type;
            this.processor = processor;
        }
    }

    private static interface A {}

    private static interface B {}

    private static interface C {}

    private static interface All extends A, B, C {}
}
