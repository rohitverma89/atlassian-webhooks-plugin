package com.atlassian.webhooks.plugin.uri;

import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import com.atlassian.webhooks.plugin.module.ClassSpecificProcessors;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.osgi.framework.InvalidSyntaxException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UriVariablesResolverImplTest
{
    private static final TestIssueEvent TEST_EVENT = new TestIssueEvent("DEMO-1", 1l);
    @Mock
    private PluggableUriResolver pluggableUriResolver;
    @Mock
    private ClassSpecificProcessors<UriVariablesProvider> variableProviders;
    @Mock
    private WebHookPluginRegistrationContainer registrationContainer;

    private UriVariablesResolverImpl uriVariablesResolver;

    @Before
    public void setUp() throws InvalidSyntaxException
    {
        when(registrationContainer.getUriVariablesProviders()).thenReturn(variableProviders);
        this.uriVariablesResolver = new UriVariablesResolverImpl(new UrlVariableSubstitutor(), pluggableUriResolver, registrationContainer);
        doAnswer(new Answer()
        {
            @Override
            public Object answer(final InvocationOnMock invocationOnMock) throws Throwable
            {
                return invocationOnMock.getArguments()[1];
            }
        }).when(pluggableUriResolver).resolve(any(WebHookListenerRegistrationDetails.class), any(URI.class));
    }

    @Test
    public void replacesContextParametersWithPluggedInUriSubstitutions()
    {
        final WebHookEvent webHookEvent = mock(WebHookEvent.class);
        when(webHookEvent.getEvent()).thenReturn(TEST_EVENT);
        when(variableProviders.allForType(TestIssueEvent.class))
                .thenReturn(Lists.<UriVariablesProvider>newArrayList(getWebHookUriSubstitution()));
        final WebHookListener webHookListener = WebHookListener.fromPersistentStore(42, "name", Optional.of("1")).to("http://atlassian.com/jira/${issue.key}").build();

        final URI resolvedUri = uriVariablesResolver.resolve(webHookListener, webHookEvent);

        assertThat(resolvedUri.toString(), equalTo("http://atlassian.com/jira/DEMO-1"));
    }

    @Test
    public void replacesMultipleContextParameters()
    {
        final WebHookEvent webHookEvent = mock(WebHookEvent.class);
        when(webHookEvent.getEvent()).thenReturn(TEST_EVENT);
        when(variableProviders.allForType(TestIssueEvent.class))
                .thenReturn(Lists.<UriVariablesProvider>newArrayList(getWebHookUriSubstitution()));
        final WebHookListener webHookListener = WebHookListener.fromPersistentStore(42, "name", Optional.of("1")).to("http://atlassian.com/jira/${issue.key}/{issue.id}").build();

        final URI resolvedUri = uriVariablesResolver.resolve(webHookListener, webHookEvent);

        assertThat(resolvedUri.toString(), equalTo("http://atlassian.com/jira/DEMO-1/1"));
    }

    @Test
    public void replacesContextParametersAndResolvesUriWithPluggedResolver() throws InvalidSyntaxException, URISyntaxException
    {
        final WebHookEvent webHookEvent = mock(WebHookEvent.class);
        when(webHookEvent.getEvent()).thenReturn(TEST_EVENT);
        when(variableProviders.allForType(TestIssueEvent.class))
                .thenReturn(Lists.<UriVariablesProvider>newArrayList(getWebHookUriSubstitution()));
        doAnswer(new Answer<URI>()
        {
            @Override
            public URI answer(final InvocationOnMock invocationOnMock) throws Throwable
            {
                final String pluginKey = ((WebHookListenerRegistrationDetails) invocationOnMock.getArguments()[0]).getModuleDescriptorDetails().get().getPluginKey();
                final URI uri = (URI) invocationOnMock.getArguments()[1];
                return new URI(uri.toString() + "/" + pluginKey);
            }
        }).when(pluggableUriResolver).resolve(any(WebHookListenerRegistrationDetails.class), any(URI.class));

        final WebHookListener webHookListener = WebHookListener.fromModuleDescriptor("pluginKey").to("http://atlassian.com/jira/${issue.key}/{issue.id}").build();

        final URI uri = uriVariablesResolver.resolve(webHookListener, webHookEvent);

        assertThat(uri.toString(), equalTo("http://atlassian.com/jira/DEMO-1/1/pluginKey"));
    }

    private UriVariablesProvider<TestIssueEvent> getWebHookUriSubstitution()
    {
        return new UriVariablesProvider<TestIssueEvent>()
        {
            @Override
            public Map<String, Object> uriVariables(final TestIssueEvent event)
            {
                return ImmutableMap.<String, Object>of("issue",
                        ImmutableMap.of("key", event.getKey(), "id", event.getId()));
            }
        };
    }

    private static class TestIssueEvent
    {
        private final String key;
        private final Long id;

        private TestIssueEvent(final String key, final Long id)
        {
            this.key = key;
            this.id = id;
        }

        public String getKey()
        {
            return key;
        }

        public Long getId()
        {
            return id;
        }
    }
}