package com.atlassian.webhooks.plugin.web;

import com.atlassian.webhooks.api.document.ProvidesUrlVariables;
import com.atlassian.webhooks.api.register.WebHookEventGroup;
import com.atlassian.webhooks.api.register.WebHookEventSection;
import com.atlassian.webhooks.api.register.WebHookPluginRegistration;
import com.atlassian.webhooks.plugin.module.AbstractWebHookFrameworkTest;
import com.atlassian.webhooks.spi.DocumentedUriVariablesProvider;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public final class UriVariablesProviderPageContextResolverTest extends AbstractWebHookFrameworkTest
{
    private UriVariablesProviderPageContextResolver variablesResolver;

    private final DocumentedUriVariablesProvider<Integer> intProvider = providerOf("int", "integer");
    private final DocumentedUriVariablesProvider<Double> doubleProvider = providerOf("double");
    private final DocumentedUriVariablesProvider<Number> numberProvider = providerOf("int", "integer", "double");
    private final DocumentedUriVariablesProvider<Object> generalProvider = providerOf("object");
    private final DocumentedUriVariablesProvider<Object> alphabeticTestProvider = providerOf("z", "a", "g", "c");

    @ProvidesUrlVariables ({ "int", "integer" })
    private static class AnnotatedClass implements UriVariablesProvider<Integer>
    {

        @Override
        public Map<String, Object> uriVariables(final Integer event)
        {
            throw new UnsupportedOperationException("Not implemented");
        }
    }


    private static class AnnotatedMethod implements UriVariablesProvider<Double>
    {
        @ProvidesUrlVariables ("double")
        @Override
        public Map<String, Object> uriVariables(final Double event)
        {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    @Before
    public void setUp() throws Exception
    {
        variablesResolver = new UriVariablesProviderPageContextResolver(container);
    }

    @Test
    public void whenJustOneVariableProviderIsDefinedForWebHookEventThenReturnItsVariables()
    {
        addModule(WebHookPluginRegistration.builder()
                .addWebHook(webHook("int", Integer.class))
                .addWebHook(webHook("double", Double.class))
                .variablesProvider(Integer.class, intProvider)
                .variablesProvider(Double.class, doubleProvider)
                .build());
        assertThat(variablesResolver.uriVariablesForWebhooks().get("int"), contains("int", "integer"));
        assertThat(variablesResolver.uriVariablesForWebhooks().get("double"), contains("double"));
    }

    @Test
    public void whenMoreThanOneMatchingProviderIsDefinedForAnEventThenReturnSum()
    {
        addModule(WebHookPluginRegistration.builder()
                .addWebHook(webHook("int", Integer.class))
                .variablesProvider(Integer.class, intProvider)
                .variablesProvider(Object.class, generalProvider)
                .build());
        assertThat(variablesResolver.uriVariablesForWebhooks().get("int"), contains("int", "integer", "object"));
    }

    @Test
    public void whenTwoProvidersForAnEventProvideTheSameVariableReturnItOnlyOnce()
    {
        addModule(WebHookPluginRegistration.builder()
                .addWebHook(webHook("int", Integer.class))
                .variablesProvider(Integer.class, intProvider)
                .variablesProvider(Number.class, numberProvider)
                .build());
        assertThat(variablesResolver.uriVariablesForWebhooks().get("int"), contains("double", "int", "integer"));
    }

    @Test
    public void sortUriVariablesForWebhooks()
    {
        addModule(WebHookPluginRegistration.builder()
                .addWebHook(webHook("int", Integer.class))
                .variablesProvider(Integer.class, intProvider)
                .variablesProvider(Object.class, alphabeticTestProvider)
                .build());
        assertThat(variablesResolver.uriVariablesForWebhooks().get("int"), contains("a", "c", "g", "int", "integer", "z"));
    }

    @Test
    public void annotationsAreProcessedCorrectly()
    {
        addModule(WebHookPluginRegistration.builder()
                .addWebHook(webHook("int", Integer.class))
                .addWebHook(webHook("double", Double.class))
                .variablesProvider(Integer.class, new AnnotatedClass())
                .variablesProvider(Double.class, new AnnotatedMethod())
                .build());
        assertThat(variablesResolver.uriVariablesForWebhooks().get("int"), contains("int", "integer"));
        assertThat(variablesResolver.uriVariablesForWebhooks().get("double"), contains("double"));
    }

    @Test
    public void ifNoUriVariableProviderIsPresentThenEmptyListIsReturnedForWebhook()
    {
        addModule(WebHookPluginRegistration.builder()
                .addWebHook(webHook("int", Integer.class))
                .build());
        assertThat(variablesResolver.uriVariablesForWebhooks().get("int"), hasSize(0));
    }

    @Test
    public void ifUndocumentedUriVariableProviderIsPresentThenEmptyListIsReturnedForWebhook()
    {
        addModule(WebHookPluginRegistration.builder()
                .addWebHook(webHook("int", Integer.class))
                .variablesProvider(Integer.class, new UriVariablesProvider<Integer>()
                {
                    @Override
                    public Map<String, Object> uriVariables(final Integer event)
                    {
                        return ImmutableMap.<String, Object>of("test", "test");
                    }
                })
                .build());
        assertThat(variablesResolver.uriVariablesForWebhooks().get("int"), hasSize(0));
    }

    @Test
    public void onlyVariablesForVisibleWebhooksAreReturnedWhenAskingForAllVariables()
    {
        addModule(WebHookPluginRegistration.builder()
                .addWebHookSection(
                        WebHookEventSection.section("visible-section")
                        .nameI18nKey("section name makes it visible")
                        .addGroup(
                                WebHookEventGroup.builder()
                                .nameI18nKey("group needs a name too")
                                .addEvent(webHook("visible", Integer.class))
                                .build()

                        )
                        .build())
                .addWebHook(webHook("not-visible-webhook", Double.class))
                .variablesProvider(Integer.class, intProvider)
                .variablesProvider(Double.class, doubleProvider)
                .build());

        assertThat(variablesResolver.allUriVariables(), hasSize(2));
        assertThat(variablesResolver.allUriVariables(), contains("int", "integer"));
    }

    private <T> DocumentedUriVariablesProvider<T> providerOf(String... variables)
    {
        DocumentedUriVariablesProvider<T> result = mock(DocumentedUriVariablesProvider.class);
        when(result.providedVariables()).thenReturn(Lists.newArrayList(variables));
        return result;
    }

}
