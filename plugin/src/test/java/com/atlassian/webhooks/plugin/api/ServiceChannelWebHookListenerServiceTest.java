package com.atlassian.webhooks.plugin.api;

import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;
import com.atlassian.webhooks.api.util.Channel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ServiceChannelWebHookListenerServiceTest
{
    @Mock
    SecuredWebHookListenerService delegate;
    @Mock
    PersistentWebHookListener parameters;
    @InjectMocks
    ServiceChannelWebHookListenerService service;

    @Test
    public void testGetAllWebHookListeners()
    {
        service.getAllWebHookListeners();
        verify(delegate).getAllWebHookListeners(Channel.SERVICE);
    }

    @Test
    public void testGetWebHookListener()
    {
        Integer id = 9;
        service.getWebHookListener(id);
        verify(delegate).getWebHookListener(Channel.SERVICE, id);
    }

    @Test
    public void testRegisterWebHookListener()
    {
        service.registerWebHookListener(parameters);
        verify(delegate).registerWebHookListener(Channel.SERVICE, parameters);
    }

    @Test
    public void testRegisterWebHookListenerRest()
    {
        service.registerWebHookListener(parameters, RegistrationMethod.REST);
        verify(delegate).registerWebHookListener(Channel.REST, parameters);
    }

    @Test
    public void testRegisterWebHookListenerService()
    {
        service.registerWebHookListener(parameters, RegistrationMethod.UI);
        verify(delegate).registerWebHookListener(Channel.UI, parameters);
    }

    @Test
    public void testRegisterWebHookListenerUi()
    {
        service.registerWebHookListener(parameters, RegistrationMethod.SERVICE);
        verify(delegate).registerWebHookListener(Channel.SERVICE, parameters);
    }
}
