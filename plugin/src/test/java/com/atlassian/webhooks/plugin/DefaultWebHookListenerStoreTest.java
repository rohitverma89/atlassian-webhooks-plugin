package com.atlassian.webhooks.plugin;

import com.atlassian.webhooks.api.util.Filter;
import com.atlassian.webhooks.api.util.SectionKey;
import com.atlassian.webhooks.plugin.store.DefaultWebHookListenerStore;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Collections;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class DefaultWebHookListenerStoreTest
{
    @Test
    public void testMarshalingAndUnmarshalingEvents()
    {
        final String marshaledEvents =
                DefaultWebHookListenerStore.DbParamMarshaler.marshalEvents(Lists.newArrayList("jira:issue_updated", "jira:issue_deleted"));

        final Iterable<String> events = DefaultWebHookListenerStore.DbParamMarshaler.unmarshalEvents(marshaledEvents);
        assertThat(events, Matchers.hasItem("jira:issue_updated"));
        assertThat(events, Matchers.hasItem("jira:issue_deleted"));
    }

    @Test
    public void testMarshalingAndUnmarshalingNullEmptyValues()
    {
        final String marshaledEvents = DefaultWebHookListenerStore.DbParamMarshaler.marshalEvents(null);
        final String emptyMarshaledEvents = DefaultWebHookListenerStore.DbParamMarshaler.marshalEvents(Collections.<String>emptyList());

        assertThat(marshaledEvents, CoreMatchers.is(StringUtils.EMPTY));
        assertThat(emptyMarshaledEvents, Matchers.containsString("[]"));
    }

    @Test
    public void testMarshalingParameters()
    {
        final String marshaledParameters = DefaultWebHookListenerStore.DbParamMarshaler.marshalFilters(
                ImmutableMap.of(new SectionKey("section1"), new Filter("filter1"), new SectionKey("section2"), new Filter("filter2"))
        );

        Map<String,String> parameters = DefaultWebHookListenerStore.DbParamMarshaler.unmarshalFilters(marshaledParameters);

        assertThat(parameters.get("section1"), equalTo("filter1"));
        assertThat(parameters.get("section2"), equalTo("filter2"));
    }

}
