package com.atlassian.webhooks.plugin;

import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.spi.WebHookListenerAccessVoter;
import com.atlassian.webhooks.api.util.Channel;
import com.atlassian.webhooks.api.util.Vote;
import com.google.common.base.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import javax.annotation.Nonnull;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OsgiWebHookListenerAccessVoterTest
{
    @Mock
    WebHookEvent event;
    @Mock
    ServiceTracker tracker;
    @Spy
    MockWebHookListenerAccessVoter voter1;
    @Spy
    MockWebHookListenerAccessVoter voter2;
    @Spy
    MockWebHookListenerAccessVoter defaultVoter;

    OsgiWebHookListenerAccessVoter voter;

    WebHookListener LISTENER = WebHookListener.fromPersistentStore(42, "listener", Optional.of("1")).to("/gallifrey.doc.com").build();

    PersistentWebHookListener REGISTERED_LISTENER = PersistentWebHookListener.existing(42).setListenerName("listener").build();

    @Before
    public void setup()
    {
        // override the createServiceTracker method to inject the mock
        voter = new OsgiWebHookListenerAccessVoter(null, defaultVoter)
        {
            @Override
            protected ServiceTracker createServiceTracker(BundleContext bundleContext)
            {
                return tracker;
            }
        };

        when(tracker.getServices()).thenReturn(new Object[] {voter1, voter2});
    }

    @Test
    public void testCanAdmin()
    {
        mockVotes(Vote.DENY, Vote.ALLOW, Vote.ABSTAIN);
        assertEquals(Vote.DENY, voter.canAdmin(REGISTERED_LISTENER, Channel.UI));
        verify(voter1).canAdmin(REGISTERED_LISTENER, Channel.UI);
        verify(voter2).canAdmin(REGISTERED_LISTENER, Channel.UI);
        verify(defaultVoter).canAdmin(REGISTERED_LISTENER, Channel.UI);
    }

    @Test
    public void testCanCreate()
    {
        mockVotes(Vote.ABSTAIN, Vote.ABSTAIN, Vote.ALLOW);
        assertEquals(Vote.ALLOW, voter.canCreate(REGISTERED_LISTENER, Channel.REST));
        verify(voter1).canCreate(REGISTERED_LISTENER, Channel.REST);
        verify(voter2).canCreate(REGISTERED_LISTENER, Channel.REST);
        verify(defaultVoter).canCreate(REGISTERED_LISTENER, Channel.REST);
    }

    @Test
    public void testCanRead()
    {
        mockVotes(Vote.ABSTAIN, Vote.ABSTAIN, Vote.ABSTAIN);
        assertEquals(Vote.ABSTAIN, voter.canRead(REGISTERED_LISTENER, Channel.UI));
        verify(voter1).canRead(REGISTERED_LISTENER, Channel.UI);
        verify(voter2).canRead(REGISTERED_LISTENER, Channel.UI);
        verify(defaultVoter).canRead(REGISTERED_LISTENER, Channel.UI);
    }

    @Test
    public void testCanPublish()
    {
        mockVotes(Vote.ALLOW, Vote.ALLOW, Vote.ALLOW);
        assertEquals(Vote.ALLOW, voter.canPublish(event, LISTENER));
        verify(voter1).canPublish(event, LISTENER);
        verify(voter2).canPublish(event, LISTENER);
        verify(defaultVoter).canPublish(event, LISTENER);
    }

    private void mockVotes(Vote vote1, Vote vote2, Vote vote3) {
        voter1.vote = vote1;
        voter2.vote = vote2;
        defaultVoter.vote = vote3;
    }

    private static class MockWebHookListenerAccessVoter implements WebHookListenerAccessVoter
    {
        private Vote vote;

        @Nonnull
        @Override
        public Vote canCreate(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel)
        {
            return vote;
        }

        @Nonnull
        @Override
        public Vote canRead(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel)
        {
            return vote;
        }

        @Nonnull
        @Override
        public Vote canAdmin(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel)
        {
            return vote;
        }

        @Nonnull
        @Override
        public Vote canPublish(@Nonnull WebHookEvent webHookEvent, @Nonnull WebHookListener listener)
        {
            return vote;
        }
    }
}
