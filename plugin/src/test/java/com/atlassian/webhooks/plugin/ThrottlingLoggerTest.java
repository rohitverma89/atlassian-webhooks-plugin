package com.atlassian.webhooks.plugin;

import com.atlassian.fugue.Effect;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.Mockito.*;

@RunWith (MockitoJUnitRunner.class)
public final class ThrottlingLoggerTest
{
    @Mock
    private TokenBucket bucket;
    @Mock
    private Logger logger;
    @Mock
    private Effect<Logger> effect;
    @InjectMocks
    private ThrottlingLogger throttlingLogger;

    @Test
    public void loggingEffectIsRunWhenTokenIsReceivedFromBucket()
    {
        when(bucket.getToken()).thenReturn(TRUE);
        throttlingLogger.use(effect);
        verify(effect).apply(logger);
    }

    @Test
    public void loggingEffectIsNotRunWhenTokenIsNotAvailableInBucket()
    {
        when(bucket.getToken()).thenReturn(FALSE);
        throttlingLogger.use(effect);
        verify(effect, times(0)).apply(logger);
    }

}
