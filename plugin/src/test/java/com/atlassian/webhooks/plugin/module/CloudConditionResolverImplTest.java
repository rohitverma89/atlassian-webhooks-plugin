package com.atlassian.webhooks.plugin.module;

import com.atlassian.webhooks.api.register.WebHookPluginRegistration;
import com.atlassian.webhooks.spi.WebHookPluginRegistrationFactory;
import com.google.common.collect.Sets;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashSet;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CloudConditionResolverImplTest
{
    private CloudConditionResolver cloudConditionResolver;

    @Mock
    private WebHookPluginRegistrationContainer container;

    @Before
    public void setUp()
    {
        cloudConditionResolver = new CloudConditionResolverImpl(container);
    }

    @Test
    public void getsVersionFromSpi()
    {
        final WebHookPluginRegistration webHookPluginRegistration = WebHookPluginRegistration.builder()
                .cloudCondition(mockCloudCondition(true))
                .build();
        when(container.getWebHookRegistrations()).thenReturn(Sets.newHashSet(webHookPluginRegistration));

        assertThat(cloudConditionResolver.isCloud(), Matchers.is(true));
    }

    @Test
    public void returnsFalseIfMultipleSPIsNotConsistent()
    {
        final HashSet<WebHookPluginRegistration> webHookPluginRegistrations = Sets.newHashSet(
                WebHookPluginRegistration.builder()
                        .cloudCondition(mockCloudCondition(true))
                        .build(),
                WebHookPluginRegistration.builder()
                        .cloudCondition(mockCloudCondition(false))
                        .build()
        );
        when(container.getWebHookRegistrations()).thenReturn(webHookPluginRegistrations);

        assertThat(cloudConditionResolver.isCloud(), Matchers.is(false));
    }

    @Test
    public void returnsFalseByDefault()
    {
        assertThat(cloudConditionResolver.isCloud(), Matchers.is(false));
    }

    private WebHookPluginRegistrationFactory.CloudCondition mockCloudCondition(final boolean cloudSetting)
    {
        final WebHookPluginRegistrationFactory.CloudCondition cloudCondition = mock(WebHookPluginRegistrationFactory.CloudCondition.class);
        when(cloudCondition.isCloud()).thenReturn(cloudSetting);
        return cloudCondition;
    }
}