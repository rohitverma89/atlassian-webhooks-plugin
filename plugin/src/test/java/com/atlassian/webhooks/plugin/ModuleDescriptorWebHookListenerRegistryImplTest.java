package com.atlassian.webhooks.plugin;

import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerService;
import com.atlassian.webhooks.api.util.EventMatchers;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ModuleDescriptorWebHookListenerRegistryImplTest
{
    public static final WebHookListener WEBHOOK_LISTENER = WebHookListener.fromModuleDescriptor("pluginKey").to("/gallifrey").build();
    public static final String WEBHOOK_ID = "id";

    @Mock
    private WebHookListenerService webHookListenerService;

    @InjectMocks
    private PersistentWebHookListenerProvider persistentWebHookListenerProvider;

    private ModuleDescriptorWebHookListenerRegistryImpl moduleDescriptorWebHookListenerRegistry = new ModuleDescriptorWebHookListenerRegistryImpl();

    @Test
    public void moduleDescriptorWebHookListenerProviderCanUnregisterWebHookListeners()
    {
        moduleDescriptorWebHookListenerRegistry.register(WEBHOOK_ID, WEBHOOK_LISTENER);
        assertThat(Lists.newArrayList(moduleDescriptorWebHookListenerRegistry.getListeners(event(WEBHOOK_ID))), hasSize(1));
        moduleDescriptorWebHookListenerRegistry.unregister(WEBHOOK_ID, WEBHOOK_LISTENER);
        assertThat(Lists.newArrayList(moduleDescriptorWebHookListenerRegistry.getListeners(event(WEBHOOK_ID))), hasSize(0));
    }

    private WebHookEvent event(final String id)
    {
        return new WebHookEvent(id, 42, EventMatchers.ALWAYS_TRUE);
    }
}
