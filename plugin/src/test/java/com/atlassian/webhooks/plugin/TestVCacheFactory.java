package com.atlassian.webhooks.plugin;

import com.atlassian.annotations.nonnull.FieldsAreNonnullByDefault;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.vcache.ChangeRate;
import com.atlassian.vcache.VCacheFactory;
import com.atlassian.vcache.internal.core.DefaultVCacheCreationHandler;
import com.atlassian.vcache.internal.core.ThreadLocalRequestContextSupplier;
import com.atlassian.vcache.internal.core.metrics.DefaultMetricsCollector;
import com.atlassian.vcache.internal.guava.GuavaServiceSettingsBuilder;
import com.atlassian.vcache.internal.guava.GuavaVCacheService;
import com.atlassian.vcache.internal.test.EmptyVCacheSettingsDefaultsProvider;

import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Duration;

@FieldsAreNonnullByDefault
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class TestVCacheFactory
{
    public static VCacheFactory create(ThreadLocalRequestContextSupplier requestContextSupplier)
    {
        return new GuavaVCacheService(
                "confira",
                requestContextSupplier,
                new EmptyVCacheSettingsDefaultsProvider(),
                new DefaultVCacheCreationHandler(20, Duration.ofHours(1), 20, ChangeRate.HIGH_CHANGE, ChangeRate.HIGH_CHANGE),
                new DefaultMetricsCollector(requestContextSupplier),
                new GuavaServiceSettingsBuilder().build(), // Don't enable the serialisation hack; we want to exercise the marshallers.
                context -> {}
        );
    }
}
