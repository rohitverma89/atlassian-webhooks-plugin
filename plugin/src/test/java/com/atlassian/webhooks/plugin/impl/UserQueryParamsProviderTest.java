package com.atlassian.webhooks.plugin.impl;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserProfile;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserQueryParamsProviderTest
{
    private UserQueryParamsProvider userQueryParamsProvider;

    @Before
    public void setUp() throws Exception
    {
        userQueryParamsProvider = new UserQueryParamsProvider();
    }

    @Test
    public void noParamsAreReturnedForAnonymousUser()
    {
        Multimap<String, String> params = userQueryParamsProvider.provideQueryParams(Optional.<UserProfile>empty(), null);

        assertThat(params.size(), is(0));
    }

    @Test
    public void paramsWithUserIdAndKeyAreReturnedForLoggedInUser()
    {
        String userName = "user-id";
        String userKey = "user-key";
        UserProfile userProfile = mock(UserProfile.class);
        when(userProfile.getUsername()).thenReturn(userName);
        when(userProfile.getUserKey()).thenReturn(new UserKey(userKey));

        Multimap<String, String> params = userQueryParamsProvider.provideQueryParams(Optional.of(userProfile), null);

        assertThat(params, is((Multimap<String, String>) ImmutableMultimap.of(
                "user_id", userName,
                "user_key", userKey
        )));
    }
}