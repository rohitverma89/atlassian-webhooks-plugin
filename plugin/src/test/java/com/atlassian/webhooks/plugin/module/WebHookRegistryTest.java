package com.atlassian.webhooks.plugin.module;

import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.RegisteredWebHookEvent;
import com.atlassian.webhooks.api.register.WebHookEventGroup;
import com.atlassian.webhooks.api.register.WebHookEventSection;
import com.atlassian.webhooks.api.register.WebHookPluginRegistration;
import com.atlassian.webhooks.api.util.EventMatchers;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertThat;


@RunWith (MockitoJUnitRunner.class)
public class WebHookRegistryTest
{

    public static final ModuleKey MODULE_KEY = new ModuleKey("moduleKey");

    private final WebHookEventGroup group = WebHookEventGroup.builder()
            .addEvent(RegisteredWebHookEvent.withId("abstract_test_event")
                    .firedWhen(BaseTestEvent.class)
                    .isMatchedBy(EventMatchers.ALWAYS_TRUE))
            .addEvent(RegisteredWebHookEvent.withId("another_test_event")
                    .firedWhen(AnotherTestEvent.class)
                    .isMatchedBy(EventMatchers.ALWAYS_TRUE))
            .build();

    private final WebHookEventSection section = WebHookEventSection.section("test").addGroup(group).build();
    private final WebHookPluginRegistration testPluginRegistration = WebHookPluginRegistration.builder().addWebHookSection(section).build();

    private final WebHookRegistry webHookRegistry = new WebHookRegistry();

    @Test
    public void testGettingAllWebHookEvents()
    {
        webHookRegistry.add(MODULE_KEY, testPluginRegistration);
        assertThat(webHookRegistry.getWebHookIds(), hasWebHooks("abstract_test_event", "another_test_event"));

        webHookRegistry.remove(MODULE_KEY);
        assertThat(webHookRegistry.getWebHookIds(), Matchers.<WebHookKey>iterableWithSize(0));
    }

    @Test
    public void testGettingWebHookForEvent()
    {
        webHookRegistry.add(MODULE_KEY, testPluginRegistration);

        assertThat(webHookRegistry.getWebHooks(new BaseTestEvent()), hasEvents("abstract_test_event"));

        webHookRegistry.remove(MODULE_KEY);
        assertThat(webHookRegistry.getWebHookIds(), Matchers.<WebHookKey>iterableWithSize(0));
    }

    @Test
    public void testGettingWebHookForSubclassOfRegisteredEvent()
    {

        webHookRegistry.add(MODULE_KEY, testPluginRegistration);

        Iterable<WebHookEvent> webHookEvents = webHookRegistry.getWebHooks(new TestEvent());
        assertThat(webHookEvents, hasEvents("abstract_test_event"));

        webHookRegistry.remove(MODULE_KEY);
    }

    @Test
    public void testGettingWebHookForSecondSubclassOfRegisteredEvent()
    {

        webHookRegistry.add(MODULE_KEY, testPluginRegistration);

        Iterable<WebHookEvent> webHookEvents = webHookRegistry.getWebHooks(new ExtTestEvent());
        assertThat(webHookEvents, hasEvents("abstract_test_event"));

        webHookRegistry.remove(MODULE_KEY);
        assertThat(webHookRegistry.getWebHookIds(), Matchers.<WebHookKey>iterableWithSize(0));
    }

    @Test
    public void testGettingWebHookForAnonymousSubclassOfRegisteredEvent()
    {

        webHookRegistry.add(MODULE_KEY, testPluginRegistration);

        Iterable<WebHookEvent> webHookEvents = webHookRegistry.getWebHooks(new ExtTestEvent()
        {
        });
        assertThat(webHookEvents, hasEvents("abstract_test_event"));

        webHookRegistry.remove(MODULE_KEY);
        assertThat(webHookRegistry.getWebHookIds(), Matchers.<WebHookKey>iterableWithSize(0));
    }

    public static final ModuleKey MODULE_KEY_2 = new ModuleKey("testKey2");

    @Test
    public void testAddingSecondProviderWithClashingIds()
    {
        WebHookEventGroup group = WebHookEventGroup.builder()
                .addEvent(RegisteredWebHookEvent.withId("another_test_event")
                        .firedWhen(BaseTestEvent.class)
                        .isMatchedBy(EventMatchers.ALWAYS_TRUE))
                .addEvent(RegisteredWebHookEvent.withId("no_clash_event")
                        .firedWhen(NoClashEvent.class)
                        .isMatchedBy(EventMatchers.ALWAYS_TRUE))
                .build();

        WebHookEventSection section = WebHookEventSection.section("test").addGroup(group).build();
        WebHookPluginRegistration clashingPluginRegistration = WebHookPluginRegistration.builder().addWebHookSection(section).build();

        webHookRegistry.add(MODULE_KEY, testPluginRegistration);
        webHookRegistry.add(MODULE_KEY_2, clashingPluginRegistration);

        assertThat(webHookRegistry.getWebHookIds(), hasWebHooks("abstract_test_event", "another_test_event", "no_clash_event"));

        // BaseTestEvent is registered under two ids - another_test_event and abstract_test_event
        assertThat(webHookRegistry.getWebHooks(new BaseTestEvent()), hasEvents("another_test_event", "abstract_test_event"));
        // ExtTestEvent which is a subclass of BaseTestEvent should get the same WebHookEvents
        assertThat(webHookRegistry.getWebHooks(new ExtTestEvent()), hasEvents("another_test_event", "abstract_test_event"));

        webHookRegistry.remove(MODULE_KEY);

        // after removal we should still keep "another_test_event" and "no_clash_event" as they came with another provider
        assertThat(webHookRegistry.getWebHookIds(), hasWebHooks("another_test_event", "no_clash_event"));
    }

    private static TypeSafeMatcher<Iterable<WebHookEvent>> hasEvents(final String... eventIds)
    {
        return new TypeSafeMatcher<Iterable<WebHookEvent>>()
        {
            @Override
            protected boolean matchesSafely(final Iterable<WebHookEvent> webHookEvents)
            {
                final ArrayList<String> expectedEventIds = Lists.newArrayList(eventIds);
                return Iterables.all(expectedEventIds, new Predicate<String>()
                {
                    @Override
                    public boolean apply(final String expectedEventId)
                    {
                        Iterable<String> actualEventIds = Iterables.transform(webHookEvents, new Function<WebHookEvent, String>()
                        {
                            @Override
                            public String apply(final WebHookEvent webHookEvent)
                            {
                                return webHookEvent.getId();
                            }
                        });
                        return Iterables.contains(actualEventIds, expectedEventId);
                    }
                });
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("Did not get all expected event ids " + Arrays.toString(eventIds));
            }
        };
    }

    private static Matcher<Iterable<WebHookKey>> hasWebHooks(String... ids)
    {
        return Matchers.hasItems(Iterables.toArray(Iterables.transform(Arrays.asList(ids), new Function<String, WebHookKey>()
        {
            @Override
            public WebHookKey apply(final String input)
            {
                return new WebHookKey(input);
            }
        }), WebHookKey.class));
    }

    private static class AnotherTestEvent
    {
    }

    private static class BaseTestEvent
    {
    }

    private static class TestEvent extends BaseTestEvent
    {
    }

    private static class ExtTestEvent extends TestEvent
    {
    }

    private static class NoClashEvent
    {
    }
}
