package com.atlassian.webhooks.plugin;

import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerService;
import com.atlassian.webhooks.api.util.ConsiderAllListeners;
import com.atlassian.webhooks.api.util.SectionKey;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.plugin.module.WebHookRegistry;
import com.atlassian.webhooks.spi.EventMatcher;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterableOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersistentWebHookListenerProviderTest
{
    public static final String EVENT_ID = "event";
    private PersistentWebHookListenerProvider persistentWebHookListenerProvider;

    @Mock
    private WebHookListenerService webHookListenerService;
    @Mock
    private WebHookEvent webHookEvent;
    @Mock
    private WebHookPluginRegistrationContainer container;
    @Mock
    private WebHookRegistry registry;

    @Before
    public void setUp() throws Exception
    {
        persistentWebHookListenerProvider = new PersistentWebHookListenerProvider(webHookListenerService, container);
        when(container.getWebHookRegistry()).thenReturn(registry);
        when(registry.sectionOf(anyString())).thenReturn(new SectionKey("test-section"));
        when(webHookEvent.getId()).thenReturn(EVENT_ID);
        when(webHookEvent.getEventMatcher()).thenReturn(mock(EventMatcher.class));
    }

    @Test
    public void testGetListeners() throws Exception
    {
        WebHookListener webHookListener = assumeListenerReturnedByService(true, EVENT_ID);

        Iterable<WebHookListener> listeners = persistentWebHookListenerProvider.getListeners(webHookEvent);
        assertThat(listeners, contains(webHookListener));
    }

    @Test
    public void testGetListenersWhenListenerIsDisabled() throws Exception
    {
        assumeListenerReturnedByService(false, EVENT_ID);

        Iterable<WebHookListener> listeners = persistentWebHookListenerProvider.getListeners(webHookEvent);
        assertThat(listeners, empty());
    }

    @Test
    public void testGetListenersWhenNoOneListenToPassedWebHook() throws Exception
    {
        assumeListenerReturnedByService(true, "some-other-id");

        Iterable<WebHookListener> listeners = persistentWebHookListenerProvider.getListeners(webHookEvent);
        assertThat(listeners, empty());
    }

    @Test
    public void testConsiderAllListeners() throws Exception
    {
        assumeListenerReturnedByService(true, "some-other-id");

        when(webHookEvent.getEventMatcher()).thenReturn(new MatcherWithoutAutomaticFiltering());

        Iterable<WebHookListener> listeners = persistentWebHookListenerProvider.getListeners(webHookEvent);
        assertThat(Lists.newArrayList(listeners), hasSize(1));
    }

    @ConsiderAllListeners
    private static class MatcherWithoutAutomaticFiltering implements EventMatcher<Object>
    {

        @Override
        public boolean matches(final Object event, final WebHookListener listener)
        {
            return true;
        }
    }

    @Test
    public void testFilterResolutionAccordingToEventSection()
    {
        assumeSectionMapping("event-1", "section-1");
        assumeSectionMapping(EVENT_ID, "section-2");
        assumeListenerReturnedByService(true, EVENT_ID, ImmutableMap.of("section-1", "filter1", "section-2", "filter2"));
        WebHookListener listener = persistentWebHookListenerProvider.getListeners(webHookEvent).iterator().next();
        assertThat(listener.getParameters().getFilter(), equalTo("filter2"));
    }

    private WebHookListener assumeListenerReturnedByService(boolean enabled, String eventId)
    {
        return assumeListenerReturnedByService(enabled, eventId, Collections.<String, String>emptyMap());
    }

    private WebHookListener assumeListenerReturnedByService(boolean enabled, String eventId, Map<String, String> filters)
    {
        PersistentWebHookListener webHookListener = PersistentWebHookListener
                .existing(42)
                .setListenerName("name")
                .addWebHookId(eventId)
                .setEnabled(enabled)
                .addFilters(filters)
                .setLastUpdatedByUser("1")
                .setUrl("/gallifrey")
                .build();
        when(webHookListenerService.getAllWebHookListeners()).thenReturn(ImmutableList.of(webHookListener));
        return WebHookListener.fromPersistentStore(42, "name", Optional.of("1")).to("/gallifrey").build();
    }

    private void assumeSectionMapping(final String eventId, final String sectionId) {
        when(registry.sectionOf(eventId)).thenReturn(new SectionKey(sectionId));
    }

    private Matcher<Iterable<WebHookListener>> empty()
    {
        return emptyIterableOf(WebHookListener.class);
    }
}