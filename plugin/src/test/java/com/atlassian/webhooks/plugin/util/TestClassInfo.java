package com.atlassian.webhooks.plugin.util;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.*;

import static com.atlassian.webhooks.plugin.util.ClassInfo.HierarchyLevel.level;
import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.*;

public class TestClassInfo
{
    @Test
    public void classHierarchyIsComputedCorrectlyForClassesWithoutInterfaces()
    {
        assertThat(hierarchyFor(SubSub.class), is(level(SubSub.class), level(Sub.class), level(Super.class), level(Object.class)));
    }

    @Test
    public void classHierarchyLikeIssueEventsIsComputedCorrectly()
    {
        assertThat(hierarchyFor(IssueEvent.class), is(
                level(IssueEvent.class), level(AbstractEvent.class, IssueRelatedEvent.class),
                level(JiraEvent.class), level(Object.class)));
    }

    @Test
    public void classHierarchyLikeProjectEventsIsComputedCorrectly()
    {
        assertThat(hierarchyFor(ProjectCreatedEvent.class), is(
                level(ProjectCreatedEvent.class), level(AbstractProjectEvent.class),
                level(AbstractEvent.class, ProjectRelatedEvent.class),
                level(JiraEvent.class), level(Object.class)));
    }

    @Test
    public void classHierarchyWhenImplementingMultipleInterfacesIsComputedCorrectly()
    {
        assertThat(hierarchyFor(Quasimodo.class), is(
                level(Quasimodo.class), level(Sub.class, IssueRelatedEvent.class, SomeInterface.class),
                level(Super.class, JiraEvent.class), level(Object.class)));
    }

    private static List<ClassInfo.HierarchyLevel> hierarchyFor(Class<?> type)
    {
        return ClassInfo.of(type).typeHierarchy();
    }

    private static Matcher<List<ClassInfo.HierarchyLevel>> is(ClassInfo.HierarchyLevel... types)
    {
        return (Matcher<List<ClassInfo.HierarchyLevel>>) Matchers.equalTo(Arrays.asList(types));
    }

    private static class Super {}

    private static class Sub extends Super {}

    private static class SubSub extends Sub {}

    private static interface SomeInterface {}

    private static interface JiraEvent {}

    private static interface IssueRelatedEvent extends JiraEvent {}

    private static interface  ProjectRelatedEvent extends JiraEvent {}

    private static class AbstractEvent implements JiraEvent {}

    private static class AbstractProjectEvent extends AbstractEvent implements ProjectRelatedEvent {}

    private static class IssueEvent extends AbstractEvent implements IssueRelatedEvent {}

    private static class ProjectCreatedEvent extends AbstractProjectEvent {}

    private static class Quasimodo extends Sub implements IssueRelatedEvent, SomeInterface {};
}
