package com.atlassian.webhooks.plugin.module;


import com.atlassian.webhooks.spi.EventSerializer;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;


import java.util.Collections;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class ClassSpecificProcessorsTest
{
    public static final EventSerializer SERIALIZER_1 = mock(EventSerializer.class);
    public static final EventSerializer SERIALIZER_2 = mock(EventSerializer.class);
    public static final EventSerializer SERIALIZER_3 = mock(EventSerializer.class);

    @Test
    public void ifExactSerializerIsProvidedThenReturnThat()
    {
        ClassSpecificProcessors<EventSerializer> processors = new ClassSpecificProcessors<EventSerializer>(ImmutableMap.<Class, EventSerializer>builder().put(Integer.class, SERIALIZER_1).build());
        assertThat(processors.forType(Integer.class).get(), (Matcher) equalTo(SERIALIZER_1));
    }

    @Test(expected = IllegalStateException.class)
    public void ifProcessorsForClassesOnTheSameHierarchyLevelAreRegisteredThenExplode()
    {
        ClassSpecificProcessors<EventSerializer> processors = new ClassSpecificProcessors<EventSerializer>(ImmutableMap.<Class, EventSerializer>builder().put(
                Super.class, SERIALIZER_1).put(A.class, SERIALIZER_2).build());
        processors.forType(Sub.class).get();
    }

    @Test
    public void iProcessorForSuperTypeIsProviderThenItIsReturned()
    {
        ClassSpecificProcessors<EventSerializer> processors = new ClassSpecificProcessors<EventSerializer>(ImmutableMap.<Class, EventSerializer>builder().put(Super.class, SERIALIZER_1).build());
        assertThat(processors.forType(Sub.class).get(), equalTo(SERIALIZER_1));
    }

    @Test
    public void iProcessorsAreProvidedForAllClassesInHierarchyThenTheCorrectOneIsChosen()
    {
        ClassSpecificProcessors<EventSerializer> processors = new ClassSpecificProcessors<EventSerializer>(ImmutableMap.<Class, EventSerializer>builder()
                .put(SubSub.class, SERIALIZER_1)
                .put(Sub.class, SERIALIZER_2)
                .put(Super.class, SERIALIZER_3)
                .build());
        assertThat(processors.forType(Sub.class).get(), equalTo(SERIALIZER_2));
    }

    @Test
    public void ifNProcessorsAreSuppliedThenAbsent()
    {
        ClassSpecificProcessors<EventSerializer> processors = new ClassSpecificProcessors<EventSerializer>(Collections.<Class, EventSerializer>emptyMap());
        assertThat(processors.forType(Integer.class), (Matcher) equalTo(Optional.absent()));
    }

    @Test
    public void ifNoMatchingProcessorIsSuppliedThenAbsent()
    {
        ClassSpecificProcessors<EventSerializer> processors = new ClassSpecificProcessors<EventSerializer>(ImmutableMap.<Class, EventSerializer>builder().put(Super.class, SERIALIZER_1).build());
        assertThat(processors.forType(Integer.class), (Matcher) equalTo(Optional.absent()));
    }

    private static class Super {}

    private static interface A {}

    private static class Sub extends Super implements A {}

    private static class SubSub extends Sub {}
}
