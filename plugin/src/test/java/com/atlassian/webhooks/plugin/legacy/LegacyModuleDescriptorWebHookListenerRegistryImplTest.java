package com.atlassian.webhooks.plugin.legacy;

import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.spi.provider.PluginModuleListenerParameters;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.net.URI;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public final class LegacyModuleDescriptorWebHookListenerRegistryImplTest
{
    public static final URI URI = java.net.URI.create("/example");
    private final LegacyModuleDescriptorWebHookListenerRegistryImpl registry = new LegacyModuleDescriptorWebHookListenerRegistryImpl();

    @Test
    public void listenerIsReturnedAfterRegistration()
    {
        PluginModuleListenerParameters listener = generateListener();
        registry.register("webhook", "pluginKey", URI, listener);
        assertThat(registry.getListeners("webhook"), hasSize(1));
        assertThat(registry.getListeners("webhook"), contains(LegacyListener.of(URI, "pluginKey", listener)));
    }

    @Test
    public void listenerCanBeUnregistered()
    {
        PluginModuleListenerParameters listener = generateListener();
        registry.register("webhook", "pluginKey", URI, listener);
        registry.unregister("webhook", "pluginKey", URI, listener);
        assertThat(registry.getListeners("webhook"), hasSize(0));
    }

    @Test
    public void listenersAreRecognizedCorrectlyWhenUnregistering()
    {
        registry.register("webhook", "pluginKey", URI, listenerWithParam("1"));
        registry.unregister("webhook", "pluginKey", URI, listenerWithParam("2"));
        assertThat(registry.getListeners("webhook"), hasSize(1));
    }

    @Test
    public void registeringAndUnregisteringTakesWebhookIdIntoConsideration()
    {
        registry.register("webhook", "pluginKey", URI, listenerWithParam("1"));
        registry.register("webhook-2", "pluginKey", URI, listenerWithParam("2"));
        assertThat(registry.getListeners("webhook"), hasSize(1));
        assertThat(registry.getListeners("webhook-2"), hasSize(1));
        registry.unregister("webhook-2", "pluginKey", URI, listenerWithParam("2"));
        assertThat(registry.getListeners("webhook"), hasSize(1));
        assertThat(registry.getListeners("webhook-2"), hasSize(0));
    }

    @Test
    public void legacyJiraIssuesParametersAreTranslatedToTheNewOnesCorrectly()
    {
        registry.register("webhook", "pluginKey", URI, issueListener("jql-filter", true));
        assertThat(registry.getListeners("webhook").iterator().next().getListener(), equalTo(
                WebHookListener.fromModuleDescriptor("pluginKey").excludeBody(true).to(URI.toString()).withFilter("jql-filter").build()));

        // clean up
        registry.unregister("webhook", "pluginKey", URI, issueListener("jql-filter", true));
        assertThat(registry.getListeners("webhook"), hasSize(0));

        registry.register("webhook", "pluginKey", URI, issueListener("jql-filter", false));
        assertThat(registry.getListeners("webhook").iterator().next().getListener(), equalTo(
                WebHookListener.fromModuleDescriptor("pluginKey").excludeBody(false).to(URI.toString()).withFilter("jql-filter").build()));
    }

    private static PluginModuleListenerParameters generateListener()
    {
        return new PluginModuleListenerParameters(randomAlphabetic(10), Optional.of(randomAlphabetic(10)),
                ImmutableMap.<String, Object>of("param", randomAlphabetic(10)), randomAlphabetic(10));
    }

    private static PluginModuleListenerParameters listenerWithParam(String param)
    {
        return new PluginModuleListenerParameters("plugin-key-param", Optional.of("module-key-param"),
                ImmutableMap.<String, Object>of("param", param), "event-identifier-param");
    }

    private static PluginModuleListenerParameters issueListener(String filter, boolean excludeDetails)
    {
        return new PluginModuleListenerParameters("plugin-key", Optional.of("moduleKey"),
                ImmutableMap.<String, Object>of("excludeIssueDetails", String.valueOf(excludeDetails), "jql", filter), "eventId");
    }

}
