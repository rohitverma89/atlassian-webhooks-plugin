package com.atlassian.webhooks.plugin.impl;

import com.atlassian.httpclient.api.Request;
import com.atlassian.osgi.tracker.WaitableServiceTrackerFactory;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import com.atlassian.webhooks.plugin.module.WebHookPluginRegistrationContainer;
import com.atlassian.webhooks.spi.RequestSigner;
import com.atlassian.webhooks.spi.RequestSigner2;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;

import java.net.URI;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RequestSignerImplTest
{
    @Mock
    private WebHookPluginRegistrationContainer container;

    private RequestSignerImpl requestSignerImpl;

    @Before
    public void setUp()
    {
        this.requestSignerImpl = new RequestSignerImpl(new WaitableServiceTrackerFactory(mock(BundleContext.class)), container);
    }

    @Test
    public void testSigningRequestWithRequestSigner2()
    {
        final RequestSigner2 requestSigner2 = new RequestSigner2()
        {
            @Override
            public void sign(final URI uri, final Optional<UserProfile> userProfile,
                    final WebHookListenerRegistrationDetails registrationDetails, final Request.Builder request)
            {
                request.setHeader("Auth", userProfile.get().getUsername());
            }
        };
        when(container.getRequestSigners2()).thenReturn(Sets.newHashSet(requestSigner2));
        Request.Builder builder = mock(Request.Builder.class);

        final UserProfile userProfile = mock(UserProfile.class);
        final String username = "username";
        when(userProfile.getUsername()).thenReturn(username);
        requestSignerImpl.sign(URI.create("http://example.com"),
                Optional.of(userProfile),
                WebHookListenerRegistrationDetails.moduleDescriptor("pluginKey"),
                builder);

        verify(builder).setHeader("Auth", username);
    }

    @Test
    public void testSigningRequestWithNewRequestSigner()
    {
        final RequestSigner requestSigner = new RequestSigner()
        {
            @Override
            public void sign(final URI uri, final WebHookListenerRegistrationDetails registrationDetails, final Request.Builder request)
            {
                request.setHeader("Auth", "abcd");
            }
        };
        when(container.getRequestSigners()).thenReturn(Sets.newHashSet(requestSigner));
        Request.Builder builder = mock(Request.Builder.class);

        requestSignerImpl.sign(URI.create("http://example.com"), Optional.<UserProfile>empty(), WebHookListenerRegistrationDetails.moduleDescriptor("pluginKey"), builder);

        verify(builder).setHeader("Auth", "abcd");
    }

    @Test
    public void testSigningRequestWithLegacyRequestSigner()
    {
        final com.atlassian.webhooks.spi.plugin.RequestSigner requestSigner = new com.atlassian.webhooks.spi.plugin.RequestSigner()
        {
            @Override
            public void sign(final URI uri, final String pluginKey, final Request.Builder request)
            {
                request.setHeader("Auth", "abcd");
            }
        };

        requestSignerImpl.getTracker().adding(requestSigner);

        Request.Builder builder = mock(Request.Builder.class);

        requestSignerImpl.sign(URI.create("http://example.com"), Optional.<UserProfile>empty(), WebHookListenerRegistrationDetails.moduleDescriptor("pluginKey"), builder);

        verify(builder).setHeader("Auth", "abcd");
    }
}