package com.atlassian.webhooks.plugin.store;

import com.atlassian.fugue.Option;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;
import com.atlassian.webhooks.spi.WebHookListenerStore;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class MockWebHookListenerStore implements WebHookListenerStore
{
    private final UserManager userManager;
    private Map<Integer, PersistentWebHookListener> store = new HashMap<Integer, PersistentWebHookListener>();
    private int nextId = 0;

    public MockWebHookListenerStore(final UserManager userManager)
    {
        this.userManager = userManager;
    }

    @Override
    public PersistentWebHookListener addWebHook(PersistentWebHookListener listener, final RegistrationMethod registrationMethod)
    {
        int generatedId = nextId++;
        store.put(generatedId, updated(listener, generatedId).build());
        return store.get(generatedId);
    }

    @Override
    public PersistentWebHookListener updateWebHook(PersistentWebHookListener listener)
            throws IllegalArgumentException
    {
        final PersistentWebHookListener listenerFromStore = store.get(listener.getId().get());
        if (listenerFromStore == null)
        {
            throw new IllegalArgumentException();
        }
        store.put(listenerFromStore.getId().get(), updated(listener, listenerFromStore.getId().get()).build());
        return store.get(listenerFromStore.getId().get());
    }

    @Override
    public Option<PersistentWebHookListener> getWebHook(final int id)
    {
        return Option.option(store.get(id));
    }

    @Override
    public void removeWebHook(int id) throws IllegalArgumentException
    {
        store.remove(id);
    }

    @Override
    public Collection<PersistentWebHookListener> getAllWebHooks()
    {
        return store.values();
    }

    private PersistentWebHookListener.Builder updated(PersistentWebHookListener listener, int id)
    {
        return PersistentWebHookListener
                .existing(id)
                .addFiltersTypeRich(listener.getFilters())
                .addWebHookIds(listener.getEvents())
                .setEnabled(listener.isEnabled())
                .setExcludeBody(listener.isExcludeBody())
                .setLastUpdated(new Date())
                .setListenerName(listener.getName())
                .setUrl(listener.getUrl())
                .setDescription(listener.getDescription())
                .setLastUpdatedByUser(userManager.getRemoteUserKey().getStringValue());
    }
}
