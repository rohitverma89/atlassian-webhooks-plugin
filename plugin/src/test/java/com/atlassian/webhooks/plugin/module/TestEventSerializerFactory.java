package com.atlassian.webhooks.plugin.module;

import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.util.EventMatchers;
import com.atlassian.webhooks.api.util.EventSerializers;
import com.atlassian.webhooks.spi.EventSerializer;
import com.google.common.base.Optional;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestEventSerializerFactory
{
    public static final Class<Integer> TYPE = Integer.class;
    public static final WebHookEvent TEST_EVENT = new WebHookEvent("id", 42, EventMatchers.ALWAYS_TRUE);
    @Mock
    WebHookPluginRegistrationContainer container;

    @Mock
    ClassSpecificProcessors<EventSerializer> eventSerializers;

    @InjectMocks
    WebHookEventSerializer factory;

    @Before
    public void setUp() throws Exception
    {
        when(container.getEventSerializers()).thenReturn(eventSerializers);
    }

    @Test
    public void ifEventSerializersFromContainerReturnSomeThanReturnThat()
    {
        EventSerializer returnedSerializer = mock(EventSerializer.class);
        when(eventSerializers.forType(TYPE)).thenReturn(Optional.of(returnedSerializer));
        assertThat(factory.serializerFor(TYPE), (Matcher) equalTo(returnedSerializer));
    }

    @Test
    public void ifEventSerializersFromContainerReturnAbsentThenReturnReflection()
    {
        when(eventSerializers.forType(TYPE)).thenReturn(Optional.<EventSerializer>absent());
        assertThat(factory.serializerFor(TYPE), (Matcher) equalTo(EventSerializers.REFLECTION));
    }

    @Test
    public void ifExcludeWebHookBodyParameterIsTrueThenReturnEmptyString()
    {
        assumeToStringSerializerIsReturnedForTestEvent();

        String serialized = factory.serialize(TEST_EVENT, listenerWithExcludeBodySetTo(true));

        assertThat(serialized, equalTo(""));
    }

    @Test
    public void ifExcludeWebhookBodyParameterIsFalseThenSerialize()
    {
        assumeToStringSerializerIsReturnedForTestEvent();

        String serialized = factory.serialize(TEST_EVENT, listenerWithExcludeBodySetTo(false));

        assertThat(serialized, equalTo(TEST_EVENT.getEvent().toString()));
    }

    private static WebHookListener listenerWithExcludeBodySetTo(boolean exclude)
    {
        return WebHookListener.fromPersistentStore(42, "name", Optional.of("1")).to("/").excludeBody(exclude).build();
    }

    private void assumeToStringSerializerIsReturnedForTestEvent()
    {
        when(eventSerializers.forType(TYPE)).thenReturn(Optional.<EventSerializer>of(new EventSerializer()
        {
            @Override
            public String serialize(Object event)
            {
                return event.toString();
            }
        }));
    }
}
