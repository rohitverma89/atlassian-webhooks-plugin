# Atlassian WebHooks Plugin

## Description

This is an Atlassian Plugin that allows products and plugins as providers to define events as webhooks. As consumers
they can register against web hooks and as such be notified via an HTTP POST request when the event corresponding to the
web hook is fired.

## Atlassian Developer?

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/WEBHOOKS)

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/WEBHOOKS)

### Documentation

TBD.

### Forking

This plugin is forked. The cloud fork can be found at https://bitbucket.org/atlassian/cloud-atlassian-webhooks-plugin