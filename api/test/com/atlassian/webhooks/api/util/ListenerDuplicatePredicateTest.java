package com.atlassian.webhooks.api.util;

import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.webhooks.api.register.listener.PersistentWebHookListener.existing;
import static com.atlassian.webhooks.api.register.listener.PersistentWebHookListener.newlyCreated;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ListenerDuplicatePredicateTest
{
    @Test
    public void youCantBeADuplicateOfYourself()
    {
        assertNotDuplicate(defaultParams(existing(42)).build(), defaultParams(existing(42)).build());
    }

    @Test
    public void duplicateWithDefaultParamsIsRecognized()
    {
        assertDuplicate(defaultParams(existing(42)).build(), defaultParams(newlyCreated()).build());
    }

    @Test
    public void listenersWithDifferentExcludeBodyParameterAreNotDuplicates()
    {
        assertNotDuplicate(defaultParams(existing(42)).setExcludeBody(true).build(), defaultParams(newlyCreated()).setExcludeBody(false).build());
    }

    @Test
    public void listenersWithDifferentFiltersAreNotDuplicates()
    {
        assertNotDuplicate(defaultParams(existing(42)).addFilter("default", "project = MKY").build(), defaultParams(newlyCreated()).addFilter("default", "project = ATL").build());
    }

    @Test
    public void listenersWithDifferentUrlsAreNotDuplicates()
    {
        assertNotDuplicate(defaultParams(existing(42)).setUrl("/url1").build(), defaultParams(newlyCreated()).setUrl("/url2").build());
    }

    @Test
    public void listenersWithDifferentEventsAreNotDuplicates()
    {
        assertNotDuplicate(defaultParams(existing(42)).addWebHookId("webhook1").build(), defaultParams(newlyCreated()).addWebHookId("webhook2").build());
    }

    @Test
    public void listenersWithDifferentNamesAreDuplicates()
    {
        assertDuplicate(defaultParams(existing(42)).setListenerName("name").build(), defaultParams(newlyCreated()).setListenerName("name").build());
    }

    private void assertNotDuplicate(PersistentWebHookListener listener, PersistentWebHookListener another)
    {
        assertFalse(new ListenerDuplicatePredicate(listener).apply(another));
    }

    private void assertDuplicate(PersistentWebHookListener listener, PersistentWebHookListener another)
    {
        assertTrue(new ListenerDuplicatePredicate(listener).apply(another));
    }

    private PersistentWebHookListener.Builder defaultParams(PersistentWebHookListener.Builder existing)
    {
        return existing.setUrl("/url").addFilter("default-filter", "whatever").addWebHookId("webhook").setListenerName("listener");
    }
}
