package com.atlassian.webhooks.api.util;

import com.atlassian.webhooks.api.util.Vote;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CompositeVoteTest {

    @Test
    public void testNoVotes() {
        assertEquals(Vote.ABSTAIN, new CompositeVote().getOutcome());
    }

    @Test
    public void testAllAbstain() {
        CompositeVote compositeVote = new CompositeVote();
        assertEquals(Vote.ABSTAIN, compositeVote.add(Vote.ABSTAIN));
        assertEquals(Vote.ABSTAIN, compositeVote.add(Vote.ABSTAIN));
        assertEquals(Vote.ABSTAIN, compositeVote.getOutcome());
    }

    @Test
    public void testAbstainThenAllow() {
        CompositeVote compositeVote = new CompositeVote();
        assertEquals(Vote.ABSTAIN, compositeVote.add(Vote.ABSTAIN));
        assertEquals(Vote.ALLOW, compositeVote.add(Vote.ALLOW));
        assertEquals(Vote.ALLOW, compositeVote.add(Vote.ABSTAIN));
        assertEquals(Vote.ALLOW, compositeVote.getOutcome());
    }

    @Test
    public void testAbstainThenDeny() {
        CompositeVote compositeVote = new CompositeVote();
        assertEquals(Vote.ABSTAIN, compositeVote.add(Vote.ABSTAIN));
        assertEquals(Vote.DENY, compositeVote.add(Vote.DENY));
        assertEquals(Vote.DENY, compositeVote.add(Vote.ABSTAIN));
        assertEquals(Vote.DENY, compositeVote.add(Vote.ALLOW));
        assertEquals(Vote.DENY, compositeVote.getOutcome());
    }

    @Test
    public void testAllowThenDeny() {
        CompositeVote compositeVote = new CompositeVote();
        assertEquals(Vote.ALLOW, compositeVote.add(Vote.ALLOW));
        assertEquals(Vote.DENY, compositeVote.add(Vote.DENY));
        assertEquals(Vote.DENY, compositeVote.add(Vote.ABSTAIN));
        assertEquals(Vote.DENY, compositeVote.add(Vote.ALLOW));
        assertEquals(Vote.DENY, compositeVote.getOutcome());
    }
}
