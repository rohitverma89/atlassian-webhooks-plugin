package com.atlassian.webhooks.api.util;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TypeRichStringTest
{
    @Test
    public void onlyStringsOfTheSameTypeAreEqual()
    {
        assertFalse(new SectionKey("foo").equals(new Filter("foo")));
    }

    @Test
    public void equalityWorkForTheSameTypes()
    {
        assertTrue(new SectionKey("foo").equals(new SectionKey("foo")));
    }

    @Test
    public void differentValuesAreNotEqual()
    {
        assertFalse(new SectionKey("foo").equals(new SectionKey("bar")));
    }
}
