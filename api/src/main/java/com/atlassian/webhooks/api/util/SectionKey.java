package com.atlassian.webhooks.api.util;

import com.atlassian.annotations.PublicApi;

import javax.annotation.concurrent.Immutable;

@PublicApi
@Immutable
public final class SectionKey extends TypeRichString
{
    public SectionKey(String id)
    {
        super(id);
    }
}
