package com.atlassian.webhooks.api.events;

import com.atlassian.annotations.PublicApi;

/**
 * Raise if you want webhook listener cache cleared.
 */
@PublicApi
public class WebHookClearListenerCacheEvent
{}
