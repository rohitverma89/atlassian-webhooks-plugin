package com.atlassian.webhooks.api.events;

import com.atlassian.analytics.api.annotations.Analytics;
import com.atlassian.annotations.PublicApi;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;

import java.util.Map;

@Analytics("webhooks.enabled")
@PublicApi
public class WebHookListenerEnabledEvent extends AbstractdWebHookListenerEvent
{
	public WebHookListenerEnabledEvent(String name, String url, Iterable<String> events, Map<String, String> parameters, RegistrationMethod registrationMethod)
    {
		super(name, url, events, parameters, registrationMethod);
	}
}
