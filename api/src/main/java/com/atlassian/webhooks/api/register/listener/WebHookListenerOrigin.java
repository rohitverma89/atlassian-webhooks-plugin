package com.atlassian.webhooks.api.register.listener;

import com.atlassian.annotations.PublicApi;
import com.google.common.base.Function;

@PublicApi
public enum WebHookListenerOrigin
{
    MODULE_DESCRIPTOR, PERSISTENT_STORE
}
