package com.atlassian.webhooks.api.util;

import com.atlassian.annotations.PublicApi;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.spi.EventMatcher;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import java.util.Arrays;

@PublicApi
public final class EventMatchers
{
    private EventMatchers() {}

    public static final EventMatcher<Object> ALWAYS_TRUE = new EventMatcher<Object>()
    {
        @Override
        public boolean matches(Object event, WebHookListener listenerParameters)
        {
            return true;
        }
    };

    /**
     * Useful for registering events but then firing them manually outside the event system
     */
    public static final EventMatcher<Object> ALWAYS_FALSE = new EventMatcher<Object>()
    {
        @Override
        public boolean matches(Object event, WebHookListener listenerParameters)
        {
            return false;
        }
    };

    public static <T> EventMatcher<T> and(final Iterable<EventMatcher<T>> matchers)
    {
        return new EventMatcher<T>()
        {
            @Override
            public boolean matches(final T event, final WebHookListener listener)
            {
                return Iterables.all(matchers, new Predicate<EventMatcher<T>>()
                {
                    @Override
                    public boolean apply(final EventMatcher<T> matcher)
                    {
                        return matcher.matches(event, listener);
                    }
                });
            }
        };
    }

    public static <T> EventMatcher<T> and(EventMatcher<T>... matchers)
    {
        return and(Arrays.asList(matchers));
    }

    public static EventMatcher<Object> or(final Iterable<EventMatcher<Object>> matchers)
    {
        return new EventMatcher<Object>()
        {
            @Override
            public boolean matches(final Object event, final WebHookListener listener)
            {
                return Iterables.any(matchers, new Predicate<EventMatcher<Object>>()
                {
                    @Override
                    public boolean apply(final EventMatcher<Object> matcher)
                    {
                        return matcher.matches(event, listener);
                    }
                });
            }
        };
    }

    public static EventMatcher<Object> or(EventMatcher<Object>... matchers)
    {
        return or(Arrays.asList(matchers));
    }
}
