package com.atlassian.webhooks.api.util;

import com.atlassian.annotations.PublicApi;

/**
 * Utility class for combining multiple votes into a single outcome. The algorithm that's use is
 * <ul>
 *     <li>If any voter denies access, the outcome is DENY</li>
 *     <li>otherwise, if any voter ALLOWs access, the outcome is ALLOW</li>
 *     <li>otherwise, (all voters abstain) the outcome is ABSTAIN</li>
 * </ul>
 */
@PublicApi
public class CompositeVote
{
    private Vote outcome = Vote.ABSTAIN;

    /**
     * Adds a vote and returns the updated {@link #getOutcome() outcome}
     *
     * @param vote the vote to add
     * @return the updated outcome, taking the new vote into account
     */
    public Vote add(Vote vote)
    {
        switch (vote)
        {
            case DENY:
                outcome = Vote.DENY;
                break;
            case ALLOW:
                if (outcome == Vote.ABSTAIN)
                {
                    outcome = Vote.ALLOW;
                }
                break;
        }
        return getOutcome();
    }

    /**
     * @return the outcome of combining all {@link #add(Vote) added} votes
     */
    public Vote getOutcome()
    {
        return outcome;
    }
}
