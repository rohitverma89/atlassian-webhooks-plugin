package com.atlassian.webhooks.api.events;

import com.atlassian.analytics.api.annotations.Analytics;
import com.atlassian.annotations.PublicApi;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;

import java.util.Map;

@Analytics("webhooks.disabled")
@PublicApi
public class WebHookListenerDisabledEvent extends AbstractdWebHookListenerEvent
{
	public WebHookListenerDisabledEvent(String name, String url, Iterable<String> events, Map<String, String> parameters, RegistrationMethod registrationMethod)
    {
		super(name, url, events, parameters, registrationMethod);
	}
}
