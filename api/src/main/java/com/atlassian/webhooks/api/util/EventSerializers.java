package com.atlassian.webhooks.api.util;

import com.atlassian.annotations.PublicApi;
import com.atlassian.webhooks.spi.EventSerializer;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

@PublicApi
public class EventSerializers
{
    private EventSerializers() {}

    public static final EventSerializer<Object> REFLECTION = new ReflectionEventSerializer<Object>();

    public static String objectToJson(Map<String, Object> map)
    {
        try
        {
            return new JSONObject(map).toString(2);
        }
        catch (JSONException e)
        {
            throw new RuntimeException(e);
        }
    }


    static final class ReflectionEventSerializer<T> implements EventSerializer<T>
    {
        private static final Set<Class<?>> RAW_TYPES = ImmutableSet.<Class<?>>builder()
                .add(boolean.class)
                .add(Boolean.class)
                .add(byte.class)
                .add(Byte.class)
                .add(short.class)
                .add(Short.class)
                .add(int.class)
                .add(Integer.class)
                .add(long.class)
                .add(Long.class)
                .add(float.class)
                .add(Float.class)
                .add(double.class)
                .add(Double.class)
                .add(char.class)
                .add(Character.class)
                .add(String.class)
                .build();

        @Override
        public String serialize(T event)
        {
            return objectToJson(toMap(event));
        }

        @VisibleForTesting
        static Map<String, Object> toMap(Object object)
        {
            if (object == null)
            {
                return ImmutableMap.of();
            }

            final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
            final Field[] fields = object.getClass().getDeclaredFields();
            for (Field field : fields)
            {
                final Object value = getValue(field, object);
                if (value != null)
                {
                    builder.put(field.getName(), value);
                }
            }
            return builder.build();
        }

        private static Object getValue(Field field, Object object)
        {
            return getTransformedValue(getRawValue(field, object));
        }

        private static Object getTransformedValue(Object object)
        {
            if (object == null)
            {
                return null;
            }
            if (RAW_TYPES.contains(object.getClass()))
            {
                return object;
            }
            if (Iterable.class.isAssignableFrom(object.getClass()))
            {
                return Iterables.transform((Iterable<Object>) object, new Function<Object, Object>()
                {
                    @Override
                    public Object apply(Object input)
                    {
                        return getTransformedValue(input);
                    }
                });
            }
            return toMap(object);
        }

        private static Object getRawValue(Field field, Object object)
        {
            final boolean isFieldAccessible = field.isAccessible();
            field.setAccessible(true);
            try
            {
                return field.get(object);
            }
            catch (IllegalAccessException e)
            {
                throw new IllegalStateException("Should not happen", e);
            }
            finally
            {
                field.setAccessible(isFieldAccessible);
            }
        }
    }
}