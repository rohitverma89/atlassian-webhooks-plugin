package com.atlassian.webhooks.api.events;

import com.atlassian.analytics.api.annotations.Analytics;
import com.atlassian.annotations.PublicApi;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;

@Analytics("webhooks.published")
@PublicApi
public final class WebHookPublishedEvent extends AbstractWebHookPublishResultEvent
{
    public WebHookPublishedEvent(String webHookId, WebHookListenerRegistrationDetails registrationDetails)
    {
        super(webHookId, registrationDetails);
    }
}
