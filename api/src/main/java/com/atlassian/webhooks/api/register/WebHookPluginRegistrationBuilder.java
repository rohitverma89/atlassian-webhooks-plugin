package com.atlassian.webhooks.api.register;

import com.atlassian.annotations.PublicApi;
import com.atlassian.webhooks.spi.EventSerializer;
import com.atlassian.webhooks.spi.QueryParamsProvider;
import com.atlassian.webhooks.spi.RequestSigner;
import com.atlassian.webhooks.spi.RequestSigner2;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import com.atlassian.webhooks.spi.WebHookListenerActionValidator;
import com.atlassian.webhooks.spi.WebHookPluginRegistrationFactory;
import com.atlassian.webhooks.spi.WebHooksHtmlPanel;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.fugue.Option.option;
import static com.google.common.collect.Iterables.concat;
import static java.util.Collections.singleton;

@PublicApi
public class WebHookPluginRegistrationBuilder
{
    WebHookPluginRegistrationBuilder() {}

    private final Map<Class, UriVariablesProvider> uriVariablesProviders = Maps.newHashMap();
    private final Map<Class, EventSerializer> eventSerializers = Maps.newHashMap();
    private WebHookPluginRegistrationFactory.CloudCondition cloudCondition;
    private final Set<WebHookEventSection> sections = Sets.newLinkedHashSet();
    private final List<WebHooksHtmlPanel> panels = Lists.newArrayList();
    private final Set<RequestSigner> requestSigners = Sets.newHashSet();
    private final Set<RequestSigner2> requestSigners2 = Sets.newHashSet();
    private final Set<WebHookListenerActionValidator> validators = Sets.newLinkedHashSet();
    private final Set<QueryParamsProvider> queryParamsProviders = Sets.newHashSet();

    private final WebHookEventGroup.Builder systemWebHooksGroup = WebHookEventGroup.builder();

    /**
     * Registers a variable provider for a particular class of events.
     *
     * @param type class of events the variable provider can operate on
     * @param uriVariablesProvider variable provider
     * @param <T> type of event
     * @return this builder
     */
    public <T> WebHookPluginRegistrationBuilder variablesProvider(Class<T> type, UriVariablesProvider<? super T> uriVariablesProvider)
    {
        this.uriVariablesProviders.put(type, uriVariablesProvider);
        return this;
    }

    /**
     * Registers an event serializer for a particular event class.
     *
     * @param type class of events that the serializer can operate on
     * @param serializer event serializer
     * @param <T> type of event
     * @return this builder
     */
    public <T> WebHookPluginRegistrationBuilder eventSerializer(Class<T> type, EventSerializer<? super T> serializer)
    {
        this.eventSerializers.put(type, serializer);
        return this;
    }

    /**
     * Register an object that will answer the question: are we in the cloud?
     *
     * @param cloudCondition a function returning boolean
     * @return this builder
     */
    public WebHookPluginRegistrationBuilder cloudCondition(WebHookPluginRegistrationFactory.CloudCondition cloudCondition)
    {
        this.cloudCondition = cloudCondition;
        return this;
    }

    /**
     * Register a section of webhooks. Sections are divided into groups of webhooks. If a section has a name, then
     * webhooks contained in it will be available for users in the UI. <p> <p>You can register a section without any
     * name if you don't want it to be available in UI. The same can be achieved using the {@link
     * com.atlassian.webhooks.api.register.WebHookPluginRegistrationBuilder#addWebHook(RegisteredWebHookEvent)} method.
     * </p>
     *
     * @param section a section of groups of webhooks
     * @return this build
     */
    public WebHookPluginRegistrationBuilder addWebHookSection(WebHookEventSection section)
    {
        sections.add(section);
        return this;
    }

    /**
     * Register a webhook event that will <b>not</b> be available in the UI. <p>That's your method of choice if you
     * don't care about sections or groups and you just want to register your webhook.</p>
     *
     * @param event webhook event
     * @return this builder
     */
    public WebHookPluginRegistrationBuilder addWebHook(RegisteredWebHookEvent<?> event)
    {
        systemWebHooksGroup.addEvent(event);
        return this;
    }

    /**
     * Add HTML panel that will be displayed below all webhook sections.
     *
     * @param panel function rendering arbitrary HTML
     * @return this builder
     */
    public WebHookPluginRegistrationBuilder addHtmlPanel(WebHooksHtmlPanel panel)
    {
        panels.add(panel);
        return this;
    }

    public WebHookPluginRegistrationBuilder addValidator(WebHookListenerActionValidator actionValidator)
    {
        validators.add(actionValidator);
        return this;
    }

    public WebHookPluginRegistrationBuilder addRequestSigner(RequestSigner requestSigner)
    {
        requestSigners.add(requestSigner);
        return this;
    }

    public WebHookPluginRegistrationBuilder addRequestSigner2(RequestSigner2 requestSigner2)
    {
        requestSigners2.add(requestSigner2);
        return this;
    }

    public WebHookPluginRegistrationBuilder addQueryParamsProvider(QueryParamsProvider queryParamsProvider)
    {
        queryParamsProviders.add(queryParamsProvider);
        return this;
    }

    public WebHookPluginRegistration build()
    {
        return new WebHookPluginRegistration(uriVariablesProviders, eventSerializers, option(cloudCondition), concat(sections, systemSection()), panels, requestSigners, requestSigners2, validators, queryParamsProviders);
    }

    private Iterable<WebHookEventSection> systemSection()
    {
        WebHookEventGroup webHooksGroup = systemWebHooksGroup.build();
        return webHooksGroup.getEvents().isEmpty() ? Collections.<WebHookEventSection>emptySet() : singleton(WebHookEventSection.section("atlassian-webhooks-plugin-system-webhooks").addGroup(webHooksGroup).build());
    }
}
