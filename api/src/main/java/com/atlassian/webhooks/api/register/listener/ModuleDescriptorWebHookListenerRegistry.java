package com.atlassian.webhooks.api.register.listener;

import com.atlassian.annotations.PublicApi;

@PublicApi
public interface ModuleDescriptorWebHookListenerRegistry
{
    void register(String webhookId, WebHookListener webHookListener);

    void unregister(String webhookId, WebHookListener webHookListener);
}
