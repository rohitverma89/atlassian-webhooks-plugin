package com.atlassian.webhooks.api.register;

import com.atlassian.annotations.PublicApi;
import com.atlassian.webhooks.spi.EventMatcher;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Strings.emptyToNull;

/**
 * A registration of a web hook
 */
@PublicApi
@Immutable
public final class RegisteredWebHookEvent<T>
{
    private final String id;
    private final String nameI18nKey;
    private final Class<T> eventClass;
    private final EventMatcher<? super T> eventMatcher;

    /**
     * Begins a creation of the new webhook event.
     *
     * @param eventId webhook id, must not be null or empty
     * @return webhook event builder
     */
    public static Builder withId(@Nonnull String eventId)
    {
        Preconditions.checkNotNull(emptyToNull(eventId), "webhook event id must not be empty");
        return new Builder(eventId);
    }

    private RegisteredWebHookEvent(String id, final String nameI18nKey, Class<T> eventClass, EventMatcher<? super T> eventMatcher)
    {
        this.id = id;
        this.nameI18nKey = nameI18nKey;
        this.eventClass = eventClass;
        this.eventMatcher = eventMatcher;
    }

    public String getId()
    {
        return id;
    }

    public String getNameI18nKey()
    {
        return nameI18nKey;
    }

    public Class<T> getEventClass()
    {
        return eventClass;
    }

    public EventMatcher<? super T> getEventMatcher()
    {
        return eventMatcher;
    }

    @PublicApi
    public static class Builder
    {
        private final String eventId;

        private Builder(String eventId)
        {
            this.eventId = eventId;
        }

        public RegisteredWebHookEventBuilder2 andDisplayName(String nameI18nKey)
        {
            return new RegisteredWebHookEventBuilder2(nameI18nKey);
        }

        public <T> RegisteredWebHookEventBuilder3<T> firedWhen(Class<T> eventClass)
        {
            return new RegisteredWebHookEventBuilder3<T>("", eventClass);
        }

        public class RegisteredWebHookEventBuilder2
        {
            private String nameI18nKey;

            private RegisteredWebHookEventBuilder2(String nameI18nKey)
            {
                this.nameI18nKey = nameI18nKey;
            }

            public <T> RegisteredWebHookEventBuilder3<T> firedWhen(Class<T> eventClass)
            {
                return new RegisteredWebHookEventBuilder3<T>(nameI18nKey, eventClass);
            }
        }

        public class RegisteredWebHookEventBuilder3<T>
        {
            private String nameI18nKey;
            private Class<T> eventClass;

            private RegisteredWebHookEventBuilder3(String nameI18nKey, Class<T> eventClass)
            {
                this.nameI18nKey = nameI18nKey;
                this.eventClass = eventClass;
            }

            public RegisteredWebHookEvent<T> isMatchedBy(EventMatcher<? super T> eventMatches)
            {
                return new RegisteredWebHookEvent<T>(eventId, nameI18nKey, eventClass, eventMatches);
            }
        }
    }
}
