package com.atlassian.webhooks.api.register.listener;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Option;
import com.atlassian.webhooks.api.util.MessageCollection;
import com.google.common.base.Function;

import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Preconditions.checkNotNull;

@PublicApi
@Immutable
public class WebHookListenerServiceResponse
{
    private final MessageCollection messageCollection;
    private final Option<PersistentWebHookListener> registeredListener;

    private WebHookListenerServiceResponse(MessageCollection messageCollection,
            Option<PersistentWebHookListener> webHookListenerParameters)
    {
        this.messageCollection = checkNotNull(messageCollection);
        this.registeredListener = checkNotNull(webHookListenerParameters);
    }

    public MessageCollection getMessageCollection()
    {
        return messageCollection;
    }

    public Option<PersistentWebHookListener> getRegisteredListener()
    {
        return registeredListener;
    }

    public static WebHookListenerServiceResponse ok(final PersistentWebHookListener registeredListener)
    {
        return new WebHookListenerServiceResponse(MessageCollection.empty(), Option.some(registeredListener));
    }

    public static WebHookListenerServiceResponse error(final MessageCollection messageCollection)
    {
        return new WebHookListenerServiceResponse(messageCollection, Option.none(PersistentWebHookListener.class));
    }

    public <T> T fold(Function<MessageCollection, T> onError, Function<PersistentWebHookListener, T> onSuccess)
    {
        if (messageCollection.isEmpty() && registeredListener.isDefined())
        {
            return onSuccess.apply(registeredListener.get());
        }
        else if (!messageCollection.isEmpty())
        {
            return onError.apply(messageCollection);
        }
        else
        {
            throw new IllegalStateException();
        }
    }
}
