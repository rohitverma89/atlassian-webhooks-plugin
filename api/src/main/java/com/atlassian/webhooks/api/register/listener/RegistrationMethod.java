package com.atlassian.webhooks.api.register.listener;

import com.atlassian.annotations.PublicApi;

@PublicApi
public enum RegistrationMethod
{
    REST,
    UI,
    SERVICE
}
