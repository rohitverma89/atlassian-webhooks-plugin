package com.atlassian.webhooks.api.util;

import com.atlassian.annotations.PublicApi;
import com.atlassian.webhooks.spi.EventSerializer;

import java.util.List;

/**
 * Parameters from WebhookListener registration, passed to {@link EventSerializer#serialize(java.lang.Object, com.atlassian.webhooks.api.util.WebhookSerializerParameters)} method
 *
 */
@PublicApi
public interface WebhookSerializerParameters {

    /**
     * Returns list of property keys which WebhookListener has subcribed to.
     * Serializer is supposed to return requested properties of the entity it serializes.
     *
     * @return list of property keys
     */
    List<String> getPropertyKeys();

}
