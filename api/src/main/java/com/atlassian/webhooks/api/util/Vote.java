package com.atlassian.webhooks.api.util;

import com.atlassian.annotations.PublicApi;

@PublicApi
public enum Vote {
    ALLOW,
    DENY,
    ABSTAIN;

    /**
     *
     * @return true iff this == {@link com.atlassian.webhooks.api.util.Vote#ALLOW}
     */
    public boolean isAllowed()
    {
        return this == ALLOW;
    }
}
