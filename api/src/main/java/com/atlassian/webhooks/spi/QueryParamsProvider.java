package com.atlassian.webhooks.spi;

import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import com.google.common.collect.Multimap;

import java.util.Optional;

/**
 * Provider that provides query params for webhook requests.
 */
public interface QueryParamsProvider
{
    /**
     * Returns a list of query params in a form of a multimap.
     * Query params are added to the url just before the uri variables are resolved.
     * If the method will throw an exception, it will be ignored.
     * @param userProfile The profile of the user who executed the request.
     * @param registrationDetails Information about how the listener was registered.
     */
    Multimap<String, String> provideQueryParams(Optional<UserProfile> userProfile, WebHookListenerRegistrationDetails registrationDetails);
}
