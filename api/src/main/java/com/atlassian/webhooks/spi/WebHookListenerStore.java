package com.atlassian.webhooks.spi;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.fugue.Option;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;

import java.util.Collection;

/**
 * Store to be implemented by products.
 */
@PublicSpi
public interface WebHookListenerStore
{
    /**
     * Adds a new WebHook listener and returns the newly created WebHook listener.
     *
     * @param listener parameters of the listener.
     * @param registrationMethod REST, UI or SERVICE.
     */
    public PersistentWebHookListener addWebHook(PersistentWebHookListener listener, RegistrationMethod registrationMethod);

    /**
     * Updates existing WebHook listener and returns the newly created WebHook.
     *
     * @param listener parameters of the listener.
     */
    public PersistentWebHookListener updateWebHook(PersistentWebHookListener listener);

    /**
     * Get a single WebHook Listener by id.
     *
     * @param id of the WebHook Listener.
     * @return the WebHook listener.
     */
    Option<PersistentWebHookListener> getWebHook(int id);

    /**
     * Removes single WebHook Listener by id.
     *
     * @param id of the WebHook Listener.
     * @throws IllegalArgumentException the specified id does not exist
     */
    void removeWebHook(int id);

    /**
     * Get a list of all listeners in the system.
     * @return collection of WebHook listeners.
     */
    Collection<PersistentWebHookListener> getAllWebHooks();

}
