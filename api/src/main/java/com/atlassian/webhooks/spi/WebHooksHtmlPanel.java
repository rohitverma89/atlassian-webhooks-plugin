package com.atlassian.webhooks.spi;

import com.atlassian.annotations.PublicSpi;

/**
 * Function that returns arbitrary HTML fragments. Used by plug-ins
 * to provide additional panels in webhooks admin interface.
 */
@PublicSpi
public interface WebHooksHtmlPanel
{
    String getHtml();
}
