package com.atlassian.webhooks.spi;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.httpclient.api.Request;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;

import java.net.URI;
import java.util.Optional;

/**
 * Signs outgoing requests when publishing webhooks
 */
@PublicSpi
public interface RequestSigner2
{
    /**
     * Signs the requests
     * @param uri The request target uri.
     * @param userProfile The profile of the user who executed the request.
     * @param registrationDetails Information about how the listener was registered.
     * @param request The request to sign.
     */
    void sign(URI uri,
            Optional<UserProfile> userProfile,
            WebHookListenerRegistrationDetails registrationDetails,
            Request.Builder request);

}
