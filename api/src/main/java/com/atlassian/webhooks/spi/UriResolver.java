package com.atlassian.webhooks.spi;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.fugue.Option;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;

import java.net.URI;

@PublicSpi
public interface UriResolver
{
    /**
     * Gets a fully constructed URI for a relative path defined in the plugin with the given key.
     *
     * @param listenerOriginDetails registration details of web hook listener
     * @param path the relative path
     * @return an absolute URI to the plugin path.
     */
    Option<URI> getUri(WebHookListenerRegistrationDetails listenerOriginDetails, URI path);
}
