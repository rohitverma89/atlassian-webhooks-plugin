package com.atlassian.webhooks.spi;

import java.util.Collection;

/**
 * This is an URI variables provider that can tell what
 * variables it provides. You can use either this or
 * {@link com.atlassian.webhooks.api.document.ProvidesUrlVariables} annotation;
 */
public interface DocumentedUriVariablesProvider<T> extends UriVariablesProvider<T>
{
    Collection<String> providedVariables();
}
