package com.atlassian.webhooks.spi;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.webhooks.api.register.WebHookPluginRegistration;

import static com.google.common.collect.Sets.newHashSet;

/**
 * A class of objects that can produce webhook plugin registrations.
 */
@PublicSpi
public interface WebHookPluginRegistrationFactory
{
    public WebHookPluginRegistration createPluginRegistration();

    /**
     * Function that can tell whether the application is running in the cloud or not.
     */
    @PublicSpi
    interface CloudCondition
    {
        /**
         * @return true if application is running in the cloud, false otherwise.
         */
        boolean isCloud();
    }

}
