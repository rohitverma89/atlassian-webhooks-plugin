package com.atlassian.webhooks.plugin.test.servlet;

public class SignerEventServlet extends AbstractWebHookListeningServlet
{
    public static final String QUEUE_NAME = "SignerEventQueue";

    protected SignerEventServlet(final WebHooksQueues webHooksQueues)
    {
        super(webHooksQueues);
    }

    @Override
    String queueName()
    {
        return QUEUE_NAME;
    }
}
