package com.atlassian.webhooks.plugin.junit;


import com.atlassian.fugue.Option;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.plugin.event.EventForFilterTest;
import com.atlassian.webhooks.plugin.test.ServiceAccessor;
import com.atlassian.webhooks.plugin.test.servlet.FilteredEventsServlet;
import com.atlassian.webhooks.plugin.test.servlet.ReceivedWebHook;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertTrue;

public class TestFilterParameter extends WebHookUnitTest
{
    @Test
    public void eventsAreFilteredProperlyForPersistedListener()
    {
        registerListenerWithFilter("section-with-filter", "accepted");
        raiseEvent("accepted");
        assertEventReceived();
        raiseEvent("notAccepted");
        assertEventNotReceived();
    }

    @Test
    public void filtersAreAppliedToCorrectSections()
    {
        registerListenerWithFilter("unknown-section", "accepted");
        raiseEvent("accepted");
        assertEventNotReceived();
    }

    @Test
    public void filtersFromModuleDescriptorAreAppliedCorrectly()
    {
        raiseEvent("accepted");
        assertEventNotReceived();
        raiseEvent("moduleDescriptorEvent");
        assertEventReceived();
    }

    private void registerListenerWithFilter(String sectionKey, String filter)
    {
        registerListenerWithFilters(ImmutableMap.of(sectionKey, filter));
    }

    private void raiseEvent(final String valueToMatch)
    {
        ServiceAccessor.getEventPublisher().publish(new EventForFilterTest(valueToMatch));
    }

    private void assertEventReceived()
    {
        Option<ReceivedWebHook> webhook = ServiceAccessor.getWebHooksQueues().getAndWait(FilteredEventsServlet.QUEUE_NAME);
        assertTrue(webhook.isDefined());
    }

    private void assertEventNotReceived() {
        Option<ReceivedWebHook> webhook = ServiceAccessor.getWebHooksQueues().getAndWait(FilteredEventsServlet.QUEUE_NAME);
        assertTrue(webhook.isEmpty());
    }

    private void registerListenerWithFilters(Map<String, String> filters)
    {
        PersistentWebHookListener listener = PersistentWebHookListener.newlyCreated()
                .addFilters(filters)
                .addWebHookId("event_with_filter")
                .setEnabled(true)
                .setUrl(urlToServlet("filter-events"))
                .setListenerName("listener " + filters)
                .build();

        ServiceAccessor.getWebHookListenerService().registerWebHookListener(listener);
    }
}
