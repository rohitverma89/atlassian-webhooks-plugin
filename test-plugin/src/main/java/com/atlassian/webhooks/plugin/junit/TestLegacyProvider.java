package com.atlassian.webhooks.plugin.junit;

import com.atlassian.fugue.Option;
import com.atlassian.webhooks.plugin.legacy.LegacyProviderEvent;
import com.atlassian.webhooks.plugin.test.ServiceAccessor;
import com.atlassian.webhooks.plugin.test.servlet.LegacyEventServlet;
import com.atlassian.webhooks.plugin.test.servlet.ReceivedWebHook;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestLegacyProvider extends WebHookUnitTest
{

    public static final String SOME_PAYLOAD_TO_CHECK = "some-payload-to-check";

    @Test
    public void testRaisingEventWhichShouldBeMatched()
    {
        ServiceAccessor.getEventPublisher().publish(new LegacyProviderEvent(SOME_PAYLOAD_TO_CHECK, true));

        final Option<ReceivedWebHook> webHook = ServiceAccessor.getWebHooksQueues().getAndWait(LegacyEventServlet.QUEUE_NAME);

        assertTrue(webHook.get().getBody().contains(SOME_PAYLOAD_TO_CHECK));
    }

    @Test
    public void testRaisingEventWhichShouldNotBeMatched()
    {
        ServiceAccessor.getEventPublisher().publish(new LegacyProviderEvent(SOME_PAYLOAD_TO_CHECK, false));

        final Option<ReceivedWebHook> webHook = ServiceAccessor.getWebHooksQueues().getAndWait(LegacyEventServlet.QUEUE_NAME);

        assertTrue(webHook.isEmpty());
    }
}
