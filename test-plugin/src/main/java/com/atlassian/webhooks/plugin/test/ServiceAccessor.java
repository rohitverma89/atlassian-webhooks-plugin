package com.atlassian.webhooks.plugin.test;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.plugin.PluginController;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.webhooks.api.register.listener.WebHookListenerService;
import com.atlassian.webhooks.plugin.test.servlet.WebHooksQueues;

import static com.google.common.base.Preconditions.checkNotNull;

public final class ServiceAccessor
{
    private static EventPublisher eventPublisher;
    private static ApplicationProperties applicationProperties;
    private static HttpClient httpClient;
    private static WebHookListenerService webHookListenerService;
    private static PluginController pluginController;
    private static WebHooksQueues webHooksQueues;

    public ServiceAccessor(EventPublisher eventPublisher,
                           ApplicationProperties applicationProperties,
                           HttpClient httpClient,
                           WebHookListenerService webHookListenerService,
                           PluginController pluginController,
                           WebHooksQueues webHooksQueues)
    {
        ServiceAccessor.webHooksQueues = checkNotNull(webHooksQueues);
        ServiceAccessor.eventPublisher = checkNotNull(eventPublisher);
        ServiceAccessor.applicationProperties = checkNotNull(applicationProperties);
        ServiceAccessor.httpClient = checkNotNull(httpClient);
        ServiceAccessor.webHookListenerService = checkNotNull(webHookListenerService);
        ServiceAccessor.pluginController = checkNotNull(pluginController);
    }

    public static EventPublisher getEventPublisher()
    {
        return eventPublisher;
    }

    public static ApplicationProperties getApplicationProperties()
    {
        return applicationProperties;
    }

    public static HttpClient getHttpClient()
    {
        return httpClient;
    }

    public static WebHookListenerService getWebHookListenerService()
    {
        return webHookListenerService;
    }

    public static PluginController getPluginController()
    {
        return pluginController;
    }

    public static WebHooksQueues getWebHooksQueues()
    {
        return webHooksQueues;
    }
}
