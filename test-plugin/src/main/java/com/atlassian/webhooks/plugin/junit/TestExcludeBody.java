package com.atlassian.webhooks.plugin.junit;

import com.atlassian.fugue.Option;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerService;
import com.atlassian.webhooks.api.register.listener.WebHookListenerServiceResponse;
import com.atlassian.webhooks.plugin.event.ExcludeBodyTestEvent;
import com.atlassian.webhooks.plugin.event.PersistentExcludeBodyTestEvent;
import com.atlassian.webhooks.plugin.test.ServiceAccessor;
import com.atlassian.webhooks.plugin.test.servlet.ExcludedEventBodyServlet;
import com.atlassian.webhooks.plugin.test.servlet.ReceivedWebHook;
import com.google.common.base.Strings;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestExcludeBody extends WebHookUnitTest
{
    @Test
    public void moduleDescriptorListenerWithExcludeBodyParameterGetsEmptyBody() throws InterruptedException
    {
        ServiceAccessor.getEventPublisher().publish(new ExcludeBodyTestEvent());

        final Option<ReceivedWebHook> hook = ServiceAccessor.getWebHooksQueues().getAndWait(ExcludedEventBodyServlet.QUEUE_NAME);

        assertTrue(hook.isDefined());
        assertTrue(Strings.isNullOrEmpty(hook.get().getBody()));
    }

    @Test
    public void emptyContentTypeIsReceivedWhenBodyIsExcluded() throws InterruptedException
    {
        ServiceAccessor.getEventPublisher().publish(new ExcludeBodyTestEvent());

        final Option<ReceivedWebHook> hook = ServiceAccessor.getWebHooksQueues().getAndWait(ExcludedEventBodyServlet.QUEUE_NAME);

        assertTrue(hook.isDefined());
        assertTrue(Strings.isNullOrEmpty(hook.get().getContentType()));
    }

    @Test
    public void persistentListenerWithExcludeBodyParameterGetsEmptyBody() throws InterruptedException
    {
        ServiceAccessor.getWebHookListenerService().registerWebHookListener(persistentListener(true).build());
        receivedWebHookDoesNotHaveBody();
    }

    @Test
    public void excludingWebHookBodyParameterCanBeUpdated() throws Exception
    {
        WebHookListenerServiceResponse response = ServiceAccessor.getWebHookListenerService().registerWebHookListener(persistentListener(true).build());
        Integer registeredId = response.getRegisteredListener().get().getId().get();

        receivedWebHookDoesNotHaveBody();

        ServiceAccessor.getWebHookListenerService().updateWebHookListener(registeredId, WebHookListenerService.WebHookListenerUpdateInput.builder().setExcludeBody(false).build());

        receivedWebHookHasBody();

        ServiceAccessor.getWebHookListenerService().updateWebHookListener(registeredId, WebHookListenerService.WebHookListenerUpdateInput.builder().setExcludeBody(true).build());

        receivedWebHookDoesNotHaveBody();
    }

    @Test
    public void webhookWithBodyHasJsonUtf8ContentType()
    {
        ServiceAccessor.getWebHookListenerService().registerWebHookListener(persistentListener(false).build());
        final Option<ReceivedWebHook> hook = publishAndReceiveWebhookForPersistentListener();
        assertEquals("application/json; charset=UTF-8", hook.get().getContentType());
    }

    private void receivedWebHookDoesNotHaveBody()
    {
        final Option<ReceivedWebHook> hook = publishAndReceiveWebhookForPersistentListener();
        assertTrue(Strings.isNullOrEmpty(hook.get().getBody()));
    }

    private void receivedWebHookHasBody()
    {
        final Option<ReceivedWebHook> hook = publishAndReceiveWebhookForPersistentListener();
        assertFalse(Strings.isNullOrEmpty(hook.get().getBody()));
    }


    private Option<ReceivedWebHook> publishAndReceiveWebhookForPersistentListener()
    {
        ServiceAccessor.getEventPublisher().publish(new PersistentExcludeBodyTestEvent());
        final Option<ReceivedWebHook> hook = ServiceAccessor.getWebHooksQueues().getAndWait(ExcludedEventBodyServlet.QUEUE_NAME);
        assertTrue(hook.isDefined());
        return hook;
    }

    private PersistentWebHookListener.Builder persistentListener(boolean excludeBody)
    {
        return PersistentWebHookListener
                .newlyCreated()
                .setUrl(urlToServlet("excludeBody"))
                .addWebHookId("exclude_body_persistent_test_event")
                .setEnabled(true)
                .setListenerName("persistentListenerExcludingBody")
                .setExcludeBody(excludeBody);
    }
}
