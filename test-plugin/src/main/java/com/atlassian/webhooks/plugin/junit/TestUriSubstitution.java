package com.atlassian.webhooks.plugin.junit;

import com.atlassian.fugue.Option;
import com.atlassian.webhooks.plugin.event.TestEvent;
import com.atlassian.webhooks.plugin.test.ServiceAccessor;
import com.atlassian.webhooks.plugin.test.servlet.ReceivedWebHook;
import com.atlassian.webhooks.plugin.test.servlet.TestEventServlet;
import com.atlassian.webhooks.plugin.test.servlet.UriSubstitutionServlet;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestUriSubstitution extends WebHookUnitTest
{
    @Test
    public void eventValueSubstitutedInUri() throws InterruptedException
    {
        ServiceAccessor.getEventPublisher().publish(new TestEvent("test-value"));

        final Option<ReceivedWebHook> hook = ServiceAccessor.getWebHooksQueues().getAndWait(UriSubstitutionServlet.QUEUE_NAME);

        assertTrue(hook.isDefined());
        assertTrue(hook.get().getPathInfo().endsWith("test-value"));
        assertTrue(hook.get().getBody().contains("test-value"));

        final Option<ReceivedWebHook> webHookWithoutSubsUri = ServiceAccessor.getWebHooksQueues().getAndWait(TestEventServlet.QUEUE_NAME);

        assertTrue(webHookWithoutSubsUri.isDefined());
    }
}