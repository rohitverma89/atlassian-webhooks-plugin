package com.atlassian.webhooks.plugin.junit;

import com.atlassian.fugue.Option;
import com.atlassian.webhooks.plugin.event.SignerTestEvent;
import com.atlassian.webhooks.plugin.test.ServiceAccessor;
import com.atlassian.webhooks.plugin.test.servlet.ReceivedWebHook;
import com.atlassian.webhooks.plugin.test.servlet.SignerEventServlet;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestRequestSigner extends WebHookUnitTest
{
    @Test
    public void testEventSignerWithLegacySigner()
    {
        ServiceAccessor.getEventPublisher().publish(new SignerTestEvent());

        final Option<ReceivedWebHook> webHook = ServiceAccessor.getWebHooksQueues().getAndWait(SignerEventServlet.QUEUE_NAME);

        assertTrue(webHook.isDefined());
        assertEquals("true", webHook.get().getHeader("signed-with-legacy-signer"));
    }

    @Test
    public void testEventSignerWithWebHooks2Signer()
    {
        ServiceAccessor.getEventPublisher().publish(new SignerTestEvent());

        final Option<ReceivedWebHook> webHook = ServiceAccessor.getWebHooksQueues().getAndWait(SignerEventServlet.QUEUE_NAME);

        assertTrue(webHook.isDefined());
        assertEquals("true", webHook.get().getHeader("new-request-signer-works"));
    }

    @Test
    public void testEventSignedWithWebHooksNewEventSigner()
    {
        ServiceAccessor.getEventPublisher().publish(new SignerTestEvent());
        Option<ReceivedWebHook> webHook = ServiceAccessor.getWebHooksQueues().getAndWait(SignerEventServlet.QUEUE_NAME);

        assertTrue(webHook.isDefined());
        assertEquals("true", webHook.get().getHeader("request-signed-with-signer2"));
    }
}
