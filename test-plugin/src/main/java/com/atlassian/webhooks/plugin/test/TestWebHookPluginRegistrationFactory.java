package com.atlassian.webhooks.plugin.test;

import com.atlassian.httpclient.api.Request;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.webhooks.api.document.ProvidesUrlVariables;
import com.atlassian.webhooks.api.register.RegisteredWebHookEvent;
import com.atlassian.webhooks.api.register.WebHookEventGroup;
import com.atlassian.webhooks.api.register.WebHookEventSection;
import com.atlassian.webhooks.api.register.WebHookPluginRegistration;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import com.atlassian.webhooks.api.util.EventMatchers;
import com.atlassian.webhooks.api.util.EventSerializers;
import com.atlassian.webhooks.api.util.WebhookSerializerParameters;
import com.atlassian.webhooks.plugin.event.EventForFilterTest;
import com.atlassian.webhooks.plugin.event.ExcludeBodyTestEvent;
import com.atlassian.webhooks.plugin.event.PersistentExcludeBodyTestEvent;
import com.atlassian.webhooks.plugin.event.SignerTestEvent;
import com.atlassian.webhooks.plugin.event.TestEvent;
import com.atlassian.webhooks.plugin.management.WebHookListenerActionValidatorImpl;
import com.atlassian.webhooks.spi.EventMatcher;
import com.atlassian.webhooks.spi.EventSerializer;
import com.atlassian.webhooks.spi.RequestSigner;
import com.atlassian.webhooks.spi.RequestSigner2;
import com.atlassian.webhooks.spi.UriVariablesProvider;
import com.atlassian.webhooks.spi.WebHookPluginRegistrationFactory;
import com.atlassian.webhooks.spi.WebHooksHtmlPanel;
import com.google.common.collect.ImmutableMap;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.atlassian.webhooks.api.register.RegisteredWebHookEvent.withId;

public class TestWebHookPluginRegistrationFactory implements WebHookPluginRegistrationFactory
{
    private final TemplateRenderer templateRenderer;

    public TestWebHookPluginRegistrationFactory(final TemplateRenderer templateRenderer)
    {
        this.templateRenderer = templateRenderer;
    }

    @Override
    public WebHookPluginRegistration createPluginRegistration()
    {
        WebHookEventGroup group1 = WebHookEventGroup.builder()
                .nameI18nKey("Events for test")
                .addEvent(withId("test_event")
                        .andDisplayName("Test event")
                        .firedWhen(TestEvent.class)
                        .isMatchedBy(EventMatchers.ALWAYS_TRUE))
                .addEvent(withId("exclude_body_test_event")
                        .andDisplayName("Test event 2")
                        .firedWhen(ExcludeBodyTestEvent.class)
                        .isMatchedBy(EventMatchers.ALWAYS_TRUE))
                .addEvent(withId("exclude_body_persistent_test_event")
                        .andDisplayName("Test event 3")
                        .firedWhen(PersistentExcludeBodyTestEvent.class)
                        .isMatchedBy(EventMatchers.ALWAYS_TRUE))
                .addEvent(withId("event_with_filter")
                        .andDisplayName("Test event 4")
                        .firedWhen(EventForFilterTest.class)
                        .isMatchedBy(new EventMatcher<EventForFilterTest>()
                        {
                            @Override
                            public boolean matches(final EventForFilterTest event, final WebHookListener listener)
                            {
                                return event.getValue().equals(listener.getParameters().getFilter());
                            }
                        }))
                .addEvent(withId("signer_event")
                        .andDisplayName("signer event")
                        .firedWhen(SignerTestEvent.class)
                        .isMatchedBy(EventMatchers.ALWAYS_TRUE))
                .build();

        WebHookEventGroup group2 = WebHookEventGroup.builder()
                .nameI18nKey("Example events for UI")
                .addEvent(RegisteredWebHookEvent.withId("event1")
                        .andDisplayName("Event number one")
                        .firedWhen(TestEvent.class)
                        .isMatchedBy(EventMatchers.ALWAYS_TRUE))
                .addEvent(RegisteredWebHookEvent.withId("event2")
                        .andDisplayName("Event number two")
                        .firedWhen(TestEvent.class)
                        .isMatchedBy(EventMatchers.ALWAYS_TRUE))
                .build();

        WebHookEventSection section1 = WebHookEventSection.section("section-with-filter")
                .nameI18nKey("section.with.filter")
                .descriptionI18nKey("This section contains custom panel for filters")
                .addGroup(group1)
                .addGroup(group2)
                .panel(renderFilter("text-area-filter.vm"))
                .build();

        WebHookEventSection section2 = WebHookEventSection.section("section-with-cool-filter")
                .nameI18nKey("Section with fancy filter")
                .descriptionI18nKey("This section contains custom radio button filter")
                .addGroup(WebHookEventGroup.builder()
                        .nameI18nKey("Group with some test events")
                        .addEvent(RegisteredWebHookEvent.withId("event3")
                                .andDisplayName("Event number three")
                                .firedWhen(TestEvent.class)
                                .isMatchedBy(EventMatchers.ALWAYS_TRUE))
                        .addEvent(RegisteredWebHookEvent.withId("event4")
                                .andDisplayName("Event number four")
                                .firedWhen(TestEvent.class)
                                .isMatchedBy(EventMatchers.ALWAYS_TRUE))
                        .build())
                .panel(renderFilter("radio-buttons-filter.vm"))
                .build();

        return WebHookPluginRegistration.builder()
                .variablesProvider(TestEvent.class, new ContextResolver())
                .eventSerializer(TestEvent.class, new EventSerializer<TestEvent>()
                {
                    @ProvidesUrlVariables ("value")
                    @Override
                    public String serialize(TestEvent event)
                    {
                        throw new AssertionError("This method should not be called by our implementation any more.");
                    }

                    @Override
                    public String serialize(TestEvent event, WebhookSerializerParameters WebhookserializerParameters) {
                        return String.format("{\"value\": \"%s\" %s}", event.value, renderPropertiesAsString(WebhookserializerParameters.getPropertyKeys()));
                    }

                    private String renderPropertiesAsString(List<String> properties) {
                        if (properties.isEmpty()) {
                            return "";
                        } else {
                            String propertiesAsString = properties.stream()
                                    .map(property -> "\"" + property + "\"")
                                    .collect(Collectors.joining(","));
                            return String.format(", \"properties\": [%s]", propertiesAsString);
                        }
                    }
                })
                .variablesProvider(ExcludeBodyTestEvent.class, new UriVariablesProvider<Object>()
                {
                    @ProvidesUrlVariables ("excludeBodyTestEvent")
                    @Override
                    public Map<String, Object> uriVariables(final Object event)
                    {
                        return ImmutableMap.<String, Object>of("excludeBodyTestEvent", "true");
                    }
                })
                .variablesProvider(PersistentExcludeBodyTestEvent.class, new UriVariablesProvider<Object>()
                {
                    @ProvidesUrlVariables ("persistentExcludeBodyTestEvent")
                    @Override
                    public Map<String, Object> uriVariables(final Object event)
                    {
                        return ImmutableMap.<String, Object>of("persistentExcludeBodyTestEvent", "true");
                    }
                })
                .eventSerializer(ExcludeBodyTestEvent.class, EventSerializers.REFLECTION)
                .eventSerializer(PersistentExcludeBodyTestEvent.class, EventSerializers.REFLECTION)
                .addWebHookSection(section1)
                .addWebHookSection(section2)
                .addValidator(new WebHookListenerActionValidatorImpl())
                .addHtmlPanel(new WebHooksHtmlPanel()
                {
                    @Override
                    public String getHtml()
                    {
                        return "<div>Custom HTML Panel from plug-in</div>";
                    }
                })
                .addRequestSigner(new RequestSigner()
                {
                    @Override
                    public void sign(final URI uri, final WebHookListenerRegistrationDetails registrationDetails, final Request.Builder request)
                    {
                        request.setHeader("new-request-signer-works", "true");
                    }
                })
                .addRequestSigner2(new RequestSigner2()
                {
                    @Override
                    public void sign(final URI uri, final Optional<UserProfile> userProfile, final WebHookListenerRegistrationDetails registrationDetails, final Request.Builder request)
                    {
                        request.setHeader("request-signed-with-signer2", "true");
                    }
                })
                .build();
    }

    @ProvidesUrlVariables ("test")
    private static class ContextResolver implements UriVariablesProvider<TestEvent>
    {
        @Override
        public Map<String, Object> uriVariables(TestEvent event)
        {
            return ImmutableMap.<String, Object>of("test", ImmutableMap.of("value", event.value));
        }
    }

    private WebHooksHtmlPanel renderFilter(final String templateName)
    {
        return new WebHooksHtmlPanel()
        {
            @Override
            public String getHtml()
            {
                StringWriter stringWriter = new StringWriter();
                try
                {
                    templateRenderer.render("templates/" + templateName, stringWriter);
                    return stringWriter.toString();
                }
                catch (IOException e)
                {
                    return e.getMessage();
                }
            }
        };
    }
}
