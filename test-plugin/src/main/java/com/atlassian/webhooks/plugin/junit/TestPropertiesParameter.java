package com.atlassian.webhooks.plugin.junit;

import com.atlassian.fugue.Option;
import com.atlassian.webhooks.plugin.event.TestEvent;
import com.atlassian.webhooks.plugin.test.ServiceAccessor;
import com.atlassian.webhooks.plugin.test.servlet.PropertiesServlet;
import com.atlassian.webhooks.plugin.test.servlet.ReceivedWebHook;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestPropertiesParameter extends WebHookUnitTest {
    @Test
    public void eventPropertiesInWebhook() throws InterruptedException {
        ServiceAccessor.getEventPublisher().publish(new TestEvent("test-value"));

        final Option<ReceivedWebHook> hook = ServiceAccessor.getWebHooksQueues().getAndWait(PropertiesServlet.QUEUE_NAME);

        assertTrue(hook.isDefined());
        assertTrue(hook.get().getBody().contains("test-value"));
        assertTrue(hook.get().getBody().contains("prop1"));
        assertTrue(hook.get().getBody().contains("prop2"));
    }
}