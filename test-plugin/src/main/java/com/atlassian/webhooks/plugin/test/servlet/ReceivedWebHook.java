package com.atlassian.webhooks.plugin.test.servlet;

import com.google.common.collect.Maps;
import com.google.common.io.CharStreams;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

public class ReceivedWebHook
{
    private final String body;
    private final String pathInfo;
    private final String contentType;
    private final Map<String, String> headers;

    private ReceivedWebHook(String body, final String pathInfo, String contentType, final Map<String, String> headers)
    {
        this.body = body;
        this.pathInfo = pathInfo;
        this.contentType = contentType;
        this.headers = headers;
    }

    public static ReceivedWebHook fromServletRequest(final HttpServletRequest req) throws IOException
    {
        Map<String, String> headers = getHeaders(req);
        String pathInfo = req.getPathInfo();
        String contentType = req.getContentType();
        String body = IOUtils.toString(req.getInputStream());
        return new ReceivedWebHook(body, pathInfo, contentType, headers);
    }

    private static Map<String, String> getHeaders(final HttpServletRequest req)
    {
        Map<String, String> headers = Maps.newHashMap();
        Enumeration headerNames = req.getHeaderNames();
        while (headerNames.hasMoreElements())
        {
            String headerName = (java.lang.String) headerNames.nextElement();
            headers.put(headerName, req.getHeader(headerName));
        }
        return headers;
    }

    public String getPathInfo()
    {
        return pathInfo;
    }

    public String getBody()
    {
        return body;
    }

    public String getHeader(String name)
    {
        return headers.get(name);
    }

    public String getContentType()
    {
        return contentType;
    }
}
