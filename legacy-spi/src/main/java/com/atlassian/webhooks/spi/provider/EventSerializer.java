package com.atlassian.webhooks.spi.provider;

@Deprecated
public interface EventSerializer
{
    Object getEvent();
    String getWebHookBody() throws EventSerializationException;
}
