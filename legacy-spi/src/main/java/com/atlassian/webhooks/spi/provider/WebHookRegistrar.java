package com.atlassian.webhooks.spi.provider;

@Deprecated
public interface WebHookRegistrar
{
    EventBuilder webhook(String id);
}
