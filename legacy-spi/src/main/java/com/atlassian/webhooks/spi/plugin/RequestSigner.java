package com.atlassian.webhooks.spi.plugin;

import com.atlassian.httpclient.api.Request;

import java.net.URI;

/**
 * Signs outgoing requests when publishing webhooks
 */
@Deprecated
public interface RequestSigner
{
    /**
     * Signs the requests
     * @param uri The request target uri.
     * @param pluginKey The remote plugin key of the target system, may be null.
     * @param request The request to sign.
     */
    void sign(URI uri, String pluginKey, Request.Builder request);
}
