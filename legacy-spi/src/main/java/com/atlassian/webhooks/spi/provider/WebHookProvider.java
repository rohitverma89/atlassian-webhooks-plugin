package com.atlassian.webhooks.spi.provider;

@Deprecated
public interface WebHookProvider
{
    void provide(WebHookRegistrar registrar);
}
